# etodo-core

This project contains all domain types and crypto routines necessary to encrypt/decrypt tasks 
as well as secure password hashing for server login.

## Cryptography

### Libraries
This project uses [libsodium](https://doc.libsodium.org/) via the
[libsodium-sys-stable](https://crates.io/crates/libsodium-sys-stable) crate.

### Encryption

User's tasks are encrypted using [sealed_boxes](https://doc.libsodium.org/public-key_cryptography/sealed_boxes). Encryption
happens with the user's public key and decryption with the private key.

The private key is stored on the server encrypted via [secret_box](https://doc.libsodium.org/secret-key_cryptography/secretbox)
with a randomly generated key. This key is then also encrypted using another key derived from the user's password.

The user's password is never transmitted to the server. The password is [hashed](https://doc.libsodium.org/password_hashing)
on the client and then the hashed output is transmitted to the server for login verification. Upon arrival,
it is hashed again before going into the database. No information will be leaked during a server breach that would allow
an attacker to access the private key.

We could further improve the secrecy of the data if combined the user password with a local secret only known to the
client, but this has not been implemented.