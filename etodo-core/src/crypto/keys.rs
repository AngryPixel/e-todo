use libsodium_sys::crypto_box_SECRETKEYBYTES;
use libsodium_sys::{crypto_box_PUBLICKEYBYTES, crypto_box_keypair};
use secrecy::{DebugSecret, Secret, Zeroize};

use crate::crypto::{CryptoError, IntoEncrypted, TryFromDecrypted};

pub type PublicKeyData = [u8; crypto_box_PUBLICKEYBYTES as usize];
pub type PrivateKeyData = [u8; crypto_box_SECRETKEYBYTES as usize];

#[derive(Debug, Eq, PartialEq, serde::Serialize, serde::Deserialize, Clone)]
pub struct PublicKey(PublicKeyData);

impl PublicKey {
    pub fn zeroed() -> Self {
        // Safety: We just don't want to initialize the array manually
        unsafe { std::mem::zeroed::<Self>() }
    }

    pub fn encrypt<T: AsRef<[u8]>, R: IntoEncrypted>(&self, data: T) -> Result<R, CryptoError> {
        let data_bytes = data.as_ref();
        let encrypted_data_len = data_bytes.len() + libsodium_sys::crypto_box_SEALBYTES as usize;
        let mut encrypted_data = vec![0; encrypted_data_len];

        // Safety: call to c binding
        unsafe {
            if libsodium_sys::crypto_box_seal(
                encrypted_data.as_mut_ptr(),
                data_bytes.as_ptr(),
                data_bytes.len() as libc::c_ulonglong,
                self.0.as_ptr(),
            ) != 0
            {
                return Err(CryptoError::KeyEncryptFailed);
            }
        }

        Ok(R::into_encrypted(encrypted_data))
    }
}

impl Zeroize for PublicKey {
    fn zeroize(&mut self) {
        self.0.zeroize()
    }
}

impl DebugSecret for PublicKey {}

impl AsRef<[u8]> for PublicKey {
    fn as_ref(&self) -> &[u8] {
        &self.0
    }
}

impl From<PublicKeyData> for PublicKey {
    fn from(d: PublicKeyData) -> Self {
        Self(d)
    }
}

impl TryFrom<Vec<u8>> for PublicKey {
    type Error = <Vec<u8> as TryInto<PublicKeyData>>::Error;

    fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
        Ok(Self(value.try_into()?))
    }
}

#[derive(Debug, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct PrivateKey(PrivateKeyData);

impl PrivateKey {
    pub fn zeroed() -> Self {
        // Safety: We just don't want to initialize the array manually
        unsafe { std::mem::zeroed::<Self>() }
    }

    pub fn decrypt<T: AsRef<[u8]>, R: TryFromDecrypted>(
        &self,
        pub_key: &PublicKey,
        data: T,
    ) -> Result<R, CryptoError> {
        let data_bytes = data.as_ref();
        if data_bytes.len() <= libsodium_sys::crypto_box_SEALBYTES as usize {
            return Err(CryptoError::KeyDecryptFailed);
        }

        let decrypted_data_len = data_bytes.len() - libsodium_sys::crypto_box_SEALBYTES as usize;
        let mut decrypted_data = vec![0; decrypted_data_len];

        // Safety: call to c binding
        unsafe {
            if libsodium_sys::crypto_box_seal_open(
                decrypted_data.as_mut_ptr(),
                data_bytes.as_ptr(),
                data_bytes.len() as libc::c_ulonglong,
                pub_key.0.as_ptr(),
                self.0.as_ptr(),
            ) != 0
            {
                return Err(CryptoError::KeyDecryptFailed);
            }
        }

        R::try_from_decrypted(decrypted_data)
    }
}

impl DebugSecret for PrivateKey {}

impl Zeroize for PrivateKey {
    fn zeroize(&mut self) {
        self.0.zeroize()
    }
}

impl AsRef<[u8]> for PrivateKey {
    fn as_ref(&self) -> &[u8] {
        &self.0
    }
}

impl From<PrivateKeyData> for PrivateKey {
    fn from(d: PrivateKeyData) -> Self {
        Self(d)
    }
}

impl TryFrom<Vec<u8>> for PrivateKey {
    type Error = <Vec<u8> as TryInto<PrivateKeyData>>::Error;

    fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
        Ok(Self(value.try_into()?))
    }
}

pub type SecretPrivateKey = Secret<PrivateKey>;

/// Generate a new random public and private key pair.
pub fn new_random_key_pair() -> Result<(PublicKey, SecretPrivateKey), CryptoError> {
    let mut pub_key = PublicKey::zeroed();
    let mut priv_key = PrivateKey::zeroed();

    // Safety: call to libsodium.
    unsafe {
        if crypto_box_keypair(pub_key.0.as_mut_ptr(), priv_key.0.as_mut_ptr()) != 0 {
            return Err(CryptoError::KeyGenFailed);
        }
    }

    Ok((pub_key, Secret::new(priv_key)))
}
