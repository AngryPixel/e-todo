use crate::crypto::CryptoError;

pub trait TryFromDecrypted: Sized {
    fn try_from_decrypted(bytes: Vec<u8>) -> Result<Self, CryptoError>;
}

impl<T> TryFromDecrypted for T
where
    T: TryFrom<Vec<u8>> + ?Sized,
{
    fn try_from_decrypted(bytes: Vec<u8>) -> Result<Self, CryptoError> {
        T::try_from(bytes).map_err(|_| CryptoError::TypeConversionFailed)
    }
}

pub trait IntoEncrypted {
    fn into_encrypted(bytes: Vec<u8>) -> Self;
}

impl<T> IntoEncrypted for T
where
    T: From<Vec<u8>> + ?Sized,
{
    fn into_encrypted(bytes: Vec<u8>) -> Self {
        T::from(bytes)
    }
}
