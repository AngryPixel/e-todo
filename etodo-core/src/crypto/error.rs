use std::error::Error;
use std::fmt::{Display, Formatter};

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum CryptoError {
    KeyGenFailed,
    KeyEncryptFailed,
    KeyDecryptFailed,
    EncryptFailed,
    DecryptFailed,
    DerivationFailed,
    TypeConversionFailed,
    PasswordHashFailed,
    CryptoHashFailed,
}

impl Error for CryptoError {}

impl Display for CryptoError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            CryptoError::KeyGenFailed => {
                write!(f, "Key generation failed")
            }
            CryptoError::KeyEncryptFailed => {
                write!(f, "Key based encryption failed")
            }
            CryptoError::KeyDecryptFailed => {
                write!(f, "Key based decryption failed")
            }
            CryptoError::EncryptFailed => {
                write!(f, "Encryption failed")
            }
            CryptoError::DecryptFailed => {
                write!(f, "Decryption failed")
            }
            CryptoError::DerivationFailed => {
                write!(f, "Derivation failed")
            }
            CryptoError::TypeConversionFailed => {
                write!(f, "Encrypted type conversion failed")
            }
            CryptoError::PasswordHashFailed => {
                write!(f, "Password hashing failed")
            }
            CryptoError::CryptoHashFailed => {
                write!(f, "Failed to generate crypto hash")
            }
        }
    }
}
