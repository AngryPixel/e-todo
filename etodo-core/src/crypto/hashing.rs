use std::ptr::null_mut;

use crate::crypto::{CryptoError, Salt};

pub type CryptoHashData = [u8; libsodium_sys::crypto_generichash_BYTES as usize];

#[derive(Debug, Clone, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct CryptoHash(CryptoHashData);

impl CryptoHash {
    pub fn hash(bytes: &[u8]) -> Result<Self, CryptoError> {
        // Safety libsodium call
        unsafe {
            let mut result = std::mem::zeroed::<CryptoHashData>();

            if libsodium_sys::crypto_generichash(
                result.as_mut_ptr(),
                result.len(),
                bytes.as_ptr(),
                bytes.len() as libc::c_ulonglong,
                null_mut(),
                0,
            ) != 0
            {
                return Err(CryptoError::CryptoHashFailed);
            }

            Ok(Self::from(result))
        }
    }

    pub fn hash_with_salt(bytes: &[u8], salt: &Salt) -> Result<Self, CryptoError> {
        // Safety libsodium call
        unsafe {
            let mut result = std::mem::zeroed::<CryptoHashData>();

            let salt_bytes = salt.as_ref();
            if libsodium_sys::crypto_generichash(
                result.as_mut_ptr(),
                result.len(),
                bytes.as_ptr(),
                bytes.len() as libc::c_ulonglong,
                salt_bytes.as_ptr(),
                salt_bytes.len(),
            ) != 0
            {
                return Err(CryptoError::CryptoHashFailed);
            }

            Ok(Self::from(result))
        }
    }
}

impl AsRef<[u8]> for CryptoHash {
    fn as_ref(&self) -> &[u8] {
        &self.0
    }
}

impl From<CryptoHashData> for CryptoHash {
    fn from(d: CryptoHashData) -> Self {
        Self(d)
    }
}

impl TryFrom<Vec<u8>> for CryptoHash {
    type Error = <Vec<u8> as TryInto<CryptoHashData>>::Error;

    fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
        Ok(Self(value.try_into()?))
    }
}
