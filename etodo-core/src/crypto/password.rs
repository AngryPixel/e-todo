use std::ffi::{CStr, CString};

use crate::crypto::{CryptoError, CryptoHash, EncryptionKeyData, Salt};

/// Hash a password and return a string that can be stored in the database as is. The resulting
/// includes random salt and properties required to validate the password later on.
pub fn hash_password_server(password: &str) -> Result<String, CryptoError> {
    let cstr = CString::new(password).map_err(|_| CryptoError::PasswordHashFailed)?;
    // Safety calling c binding
    unsafe {
        let mut output: [libc::c_char; libsodium_sys::crypto_pwhash_STRBYTES as usize] =
            [0; libsodium_sys::crypto_pwhash_STRBYTES as usize];

        if libsodium_sys::crypto_pwhash_str(
            output.as_mut_ptr(),
            cstr.as_ptr(),
            password.len() as libc::c_ulonglong,
            libsodium_sys::crypto_pwhash_OPSLIMIT_INTERACTIVE as libc::c_ulonglong,
            libsodium_sys::crypto_pwhash_MEMLIMIT_INTERACTIVE as usize,
        ) != 0
        {
            return Err(CryptoError::PasswordHashFailed);
        }

        let output_cstr = CStr::from_ptr(output.as_ptr());

        let output_str_ref = output_cstr
            .to_str()
            .map_err(|_| CryptoError::PasswordHashFailed)?;

        Ok(String::from(output_str_ref))
    }
}

/// Verify whether an incoming password matches one that has been previously hashed. Returns true on
/// success and false on any kind of failure.
pub fn hash_password_server_verify(hashed_password: &str, password: &str) -> bool {
    let cstr = if let Ok(s) = CString::new(password) {
        s
    } else {
        return false;
    };

    let hashed_cstr = if let Ok(s) = CString::new(hashed_password) {
        s
    } else {
        return false;
    };

    // Safety calling c binding
    unsafe {
        if libsodium_sys::crypto_pwhash_str_verify(
            hashed_cstr.as_ptr(),
            cstr.as_ptr(),
            password.len() as libc::c_ulonglong,
        ) != 0
        {
            return false;
        }

        true
    }
}

pub fn hash_password_server_with_salt(password: &str, salt: &Salt) -> Result<String, CryptoError> {
    let hashed = hex::encode(CryptoHash::hash_with_salt(password.as_bytes(), salt)?);
    hash_password_server(&hashed)
}

pub fn hash_password_server_verify_with_salt(
    server_password: &str,
    password: &str,
    salt: &Salt,
) -> Result<bool, CryptoError> {
    let hashed = hex::encode(CryptoHash::hash_with_salt(password.as_bytes(), salt)?);
    Ok(hash_password_server_verify(server_password, &hashed))
}

pub fn hash_password_client(password: &str, salt: &Salt) -> Result<String, CryptoError> {
    unsafe {
        let mut result = std::mem::zeroed::<EncryptionKeyData>();
        if libsodium_sys::crypto_pwhash(
            result.as_mut_ptr(),
            result.len() as libc::c_ulonglong,
            std::mem::transmute(password.as_ptr()),
            password.len() as libc::c_ulonglong,
            salt.as_ref().as_ptr(),
            libsodium_sys::crypto_pwhash_OPSLIMIT_SENSITIVE as libc::c_ulonglong,
            libsodium_sys::crypto_pwhash_MEMLIMIT_MODERATE as usize,
            libsodium_sys::crypto_pwhash_ALG_DEFAULT as libc::c_int,
        ) != 0
        {
            return Err(CryptoError::PasswordHashFailed);
        }

        Ok(hex::encode(result))
    }
}
