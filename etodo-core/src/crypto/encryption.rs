use std::marker::PhantomData;
use std::ptr::null;

use secrecy::{DebugSecret, Secret, Zeroize};

use crate::crypto::{CryptoError, IntoEncrypted, PrivateKey, PublicKey, TryFromDecrypted};

pub type SaltData = [u8; libsodium_sys::crypto_pwhash_SALTBYTES as usize];

#[derive(Debug, serde::Serialize, serde::Deserialize, Eq, PartialEq, Clone)]
pub struct Salt(SaltData);

impl Salt {
    pub fn random() -> Self {
        unsafe {
            let mut salt = std::mem::zeroed::<Self>();
            libsodium_sys::randombytes_buf(salt.0.as_mut_ptr() as *mut libc::c_void, salt.0.len());
            salt
        }
    }
}

impl From<SaltData> for Salt {
    fn from(d: SaltData) -> Self {
        Self(d)
    }
}

impl TryFrom<Vec<u8>> for Salt {
    type Error = <Vec<u8> as TryInto<SaltData>>::Error;

    fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
        Ok(Self(value.try_into()?))
    }
}

impl AsRef<[u8]> for Salt {
    fn as_ref(&self) -> &[u8] {
        &self.0
    }
}

pub type NonceData = [u8; libsodium_sys::crypto_secretbox_NONCEBYTES as usize];

#[derive(Debug, serde::Serialize, serde::Deserialize, Eq, PartialEq, Clone)]
pub struct Nonce(NonceData);

impl Nonce {
    pub fn random() -> Self {
        unsafe {
            let mut data = std::mem::zeroed::<NonceData>();
            libsodium_sys::randombytes_buf(data.as_mut_ptr() as *mut libc::c_void, data.len());

            Self(data)
        }
    }

    pub fn derived<T: AsRef<[u8]>>(data: T) -> Result<Self, CryptoError> {
        unsafe {
            let mut result = std::mem::zeroed::<NonceData>();
            let data_bytes = data.as_ref();
            if libsodium_sys::crypto_generichash(
                result.as_mut_ptr(),
                result.len(),
                data_bytes.as_ptr(),
                data_bytes.len() as libc::c_ulonglong,
                null(),
                0,
            ) != 0
            {
                return Err(CryptoError::DerivationFailed);
            }

            Ok(Self(result))
        }
    }
}

impl From<NonceData> for Nonce {
    fn from(d: NonceData) -> Self {
        Self(d)
    }
}

impl TryFrom<Vec<u8>> for Nonce {
    type Error = <Vec<u8> as TryInto<NonceData>>::Error;

    fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
        Ok(Self(value.try_into()?))
    }
}

impl AsRef<[u8]> for Nonce {
    fn as_ref(&self) -> &[u8] {
        &self.0
    }
}

pub type EncryptionKeyData = [u8; libsodium_sys::crypto_secretbox_KEYBYTES as usize];

#[derive(Debug, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct EncryptionKey(EncryptionKeyData);

impl EncryptionKey {
    pub fn zeroed() -> Self {
        // Safety: We just don't want to initialize the array manually
        unsafe { std::mem::zeroed::<Self>() }
    }

    pub fn random() -> Self {
        unsafe {
            let mut result = std::mem::zeroed::<Self>();
            libsodium_sys::crypto_secretbox_keygen(result.0.as_mut_ptr());
            result
        }
    }

    pub fn derived(password: &str, salt: &Salt) -> Result<Self, CryptoError> {
        let mut result = Self::zeroed();
        let pwd_bytes = password.as_bytes();
        unsafe {
            if libsodium_sys::crypto_pwhash(
                result.0.as_mut_ptr(),
                result.0.len() as libc::c_ulonglong,
                std::mem::transmute(pwd_bytes.as_ptr()),
                pwd_bytes.len() as libc::c_ulonglong,
                salt.0.as_ptr(),
                libsodium_sys::crypto_pwhash_OPSLIMIT_INTERACTIVE as libc::c_ulonglong,
                libsodium_sys::crypto_pwhash_MEMLIMIT_INTERACTIVE as usize,
                libsodium_sys::crypto_pwhash_ALG_DEFAULT as libc::c_int,
            ) != 0
            {
                return Err(CryptoError::KeyEncryptFailed);
            }
        }

        Ok(result)
    }

    pub fn encrypt<T: AsRef<[u8]>, R: IntoEncrypted>(
        &self,
        nonce: &Nonce,
        data: T,
    ) -> Result<R, CryptoError> {
        let data_bytes = data.as_ref();
        let encrypted_data_len =
            data_bytes.len() + libsodium_sys::crypto_secretbox_MACBYTES as usize;
        let mut result = vec![0u8; encrypted_data_len];

        unsafe {
            if libsodium_sys::crypto_secretbox_easy(
                result.as_mut_ptr(),
                data_bytes.as_ptr(),
                data_bytes.len() as libc::c_ulonglong,
                nonce.0.as_ptr(),
                self.0.as_ptr(),
            ) != 0
            {
                return Err(CryptoError::EncryptFailed);
            }
        }

        Ok(R::into_encrypted(result))
    }

    pub fn decrypt<T: AsRef<[u8]>, R: TryFromDecrypted>(
        &self,
        nonce: &Nonce,
        data: T,
    ) -> Result<R, CryptoError> {
        let data_bytes = data.as_ref();

        if data_bytes.len() <= libsodium_sys::crypto_secretbox_MACBYTES as usize {
            return Err(CryptoError::DecryptFailed);
        }

        let decrypted_data_len =
            data_bytes.len() - libsodium_sys::crypto_secretbox_MACBYTES as usize;
        let mut result = vec![0u8; decrypted_data_len];

        unsafe {
            if libsodium_sys::crypto_secretbox_open_easy(
                result.as_mut_ptr(),
                data_bytes.as_ptr(),
                data_bytes.len() as libc::c_ulonglong,
                nonce.0.as_ptr(),
                self.0.as_ptr(),
            ) != 0
            {
                return Err(CryptoError::EncryptFailed);
            }
        }

        R::try_from_decrypted(result)
    }
}

impl Zeroize for EncryptionKey {
    fn zeroize(&mut self) {
        self.0.zeroize()
    }
}

impl DebugSecret for EncryptionKey {}

impl AsRef<[u8]> for EncryptionKey {
    fn as_ref(&self) -> &[u8] {
        &self.0
    }
}

impl From<EncryptionKeyData> for EncryptionKey {
    fn from(d: EncryptionKeyData) -> Self {
        Self(d)
    }
}

impl TryFrom<Vec<u8>> for EncryptionKey {
    type Error = <Vec<u8> as TryInto<EncryptionKeyData>>::Error;

    fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
        Ok(Self(value.try_into()?))
    }
}

#[derive(Debug, serde::Serialize, serde::Deserialize, Eq, PartialEq)]
pub struct Encrypted<T>
where
    T: TryFromDecrypted + Zeroize,
{
    data: Vec<u8>,
    #[serde(skip)]
    p: PhantomData<T>,
}

impl<T: TryFromDecrypted + Zeroize> Clone for Encrypted<T> {
    fn clone(&self) -> Self {
        Self {
            data: self.data.clone(),
            p: PhantomData,
        }
    }
}

impl<T: TryFromDecrypted + Zeroize> IntoEncrypted for Encrypted<T> {
    fn into_encrypted(bytes: Vec<u8>) -> Self {
        Self {
            data: bytes,
            p: PhantomData,
        }
    }
}

impl<T: TryFromDecrypted + Zeroize> Encrypted<T> {
    pub fn new(data: Vec<u8>) -> Self {
        Self {
            data,
            p: PhantomData,
        }
    }

    pub fn decrypt(
        &self,
        encryption_key: &EncryptionKey,
        nonce: &Nonce,
    ) -> Result<Secret<T>, CryptoError> {
        encryption_key
            .decrypt(nonce, &self.data)
            .map(|v| Secret::new(v))
    }
}

impl<T: TryFromDecrypted + Zeroize> Zeroize for Encrypted<T> {
    fn zeroize(&mut self) {
        self.data.zeroize()
    }
}

impl<T: TryFromDecrypted + Zeroize> AsRef<[u8]> for Encrypted<T> {
    fn as_ref(&self) -> &[u8] {
        &self.data
    }
}

#[derive(Debug, serde::Deserialize, serde::Serialize, Eq, PartialEq, Clone)]
pub struct PrivKeyEncrypted<T>
where
    T: TryFromDecrypted + Zeroize,
{
    data: Vec<u8>,
    #[serde(skip)]
    p: PhantomData<T>,
}

impl<T: TryFromDecrypted + Zeroize> PrivKeyEncrypted<T> {
    pub fn new() -> Self {
        Self {
            data: vec![],
            p: PhantomData,
        }
    }
    pub fn with_data(data: Vec<u8>) -> Self {
        Self {
            data,
            p: PhantomData,
        }
    }

    pub fn decrypt(
        &self,
        public_key: &PublicKey,
        private_key: &PrivateKey,
    ) -> Result<Secret<T>, CryptoError> {
        private_key
            .decrypt(public_key, &self.data)
            .map(|v| Secret::new(v))
    }
}

impl<T: TryFromDecrypted + Zeroize> Default for PrivKeyEncrypted<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T: TryFromDecrypted + Zeroize> AsRef<[u8]> for PrivKeyEncrypted<T> {
    fn as_ref(&self) -> &[u8] {
        &self.data
    }
}

impl<T: TryFromDecrypted + Zeroize> IntoEncrypted for PrivKeyEncrypted<T> {
    fn into_encrypted(bytes: Vec<u8>) -> Self {
        Self::with_data(bytes)
    }
}
