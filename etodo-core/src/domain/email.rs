use validator::validate_email;

#[derive(Debug, serde::Serialize, serde::Deserialize, Eq, PartialEq, Clone, Hash)]
pub struct Email(String);

impl Email {
    pub fn parse<T: AsRef<str>>(value: T) -> Result<Self, T> {
        let str = value.as_ref();
        if !validate_email(str) {
            return Err(value);
        }

        Ok(Self(String::from(str)))
    }
}

impl AsRef<str> for Email {
    fn as_ref(&self) -> &str {
        &self.0
    }
}
