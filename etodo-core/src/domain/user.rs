use std::fmt::{Display, Formatter};

use secrecy::{ExposeSecret, Secret};
use uuid::Uuid;

use crate::crypto::{
    new_random_key_pair, CryptoError, Encrypted, EncryptionKey, Nonce, PrivateKey, PublicKey, Salt,
};
use crate::domain::Email;

/// Identifies a User in the system.
#[derive(Debug, Copy, Clone, serde::Deserialize, serde::Serialize, Eq, PartialEq, Hash)]
pub struct UserId(pub Uuid);

impl UserId {
    pub fn random() -> Self {
        Self(Uuid::new_v4())
    }
}

impl Display for UserId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.simple().fmt(f)
    }
}

/// User associated data.
#[derive(Debug, serde::Serialize, serde::Deserialize, Eq, PartialEq, Clone)]
pub struct User {
    pub id: UserId,
    pub public_key: PublicKey,
    pub encryption_key: Encrypted<EncryptionKey>,
    pub encryption_key_nonce: Nonce,
    pub private_key: Encrypted<PrivateKey>,
    pub private_key_nonce: Nonce,
    pub encryption_salt: Salt,
    pub email: Email,
}

#[derive(Debug)]
pub struct PasswordChangeOutput {
    pub key: Encrypted<EncryptionKey>,
    pub nonce: Nonce,
    pub salt: Salt,
}

impl User {
    pub fn generate(email: Email, password: &Secret<String>) -> Result<Self, CryptoError> {
        let id = UserId::random();

        let (pub_key, priv_key) = new_random_key_pair()?;
        let encryption_salt = Salt::random();
        let encryption_key_nonce = Nonce::random();
        let private_key_nonce = Nonce::random();

        let unlock_key = Self::derive_decryption_key(password, &encryption_salt)?;

        let encryption_key = Secret::new(EncryptionKey::random());

        let encrypted_priv_key = encryption_key
            .expose_secret()
            .encrypt(&private_key_nonce, priv_key.expose_secret())?;

        let encrypted_encryption_key = unlock_key
            .expose_secret()
            .encrypt(&encryption_key_nonce, encryption_key.expose_secret())?;

        Ok(Self {
            id,
            public_key: pub_key,
            encryption_key: encrypted_encryption_key,
            encryption_key_nonce,
            private_key: encrypted_priv_key,
            private_key_nonce,
            encryption_salt,
            email,
        })
    }

    pub fn with(
        email: Email,
        public_key: PublicKey,
        encryption_key: Encrypted<EncryptionKey>,
        encryption_key_nonce: Nonce,
        encryption_salt: Salt,
        private_key: Encrypted<PrivateKey>,
        private_key_nonce: Nonce,
    ) -> Self {
        Self {
            id: UserId::random(),
            email,
            public_key,
            encryption_key,
            encryption_key_nonce,
            private_key,
            private_key_nonce,
            encryption_salt,
        }
    }

    /// Derive the user's decryption key to unlock th encryption key used to encrypt the private
    /// key.
    fn derive_decryption_key(
        password: &Secret<String>,
        salt: &Salt,
    ) -> Result<Secret<EncryptionKey>, CryptoError> {
        return EncryptionKey::derived(password.expose_secret(), salt).map(Secret::new);
    }

    fn unlock_encryption_key(
        &self,
        password: &Secret<String>,
    ) -> Result<Secret<EncryptionKey>, CryptoError> {
        let decryption_key = Self::derive_decryption_key(password, &self.encryption_salt)?;
        self.encryption_key
            .decrypt(decryption_key.expose_secret(), &self.encryption_key_nonce)
    }

    /// Unlock the user's private key to decrypt all the user's encrypted data. Derive the key
    /// using `derive_decryption_key`.
    pub fn unlock_private_key(
        &self,
        password: &Secret<String>,
    ) -> Result<Secret<PrivateKey>, CryptoError> {
        let encryption_key = self.unlock_encryption_key(password)?;
        self.private_key
            .decrypt(encryption_key.expose_secret(), &self.private_key_nonce)
    }

    pub fn compute_password_change(
        &self,
        old_password: &Secret<String>,
        new_password: &Secret<String>,
    ) -> Result<PasswordChangeOutput, CryptoError> {
        let encryption_key = self.unlock_encryption_key(old_password)?;
        let encryption_salt = Salt::random();
        let encryption_key_nonce = Nonce::random();

        let new_unlock_key = Self::derive_decryption_key(new_password, &encryption_salt)?;
        let new_unlock_key_encrypted = new_unlock_key
            .expose_secret()
            .encrypt(&encryption_key_nonce, encryption_key.expose_secret())?;

        Ok(PasswordChangeOutput {
            key: new_unlock_key_encrypted,
            nonce: encryption_key_nonce,
            salt: encryption_salt,
        })
    }

    pub fn apply_password_change(&mut self, password_change_output: PasswordChangeOutput) {
        self.encryption_salt = password_change_output.salt;
        self.encryption_key_nonce = password_change_output.nonce;
        self.encryption_key = password_change_output.key;
    }
}
