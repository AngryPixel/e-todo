use std::fmt::{Display, Formatter};

use chrono::prelude::*;
use uuid::Uuid;

use crate::crypto::PrivKeyEncrypted;

/// Identifies a Todo item.
#[derive(Debug, Copy, Clone, Eq, PartialEq, serde::Serialize, serde::Deserialize, Hash)]
pub struct TaskId(pub Uuid);

impl TaskId {
    pub fn random() -> Self {
        Self(Uuid::new_v4())
    }
}

impl Display for TaskId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.simple().fmt(f)
    }
}

/// Todo item data.
#[derive(Debug, serde::Serialize, serde::Deserialize, Eq, PartialEq, Clone)]
pub struct Task {
    pub id: TaskId,
    pub dt_created: DateTime<Utc>,
    pub dt_last_updated: DateTime<Utc>,
    pub finished: bool,
    pub data: PrivKeyEncrypted<Vec<u8>>,
}

impl Task {
    pub fn new() -> Self {
        Self::with_data(PrivKeyEncrypted::new())
    }

    pub fn with_data(data: PrivKeyEncrypted<Vec<u8>>) -> Self {
        let date_time = Utc::now();

        Self {
            id: TaskId::random(),
            dt_created: date_time,
            dt_last_updated: date_time,
            finished: false,
            data,
        }
    }
}

impl Default for Task {
    fn default() -> Self {
        Self::new()
    }
}
