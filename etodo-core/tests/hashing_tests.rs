use etodo_core::crypto::{CryptoHash, Salt};

#[test]
fn crypto_hash_produces_same_results() {
    let test_input = "This is a test".as_bytes();

    let output1 = CryptoHash::hash(test_input).expect("Failed to hash");
    let output2 = CryptoHash::hash(test_input).expect("Failed to hash");

    assert_eq!(output1, output2);
}

#[test]
fn crypto_hash_with_salt_produces_same_results() {
    let salt = Salt::random();
    let test_input = "This is a test".as_bytes();

    let output1 = CryptoHash::hash_with_salt(test_input, &salt).expect("Failed to hash");
    let output2 = CryptoHash::hash_with_salt(test_input, &salt).expect("Failed to hash");

    assert_eq!(output1, output2);
}
