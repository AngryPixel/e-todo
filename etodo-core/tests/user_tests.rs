use etodo_core::domain::Email;
use secrecy::{ExposeSecret, Secret};

#[test]
fn validate_user_generates_keys() {
    // Encrypt something with user's public key and see if we can then
    // decrypt it with the private key.

    let user_password = Secret::new(uuid::Uuid::new_v4().to_string());
    let email = Email::parse("foo@bar.com").expect("Invalid email");

    let user = etodo_core::domain::User::generate(email, &user_password)
        .expect("Failed to generate user data");

    let data = "This is my secret data";

    let encrypted_data: Vec<u8> = user
        .public_key
        .encrypt(&data)
        .expect("Failed to encrypt data");

    let private_key = user
        .unlock_private_key(&user_password)
        .expect("Failed to unlock private key");

    let decrypted_data = {
        let decrypted_data: Vec<u8> = private_key
            .expose_secret()
            .decrypt(&user.public_key, &encrypted_data)
            .expect("Failed to decrypt data");
        String::from_utf8(decrypted_data).unwrap()
    };

    assert_eq!(decrypted_data, data);
}

#[test]
fn validate_password_change_still_unlocks_previously_encrypted_data() {
    let user_password = Secret::new(uuid::Uuid::new_v4().to_string());
    let new_user_password = Secret::new(uuid::Uuid::new_v4().to_string());
    let email = Email::parse("foo@bar.com").expect("Invalid email");

    let mut user = etodo_core::domain::User::generate(email, &user_password)
        .expect("Failed to generate user data");

    let data = "This is my secret data";

    let encrypted_data: Vec<u8> = user
        .public_key
        .encrypt(&data)
        .expect("Failed to encrypt data");

    let password_change_output = user
        .compute_password_change(&user_password, &new_user_password)
        .expect("Failed to compute password change");

    user.apply_password_change(password_change_output);

    user.unlock_private_key(&user_password)
        .expect_err("Managed to unlock private key with old password");

    let private_key = user
        .unlock_private_key(&new_user_password)
        .expect("Failed to unlock private key");

    let decrypted_data = {
        let decrypted_data: Vec<u8> = private_key
            .expose_secret()
            .decrypt(&user.public_key, &encrypted_data)
            .expect("Failed to decrypt data");
        String::from_utf8(decrypted_data).unwrap()
    };

    assert_eq!(decrypted_data, data);
}
