#[test]
fn test_password_hashing() {
    let password_expected = "This is my fancy password";
    let password_invalid = "This is not my fancy password";

    let password_hash = etodo_core::crypto::hash_password_server(password_expected)
        .expect("Failed to hash password");

    let invalid_password_hash =
        "this thing is very long and not really an output of password hashing";

    assert!(etodo_core::crypto::hash_password_server_verify(
        &password_hash,
        password_expected
    ));

    assert!(!etodo_core::crypto::hash_password_server_verify(
        &password_hash,
        password_invalid
    ));

    assert!(!etodo_core::crypto::hash_password_server_verify(
        invalid_password_hash,
        password_invalid
    ));
}
