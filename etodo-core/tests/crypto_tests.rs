use etodo_core::crypto::*;
use secrecy::ExposeSecret;

#[test]
fn test_key_pair_encrypt_decrypt() {
    let (pub_key, priv_key) = new_random_key_pair().unwrap();

    let data = "This is my secret data";
    let encrypted_data: Vec<u8> = pub_key.encrypt(data.as_bytes()).unwrap();
    let decrypted_data: Vec<u8> = priv_key
        .expose_secret()
        .decrypt(&pub_key, &encrypted_data)
        .unwrap();
    let decrypted_str = String::from_utf8(decrypted_data).unwrap();

    assert_eq!(data, decrypted_str)
}

#[test]
fn test_encryption_decryption() {
    let encryption_key = EncryptionKey::random();
    let nonce = Nonce::random();

    let data = "This is my secret data";
    let encrypted_data: Vec<u8> = encryption_key.encrypt(&nonce, data).unwrap();
    let decrypted_data = encryption_key.decrypt(&nonce, &encrypted_data).unwrap();
    let decrypted_str = String::from_utf8(decrypted_data).unwrap();

    assert_eq!(data, decrypted_str)
}

#[test]
fn test_encryption_decryption_with_custom_type() {
    struct FooEncrypted(Vec<u8>);

    impl From<Vec<u8>> for FooEncrypted {
        fn from(data: Vec<u8>) -> Self {
            Self(data)
        }
    }

    impl AsRef<[u8]> for FooEncrypted {
        fn as_ref(&self) -> &[u8] {
            &self.0
        }
    }

    let encryption_key = EncryptionKey::random();
    let nonce = Nonce::random();

    let data = "This is my secret data";
    let encrypted_data: FooEncrypted = encryption_key.encrypt(&nonce, data).unwrap();
    let decrypted_data = encryption_key.decrypt(&nonce, &encrypted_data).unwrap();
    let decrypted_str = String::from_utf8(decrypted_data).unwrap();

    assert_eq!(data, decrypted_str)
}

#[test]
fn test_encryption_with_key_and_nonce_derivation() {
    let my_pwd = "Super Secret Password";
    let salt = Salt::random();
    let nonce = Nonce::derived("my_user_data").unwrap();
    let data = "This is my secret data";

    let encrypted_data: Vec<u8> = {
        let encryption_key = EncryptionKey::derived(my_pwd, &salt).unwrap();
        encryption_key.encrypt(&nonce, data).unwrap()
    };

    let decrypted_str = {
        let decryption_key = EncryptionKey::derived(my_pwd, &salt).unwrap();
        let decrypted_data = decryption_key.decrypt(&nonce, &encrypted_data).unwrap();
        String::from_utf8(decrypted_data).unwrap()
    };

    assert_eq!(data, decrypted_str)
}
