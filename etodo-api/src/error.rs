use etodo_core::crypto::CryptoError;

#[derive(Debug, thiserror::Error, Eq, PartialEq, Clone)]
pub enum Error {
    #[error("Invalid parameters or pre-condition check failed")]
    InvalidParameters,
    #[error("Request requires logged in account")]
    NotLoggedIn,
    #[error("Requested resource not found")]
    NotFound,
    #[error("Server url is invalid")]
    InvalidServerURL,
    #[error("Http client error: {0}")]
    HttpClient(#[from] HttpClientError),
    #[error("An error occurred on the server")]
    ServerError,
    #[error("An error occurred while generating crypto primitives")]
    CryptoError,
    #[error("An account with this email already exists")]
    AlreadyExists,
    #[error("Already logged in")]
    AlreadyLoggedIn,
    #[error("Access forbidden")]
    AccessForbidden,
    #[error("Unexpected error")]
    UnexpectedError,
}

impl From<CryptoError> for Error {
    fn from(_: CryptoError) -> Self {
        Self::CryptoError
    }
}

#[derive(Debug, thiserror::Error, Eq, PartialEq, Clone)]
pub enum HttpClientError {
    #[error("Failed to connect to server")]
    ConnectionError,
    #[error("Request timed out")]
    TimedOut,
    #[error("Body decode error")]
    DecodeError,
    #[error("Error sending the request")]
    RequestError,
    #[error("HTTP request error {0:03}")]
    StatusCode(u16),
    #[error("Error while redirecting")]
    RedirectError,
    #[error("Unknown error")]
    Unknown,
}

impl From<reqwest::Error> for HttpClientError {
    fn from(e: reqwest::Error) -> Self {
        if e.is_status() {
            return Self::StatusCode(e.status().unwrap().as_u16());
        } else if e.is_connect() {
            return Self::ConnectionError;
        } else if e.is_timeout() {
            return Self::TimedOut;
        } else if e.is_decode() {
            return Self::DecodeError;
        } else if e.is_request() {
            return Self::RequestError;
        } else if e.is_redirect() {
            return Self::RedirectError;
        }

        Self::Unknown
    }
}

impl From<reqwest::Error> for Error {
    fn from(e: reqwest::Error) -> Self {
        Self::HttpClient(HttpClientError::from(e))
    }
}

pub(crate) fn translate_request_response_to_result(
    response: &reqwest::Response,
) -> Result<(), Error> {
    let status = response.status();

    if status.is_success() {
        return Ok(());
    }

    if status.is_server_error() {
        return Err(Error::ServerError);
    } else if status.is_client_error() {
        return match status.as_u16() {
            400 => Err(Error::InvalidParameters),
            401 => Err(Error::NotLoggedIn),
            403 => Err(Error::AccessForbidden),
            404 => Err(Error::NotFound),
            409 => Err(Error::AlreadyExists),
            _ => Err(Error::UnexpectedError),
        };
    }

    Err(Error::UnexpectedError)
}
