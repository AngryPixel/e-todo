use std::error::Error;
use std::io;
use std::io::BufReader;

#[derive(Debug, Default, Clone)]
pub struct CookieStore(pub(crate) std::sync::Arc<reqwest_cookie_store::CookieStoreRwLock>);

impl CookieStore {
    pub fn new() -> Self {
        Self(std::sync::Arc::new(
            reqwest_cookie_store::CookieStoreRwLock::new(
                reqwest_cookie_store::CookieStore::default(),
            ),
        ))
    }

    pub fn from_reader(reader: impl io::Read) -> Result<Self, Box<dyn Error + Send + Sync>> {
        let buffered_reader = BufReader::new(reader);
        let store = reqwest_cookie_store::CookieStore::load_json(buffered_reader)?;
        Ok(Self(std::sync::Arc::new(
            reqwest_cookie_store::CookieStoreRwLock::new(store),
        )))
    }

    pub fn save(&self, writer: impl io::Write) -> Result<(), Box<dyn Error + Send + Sync>> {
        let mut buffered_writer = std::io::BufWriter::new(writer);
        let store = self.0.read().unwrap();
        store.save_json(&mut buffered_writer)?;
        Ok(())
    }
}
