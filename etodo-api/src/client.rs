use std::time::Duration;

use chrono::{DateTime, Utc};
use etodo_core::crypto::{
    hash_password_client, Encrypted, EncryptionKey, Nonce, PrivKeyEncrypted, PrivateKey, PublicKey,
    Salt,
};
use etodo_core::domain::{Email, Task, TaskId, User, UserId};
use reqwest::Response;
use secrecy::{ExposeSecret, Secret};
use validator::validate_url;

use crate::cookie_store::CookieStore;
use crate::{translate_request_response_to_result, Error};

#[derive(Debug, Default)]
pub struct ClientBuilder {
    client_builder: reqwest::ClientBuilder,
    server_address: String,
    cookie_store: Option<CookieStore>,
    user: Option<User>,
}

static APP_USER_AGENT: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"),);

impl ClientBuilder {
    pub fn new() -> ClientBuilder {
        ClientBuilder {
            client_builder: reqwest::ClientBuilder::new(),
            server_address: String::new(),
            cookie_store: None,
            user: None,
        }
    }

    /// Specify the server `address:port` to which to connector to.
    pub fn server_address(mut self, address: &str) -> Self {
        self.server_address = address.into();
        self
    }

    /// Set a previously available user.
    pub fn user(mut self, user: User) -> Self {
        self.user = Some(user);
        self
    }

    /// Set the cookie store for all requests.
    pub fn cookie_store(mut self, cookie_store: CookieStore) -> Self {
        self.cookie_store = Some(cookie_store);
        self
    }

    /// Set the request timeout.
    pub fn timeout(mut self, timeout: Duration) -> Self {
        self.client_builder = self.client_builder.timeout(timeout);
        self
    }

    /// Set the connection timeout.
    pub fn connect_timeout(mut self, timeout: Duration) -> Self {
        self.client_builder = self.client_builder.connect_timeout(timeout);
        self
    }

    pub fn build(self) -> Result<Client, Error> {
        if !validate_url(&self.server_address) {
            return Err(Error::InvalidServerURL);
        }

        let cookie_store = self.cookie_store.unwrap_or_default();

        let client = self
            .client_builder
            .redirect(reqwest::redirect::Policy::none())
            .cookie_provider(cookie_store.0.clone())
            .user_agent(APP_USER_AGENT)
            // Server runs on self signed certificates
            .danger_accept_invalid_certs(true)
            .build()?;

        Ok(Client {
            client,
            server_address: self.server_address,
            cookie_store,
            user: self.user,
        })
    }
}

pub struct Client {
    client: reqwest::Client,
    server_address: String,
    cookie_store: CookieStore,
    user: Option<User>,
}

#[derive(serde::Serialize)]
struct EmailData<'a> {
    email: &'a str,
}

impl Client {
    pub async fn check_server_reachable(&self) -> Result<bool, Error> {
        let resp = self
            .client
            .get(&self.build_url("/health_check"))
            .send()
            .await?;
        Ok(resp.status().is_success())
    }

    async fn retrieve_login_salt(&mut self, email: &Email) -> Result<Salt, Error> {
        let response = self
            .client
            .get(self.build_url("/login/salt"))
            .json(&EmailData {
                email: email.as_ref(),
            })
            .send()
            .await
            .map_err(|e| Error::HttpClient(e.into()))?;

        self.convert_to_error(&response)?;

        response
            .json::<Salt>()
            .await
            .map_err(|e| Error::HttpClient(e.into()))
    }

    pub async fn login(&mut self, email: &Email, password: &Secret<String>) -> Result<(), Error> {
        if self.user.is_some() {
            return Err(Error::AlreadyLoggedIn);
        }

        let login_salt = self.retrieve_login_salt(email).await?;

        let password_hash = hash_password_client(password.expose_secret(), &login_salt)?;

        #[derive(serde::Serialize)]
        struct LoginData<'a> {
            email: &'a str,
            password: &'a str,
        }

        let login_response = self
            .client
            .post(self.build_url("/login"))
            .form(&LoginData {
                email: email.as_ref(),
                password: &password_hash,
            })
            .send()
            .await?;

        self.convert_to_error(&login_response)?;

        self.user = Some(login_response.json::<User>().await?);

        Ok(())
    }

    fn build_url(&self, path: &str) -> String {
        format!("{}{path}", &self.server_address)
    }

    pub async fn logout(&mut self) -> Result<(), Error> {
        if self.user.is_some() {
            let response = self.client.post(self.build_url("/logout")).send().await?;
            self.convert_to_error(&response)?;
            self.cookie_store.0.write().unwrap().clear();
            self.user = None;
        }
        Ok(())
    }

    pub fn user(&self) -> Option<&User> {
        self.user.as_ref()
    }

    // ---- User based operations -------------------------------------------------------------

    pub async fn create_user(
        &mut self,
        email: Email,
        password: Secret<String>,
    ) -> Result<User, Error> {
        let login_salt = self.retrieve_login_salt(&email).await?;

        let mut user = User::generate(email.clone(), &password)?;

        let hashed_password = hash_password_client(password.expose_secret(), &login_salt)?;

        #[derive(serde::Serialize)]
        struct AddUserData<'a> {
            email: &'a str,
            public_key: &'a PublicKey,
            encryption_key: &'a Encrypted<EncryptionKey>,
            encryption_key_nonce: &'a Nonce,
            private_key: &'a Encrypted<PrivateKey>,
            private_key_nonce: &'a Nonce,
            encryption_salt: &'a Salt,
            password: &'a str,
        }

        let submission_data = AddUserData {
            email: email.as_ref(),
            public_key: &user.public_key,
            encryption_key: &user.encryption_key,
            encryption_key_nonce: &user.encryption_key_nonce,
            private_key: &user.private_key,
            private_key_nonce: &user.private_key_nonce,
            encryption_salt: &user.encryption_salt,
            password: &hashed_password,
        };

        let response = self
            .client
            .post(&self.build_url("/user"))
            .json(&submission_data)
            .send()
            .await?;

        self.convert_to_error(&response)?;

        let user_id = response.json::<UserId>().await?;

        user.id = user_id;

        Ok(user)
    }

    pub async fn delete_user(&mut self) -> Result<(), Error> {
        if self.user.is_none() {
            return Err(Error::NotLoggedIn);
        }

        let response = self.client.delete(&self.build_url("/user")).send().await?;

        self.convert_to_error(&response)?;

        self.user = None;
        self.cookie_store.0.write().unwrap().clear();

        Ok(())
    }

    pub async fn update_email(&mut self, email: Email) -> Result<(), Error> {
        if self.user.is_none() {
            return Err(Error::NotLoggedIn);
        }

        let submission_data = EmailData {
            email: email.as_ref(),
        };

        let response = self
            .client
            .patch(&self.build_url("/user/email"))
            .json(&submission_data)
            .send()
            .await?;

        self.convert_to_error(&response)?;

        let user = self.user.as_mut().unwrap();
        user.email = email;

        Ok(())
    }

    pub async fn update_password(
        &mut self,
        old_password: Secret<String>,
        new_password: Secret<String>,
    ) -> Result<(), Error> {
        if self.user.is_none() {
            return Err(Error::NotLoggedIn);
        }

        let user_email = self.user.as_mut().unwrap().email.clone();

        let login_salt = self.retrieve_login_salt(&user_email).await?;

        let update_salt = {
            let response = self
                .client
                .get(self.build_url("/user/salt"))
                .json(&EmailData {
                    email: user_email.as_ref(),
                })
                .send()
                .await?;

            self.convert_to_error(&response)?;

            response.json::<Salt>().await?
        };

        let new_password_hash = hash_password_client(new_password.expose_secret(), &update_salt)?;
        let old_password_hash = hash_password_client(old_password.expose_secret(), &login_salt)?;

        let pwd_output = self
            .user
            .as_mut()
            .unwrap()
            .compute_password_change(&old_password, &new_password)?;

        #[derive(serde::Serialize)]
        struct PasswordUpdate<'a> {
            old_password: &'a str,
            new_password: &'a str,
            encrypted_key: &'a Encrypted<EncryptionKey>,
            encryption_nonce: &'a Nonce,
            encryption_salt: &'a Salt,
        }

        let response = self
            .client
            .patch(&self.build_url("/user/password"))
            .json(&PasswordUpdate {
                old_password: &old_password_hash,
                new_password: &new_password_hash,
                encrypted_key: &pwd_output.key,
                encryption_nonce: &pwd_output.nonce,
                encryption_salt: &pwd_output.salt,
            })
            .send()
            .await?;

        self.convert_to_error(&response)?;

        self.user
            .as_mut()
            .unwrap()
            .apply_password_change(pwd_output);
        Ok(())
    }

    pub async fn get_all_tasks(&mut self) -> Result<Vec<Task>, Error> {
        if self.user.is_none() {
            return Err(Error::NotLoggedIn);
        }

        let response = self.client.get(self.build_url("/task")).send().await?;

        self.convert_to_error(&response)?;

        let tasks = response.json::<Vec<Task>>().await?;

        Ok(tasks)
    }

    pub async fn add_task(&mut self, data: Vec<u8>) -> Result<Task, Error> {
        if self.user.is_none() {
            return Err(Error::NotLoggedIn);
        }

        let datetime_created = Utc::now();
        #[derive(serde::Serialize)]
        struct AddTaskData<'a> {
            created: DateTime<Utc>,
            data: &'a [u8],
        }

        let submission_data = AddTaskData {
            created: datetime_created,
            data: &data,
        };

        let response = self
            .client
            .post(self.build_url("/task"))
            .json(&submission_data)
            .send()
            .await?;

        self.convert_to_error(&response)?;

        let task_id = response.json::<TaskId>().await?;

        Ok(Task {
            id: task_id,
            dt_created: datetime_created,
            dt_last_updated: datetime_created,
            finished: false,
            data: PrivKeyEncrypted::with_data(data),
        })
    }

    pub async fn delete_tasks(&mut self, task_ids: &[TaskId]) -> Result<(), Error> {
        if self.user.is_none() {
            return Err(Error::NotLoggedIn);
        }

        if task_ids.is_empty() {
            return Err(Error::InvalidParameters);
        }

        let response = self
            .client
            .delete(self.build_url("/task"))
            .json(&task_ids)
            .send()
            .await?;

        self.convert_to_error(&response)?;

        Ok(())
    }

    pub async fn update_task(
        &mut self,
        task_id: TaskId,
        finished: Option<bool>,
        data: Option<&[u8]>,
    ) -> Result<DateTime<Utc>, Error> {
        if self.user.is_none() {
            return Err(Error::NotLoggedIn);
        }

        if finished.is_none() && data.is_none() {
            return Err(Error::InvalidParameters);
        }

        let modification_time = Utc::now();

        #[derive(serde::Serialize)]
        pub struct UpdateTaskInput<'a> {
            finished: Option<bool>,
            updated: Option<DateTime<Utc>>,
            data: Option<&'a [u8]>,
        }

        let submission_data = UpdateTaskInput {
            finished,
            data,
            updated: Some(modification_time),
        };

        let response = self
            .client
            .patch(self.build_url(&format!("/task/{task_id}")))
            .json(&submission_data)
            .send()
            .await?;

        self.convert_to_error(&response)?;

        Ok(modification_time)
    }

    fn convert_to_error(&mut self, resp: &Response) -> Result<(), Error> {
        let r = translate_request_response_to_result(resp);
        if let Err(Error::NotLoggedIn) = r {
            self.user = None;
        }

        r
    }
}
