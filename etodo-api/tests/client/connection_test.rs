use std::time::Duration;

use etodo_api::HttpClientError;
use etodo_api::{ClientBuilder, Error};
use wiremock::matchers::{method, path};
use wiremock::{Mock, ResponseTemplate};

use crate::test_client::TestClient;

#[tokio::test]
async fn test_server_not_reachable() {
    let api_client = ClientBuilder::new()
        .connect_timeout(Duration::from_millis(100))
        .timeout(Duration::from_secs(100))
        .server_address("http://locahost:8080")
        .build()
        .expect("Failed to build client");

    let err = api_client
        .check_server_reachable()
        .await
        .expect_err("Request should fail");

    assert!(matches!(
        err,
        Error::HttpClient(HttpClientError::ConnectionError)
    ));
}

#[tokio::test]
async fn test_server_connection_timed_out() {
    let tc = TestClient::new().await;

    let _mock_guard = Mock::given(path("/health_check"))
        .and(method("GET"))
        .respond_with(ResponseTemplate::new(200).set_delay(TestClient::FORCE_TIMEOUT_DURATION))
        .named("Invalid email address")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    let err = tc
        .api_client
        .check_server_reachable()
        .await
        .expect_err("Request should fail");

    assert!(matches!(err, Error::HttpClient(HttpClientError::TimedOut)));
}
