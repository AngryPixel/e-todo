use etodo_api::Error;
use etodo_core::crypto::Salt;
use etodo_core::domain::{Email, User, UserId};
use secrecy::Secret;
use wiremock::matchers::{method, path};
use wiremock::{Mock, ResponseTemplate};

use crate::test_client::TestClient;

#[tokio::test]
async fn test_call_create_user() {
    let mut tc = TestClient::new().await;

    let _mock_login_salt_guard = Mock::given(path("/login/salt"))
        .and(method("GET"))
        .respond_with(ResponseTemplate::new(200).set_body_json(Salt::random()))
        .named("Logging out")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    let _mock_user_create_guard = Mock::given(path("/user"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200).set_body_json(UserId::random()))
        .named("Logging out")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    tc.api_client
        .create_user(
            Email::parse("foo@bar.com").expect("invalid email"),
            Secret::new("foo".to_string()),
        )
        .await
        .expect("Failed to create user");
}

#[tokio::test]
async fn test_update_user_email() {
    let mut tc = TestClient::new_and_generate_user("foo@bar", "password").await;
    let new_email = Email::parse("bar@bar.com").expect("invalid email");

    let _mock_login_salt_guard = Mock::given(path("/user/email"))
        .and(method("patch"))
        .respond_with(ResponseTemplate::new(200))
        .named("Updating email")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    tc.api_client
        .update_email(new_email.clone())
        .await
        .expect("Failed to update email");

    assert_eq!(tc.api_client.user().as_ref().unwrap().email, new_email);
}

#[tokio::test]
async fn test_update_user_password() {
    let mut tc = TestClient::new_and_generate_user("foo@bar.com", "password").await;
    let new_password = Secret::new("bar".to_string());

    let _mock_login_salt_guard = Mock::given(path("/login/salt"))
        .and(method("GET"))
        .respond_with(ResponseTemplate::new(200).set_body_json(Salt::random()))
        .named("Logging out")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    let _mock_user_salt_guard = Mock::given(path("/user/salt"))
        .and(method("GET"))
        .respond_with(ResponseTemplate::new(200).set_body_json(Salt::random()))
        .named("requesting new salt")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    let _mock_update_password = Mock::given(path("/user/password"))
        .and(method("patch"))
        .respond_with(ResponseTemplate::new(200))
        .named("Updating user password")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    let old_user: User = tc.api_client.user().unwrap().clone();

    tc.api_client
        .update_password(Secret::new("password".to_string()), new_password)
        .await
        .expect("Failed to update password");

    assert_ne!(*tc.api_client.user().unwrap(), old_user);
}

#[tokio::test]
async fn test_delete_user() {
    let mut tc = TestClient::new_and_generate_user("foo@bar.com", "password").await;

    let _mock_delete_user = Mock::given(path("/user"))
        .and(method("delete"))
        .respond_with(ResponseTemplate::new(200))
        .named("deleting user")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    tc.api_client
        .delete_user()
        .await
        .expect("Failed to delete user");

    assert!(tc.api_client.user().is_none());
}

#[tokio::test]
async fn test_call_create_user_when_disabled() {
    let mut tc = TestClient::new().await;

    let _mock_login_salt_guard = Mock::given(path("/login/salt"))
        .and(method("GET"))
        .respond_with(ResponseTemplate::new(200).set_body_json(Salt::random()))
        .named("Get Login Salt")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    let _mock_user_create_guard = Mock::given(path("/user"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(403))
        .named("Use creation disabled")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    let err = tc
        .api_client
        .create_user(
            Email::parse("foo@bar.com").expect("invalid email"),
            Secret::new("foo".to_string()),
        )
        .await
        .expect_err("Request should fail");

    assert_eq!(err, Error::AccessForbidden);
}
