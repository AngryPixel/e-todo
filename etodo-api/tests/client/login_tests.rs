use etodo_api::*;
use etodo_core::crypto::Salt;
use etodo_core::domain::{Email, User};
use secrecy::Secret;
use wiremock::matchers::{method, path};
use wiremock::{Mock, ResponseTemplate};

use crate::test_client::TestClient;

#[tokio::test]
async fn test_login_salt_invalid_email() {
    let mut tc = TestClient::new().await;

    let _mock_guard = Mock::given(path("/login/salt"))
        .and(method("GET"))
        .respond_with(ResponseTemplate::new(400))
        .named("Invalid email address")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    let password = Secret::new("random password".to_string());
    let email = Email::parse("foo@foo.com").expect("Invalid email");

    let err = tc
        .api_client
        .login(&email, &password)
        .await
        .expect_err("Request should have erred");

    assert_eq!(err, Error::InvalidParameters);
}

#[tokio::test]
async fn test_login_credentials_invalid() {
    let mut tc = TestClient::new().await;

    let _mock_guard = Mock::given(path("/login/salt"))
        .and(method("GET"))
        .respond_with(ResponseTemplate::new(200).set_body_json(Salt::random()))
        .named("Get user login salt")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;
    let _mock_guard_login = Mock::given(path("/login"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(400))
        .named("Post invalid login credentials")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    let password = Secret::new("random password".to_string());
    let email = Email::parse("foo@foo.com").expect("Invalid email");

    let err = tc
        .api_client
        .login(&email, &password)
        .await
        .expect_err("Request should have erred");

    assert_eq!(err, Error::InvalidParameters);
}

#[tokio::test]
async fn test_login_credentials_valid() {
    let password = Secret::new("random password".to_string());
    let email = Email::parse("foo@foo.com").expect("Invalid email");

    let mut tc = TestClient::new().await;

    let _mock_guard = Mock::given(path("/login/salt"))
        .and(method("GET"))
        .respond_with(ResponseTemplate::new(200).set_body_json(Salt::random()))
        .named("Get user login salt")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;
    let _mock_guard_login = Mock::given(path("/login"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200).set_body_json(
            User::generate(email.clone(), &password).expect("Failed to generated user"),
        ))
        .named("Post login credentials")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    tc.api_client
        .login(&email, &password)
        .await
        .expect("Login should have succeeded");

    assert!(tc.api_client.user().is_some());
}

#[tokio::test]
async fn test_no_call_to_logout_if_not_logged_in() {
    let mut tc = TestClient::new().await;

    let _mock_guard = Mock::given(path("/logout"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .named("Logging out")
        .expect(0)
        .mount_as_scoped(&tc.server)
        .await;

    tc.api_client.logout().await.expect("Failed to logout");
}

#[tokio::test]
async fn test_call_logout_when_logged_in() {
    let mut tc = TestClient::new_and_generate_user("foo@bar", "password").await;

    let _mock_guard = Mock::given(path("/logout"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .named("Logging out")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    tc.api_client.logout().await.expect("Failed to logout");
}
