use std::time::Duration;

use etodo_api::{Client, ClientBuilder, CookieStore};
use etodo_core::domain::{Email, User};
use secrecy::Secret;
use wiremock::MockServer;

pub struct TestClient {
    pub server: MockServer,
    pub api_client: Client,
}

impl TestClient {
    pub const TIMEOUT_DURATION: Duration = Duration::from_millis(500);
    pub const FORCE_TIMEOUT_DURATION: Duration = Duration::from_millis(800);

    pub async fn new() -> TestClient {
        let server = MockServer::start().await;

        let api_client = ClientBuilder::new()
            .connect_timeout(Self::TIMEOUT_DURATION)
            .timeout(Self::TIMEOUT_DURATION)
            .server_address(&server.uri())
            .build()
            .expect("Failed to build client");

        TestClient { server, api_client }
    }

    pub async fn new_with(user: User) -> TestClient {
        let server = MockServer::start().await;

        let api_client = ClientBuilder::new()
            .connect_timeout(Self::TIMEOUT_DURATION)
            .timeout(Self::TIMEOUT_DURATION)
            .server_address(&server.uri())
            .cookie_store(CookieStore::new())
            .user(user)
            .build()
            .expect("Failed to build client");

        TestClient { server, api_client }
    }

    pub async fn new_and_generate_user(email: &str, password: &str) -> TestClient {
        let user = User::generate(
            Email::parse(email).expect("Invalid Email"),
            &Secret::new(password.to_string()),
        )
        .expect("failed to generate user");
        Self::new_with(user).await
    }
}
