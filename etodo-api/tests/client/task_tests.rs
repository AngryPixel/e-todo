use etodo_api::Error;
use etodo_core::domain::{Task, TaskId};
use wiremock::matchers::{method, path};
use wiremock::{Mock, ResponseTemplate};

use crate::test_client::TestClient;

#[tokio::test]
async fn test_get_all_tasks_authenticated() {
    let mut tc = TestClient::new_and_generate_user("foo@bar.com", "password").await;

    let _mock_get_tasks = Mock::given(path("/task"))
        .and(method("get"))
        .respond_with(ResponseTemplate::new(200).set_body_json(vec![Task::new(), Task::new()]))
        .named("Get all tasks")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    let tasks = tc
        .api_client
        .get_all_tasks()
        .await
        .expect("Failed to get tasks");

    assert_eq!(tasks.len(), 2);
}

#[tokio::test]
async fn test_get_all_tasks_auth_token_expired() {
    let mut tc = TestClient::new_and_generate_user("foo@bar.com", "password").await;

    let _mock_get_tasks = Mock::given(path("/task"))
        .and(method("get"))
        .respond_with(ResponseTemplate::new(401))
        .named("Get all tasks")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    let err = tc
        .api_client
        .get_all_tasks()
        .await
        .expect_err("Failed to not get tasks");

    assert_eq!(err, Error::NotLoggedIn,);
}

#[tokio::test]
async fn test_add_task() {
    let mut tc = TestClient::new_and_generate_user("foo@bar.com", "password").await;

    let _mock_guard = Mock::given(path("/task"))
        .and(method("post"))
        .respond_with(ResponseTemplate::new(200).set_body_json(TaskId::random()))
        .named("Add a new task")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    let _ = tc
        .api_client
        .add_task(Vec::new())
        .await
        .expect("Failed to add task");
}

#[tokio::test]
async fn test_update_task() {
    let mut tc = TestClient::new_and_generate_user("foo@bar.com", "password").await;

    let task_id = TaskId::random();

    let _mock_guard = Mock::given(path(format!("/task/{}", task_id)))
        .and(method("patch"))
        .respond_with(ResponseTemplate::new(200))
        .named("Update task")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    tc.api_client
        .update_task(task_id, Some(true), None)
        .await
        .expect("Failed to update task");
}

#[tokio::test]
async fn test_update_invalid_id() {
    let mut tc = TestClient::new_and_generate_user("foo@bar.com", "password").await;

    let task_id = TaskId::random();

    let _mock_guard = Mock::given(path(format!("/task/{}", task_id)))
        .and(method("patch"))
        .respond_with(ResponseTemplate::new(404))
        .named("Update task")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    let err = tc
        .api_client
        .update_task(task_id, Some(true), None)
        .await
        .expect_err("Succeeded to update task");

    assert_eq!(err, Error::NotFound,);
}

#[tokio::test]
async fn test_delete_task() {
    let mut tc = TestClient::new_and_generate_user("foo@bar.com", "password").await;

    let _mock_guard = Mock::given(path("/task"))
        .and(method("delete"))
        .respond_with(ResponseTemplate::new(200))
        .named("Delete tasks")
        .expect(1)
        .mount_as_scoped(&tc.server)
        .await;

    tc.api_client
        .delete_tasks(&[TaskId::random()])
        .await
        .expect("Failed to delete task");
}
