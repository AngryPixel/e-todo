# etodo-api

Library which provides a REST client written with [reqwest](https://crates.io/crates/reqwest) to interact with the 
[etodo-server](../etodo-server).

Login information is stored in the `CookieStore`, which can be written to and loaded from disk to provide persistent 
sessions.