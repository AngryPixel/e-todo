# etodo-gui

Example GUI client written with [iced](https://crates.io/crates/iced) which covers all features exposed by etodo-server.


## Persistence

The client stores information about the logged in user and the server it last communicated with in the user's
configuration directory. 

# Security 

The user password and client cookie store are stored securely using [keyring](https://crates.io/crates/keyring). 
The cookies store is loaded when the api client is created. 

The user's password is retrieved from the OS's keyring on demand and only used to unlock the user's 
private key. Both the password and key are promptly discarded after the scope of operations has concluded.

## Notes

While it was interesting to write a GUI using rust only, the current features offered by 
[iced](https://crates.io/crates/iced) are still very bare-bones. For instance, there is currently no
support for additional windows or dialogs.