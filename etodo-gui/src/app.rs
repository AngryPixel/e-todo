use std::fmt::Debug;

use etodo_api::crypto::{PrivateKey, PublicKey};
use etodo_api::Secret;
use iced::widget::{row, text, Column, Row};
use iced::Color;
use iced::{alignment, widget, Application, Command, Element, Length, Subscription, Theme};
use iced_native::widget::{button, container, horizontal_space};
use iced_native::{event, keyboard, subscription, Event};

use crate::config::AppConfig;
use crate::views::{LoginView, ServerView, TasksView, Toast, View, ViewAction};
use crate::{
    config, create_subscription, icons, ClientReply, ClientRequest, InputEvent, Message,
    ELEMENT_PADDING,
};

#[derive(Debug)]
pub struct AppState {
    pub config: AppConfig,
    client: crate::client::ClientSender,
    interactive: bool,
    pub toast: Toast,
}

impl AppState {
    pub fn perform_client_request(&mut self, r: ClientRequest) {
        self.interactive = false;
        self.toast.clear();

        if self.client.try_send(r).is_err() {
            self.toast
                .set_error("Failed to communicate with client worker");
            self.interactive = true;
        }
    }

    pub fn is_interactive(&self) -> bool {
        self.interactive
    }

    pub fn unlock_user_private_key(&self) -> Result<(PublicKey, Secret<PrivateKey>), String> {
        if let Some(usr) = &self.config.user {
            if let Ok(password) = config::kr_load_user_password() {
                usr.unlock_private_key(&password)
                    .map_err(|_| "Failed to unlock private key".to_string())
                    .map(|p| (usr.public_key.clone(), p))
            } else {
                Err("Failed to retrieve user password form key ring".to_string())
            }
        } else {
            Err("Not logged in".to_string())
        }
    }
}

#[derive(Debug)]
enum AppStatus {
    LoadingAppConfig,
    LoadingClient(AppConfig),
    ConfiguringClient(AppConfig, crate::client::ClientSender),
    Loaded(AppState),
}

#[derive(Debug)]
pub struct App {
    app_status: AppStatus,
    views: Vec<Box<dyn View>>,
}

impl App {
    fn new() -> Self {
        Self {
            views: vec![],
            app_status: AppStatus::LoadingAppConfig,
        }
    }
}

impl Application for App {
    type Executor = iced::executor::Default;
    type Message = Message;
    type Theme = Theme;
    type Flags = ();

    fn new(_: Self::Flags) -> (Self, Command<Self::Message>) {
        (
            Self::new(),
            Command::perform(AppConfig::load(), Message::AppConfigLoaded),
        )
    }

    fn title(&self) -> String {
        "E-Todo".to_string()
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            Message::Input(e) => match e {
                InputEvent::TabPressed(shift) => {
                    return if shift {
                        widget::focus_previous()
                    } else {
                        widget::focus_next()
                    };
                }
            },
            Message::AppConfigSaved(r) => {
                if let Err(e) = r {
                    log::error!("Failed to save app configuration: {}", e);
                }
                return Command::none();
            }
            _ => {}
        }

        match &mut self.app_status {
            AppStatus::LoadingAppConfig => {
                if let Message::AppConfigLoaded(r) = message {
                    let app_config = match r {
                        Ok(c) => c,
                        Err(e) => {
                            log::error!("Failed to load app configuration: {}", e);
                            AppConfig::default()
                        }
                    };

                    self.app_status = AppStatus::LoadingClient(app_config);
                }
                return Command::none();
            }
            AppStatus::LoadingClient(config) => {
                if let Message::ClientReady(mut sender) = message {
                    if sender
                        .try_send(ClientRequest::Configure(
                            config.server_url.clone(),
                            config.user.clone(),
                        ))
                        .is_err()
                    {
                        log::error!("Failed to configure client, resetting state");
                        self.views.push(Box::new(ServerView::new()));
                        self.app_status = AppStatus::Loaded(AppState {
                            client: sender,
                            config: AppConfig::default(),
                            toast: Toast::Error(
                                "Failed to configure client, account information reset."
                                    .to_string(),
                            ),
                            interactive: true,
                        })
                    } else {
                        self.app_status = AppStatus::ConfiguringClient(config.clone(), sender);
                    }
                }
                return Command::none();
            }

            AppStatus::ConfiguringClient(config, client) => {
                if let Message::ClientReply(ClientReply::Configure(r)) = message {
                    match r {
                        Ok(_) => {
                            self.views
                                .push(Box::new(ServerView::with_url(&config.server_url)));

                            if !config.server_url.is_empty() {
                                if config.user.is_some() {
                                    self.views.push(Box::new(LoginView::with_email(
                                        &config.user.as_ref().unwrap().email,
                                    )));
                                    self.views.push(Box::new(TasksView::new()))
                                } else {
                                    self.views.push(Box::new(LoginView::new()));
                                }
                            }

                            let mut app_state = AppState {
                                client: client.clone(),
                                config: config.clone(),
                                toast: Toast::None,
                                interactive: true,
                            };

                            let cmd = self.views.last_mut().unwrap().on_enter(&mut app_state);

                            self.app_status = AppStatus::Loaded(app_state);

                            return cmd;
                        }
                        Err(e) => {
                            log::error!("Failed to configure client, resetting state: {}", e);
                            self.views.push(Box::new(ServerView::new()));
                            self.app_status = AppStatus::Loaded(AppState {
                                client: client.clone(),
                                config: AppConfig::default(),
                                toast: Toast::Error(
                                    "Failed to configure client, account information reset."
                                        .to_string(),
                                ),
                                interactive: true,
                            })
                        }
                    }
                }

                return Command::none();
            }
            AppStatus::Loaded(state) => {
                if let Message::ToastDismiss = message {
                    state.toast.clear();
                    return Command::none();
                }

                if let Message::ClientReply(_) = &message {
                    state.interactive = true;
                }

                let action = self
                    .views
                    .last_mut()
                    .unwrap()
                    .update(state, message.clone());

                match action {
                    ViewAction::Push(mut view) => {
                        let mut commands = Vec::with_capacity(2);
                        if let Some(v) = self.views.last_mut() {
                            commands.push(v.on_exit(state));
                        }
                        commands.push(view.on_enter(state));
                        self.views.push(view);
                        return Command::batch(commands);
                    }
                    ViewAction::Pop => {
                        let mut commands = Vec::with_capacity(2);
                        if let Some(mut v) = self.views.pop() {
                            commands.push(v.on_exit(state));
                        }
                        if let Some(v) = self.views.last_mut() {
                            commands.push(v.on_enter(state));
                        }
                        return Command::batch(commands);
                    }
                    ViewAction::None => {
                        // If no view reacted to this pop message, perform the pop ourselves.
                        if let Message::PopView = message {
                            let mut commands = Vec::with_capacity(2);
                            if let Some(mut v) = self.views.pop() {
                                commands.push(v.on_exit(state));
                            }
                            if let Some(v) = self.views.last_mut() {
                                commands.push(v.on_enter(state));
                            }
                            return Command::batch(commands);
                        }
                    }
                    ViewAction::Command(c) => {
                        return c;
                    }
                }
            }
        };

        Command::none()
    }

    fn view(&self) -> Element<Self::Message> {
        if let AppStatus::Loaded(state) = &self.app_status {
            let view = self.views.last().unwrap();

            let title = text(view.title())
                .width(Length::FillPortion(3))
                .size(60)
                .style(Color::from([0.5, 0.5, 0.5]))
                .horizontal_alignment(alignment::Horizontal::Center)
                .vertical_alignment(alignment::Vertical::Center);
            let mut title_row = Row::new()
                .padding(ELEMENT_PADDING)
                .width(Length::Fill)
                .align_items(alignment::Alignment::Center);

            title_row = if self.views.len() > 1 {
                title_row.push(
                    button(
                        row![
                            icons::chevron_left().style(Color::from([0.5, 0.5, 0.5])),
                            text(self.views[self.views.len() - 2].title())
                                .size(15)
                                .style(Color::from([0.5, 0.5, 0.5])),
                        ]
                        .align_items(alignment::Alignment::Center),
                    )
                    .style(iced::theme::Button::Text)
                    .width(Length::FillPortion(1))
                    .padding(ELEMENT_PADDING / 2)
                    .on_press(Message::PopView),
                )
            } else {
                title_row.push(horizontal_space(Length::FillPortion(1)))
            };

            title_row = title_row
                .push(title)
                .push(horizontal_space(Length::FillPortion(1)));

            let view_elements = view.display(state);

            let mut column = Column::new().push(title_row);
            if let Some(elem) = state.toast.display() {
                column = column.push(elem);
            }
            column.push(view_elements).into()
        } else {
            container(icons::hourglass())
                .width(Length::Fill)
                .height(Length::Fill)
                .center_x()
                .center_y()
                .into()
        }
    }

    fn subscription(&self) -> Subscription<Self::Message> {
        Subscription::batch(vec![
            if !matches!(self.app_status, AppStatus::LoadingAppConfig) {
                create_subscription()
            } else {
                Subscription::none()
            },
            subscription::events_with(|event, status| match (event, status) {
                (
                    Event::Keyboard(keyboard::Event::KeyPressed {
                        key_code: keyboard::KeyCode::Tab,
                        modifiers,
                        ..
                    }),
                    event::Status::Ignored,
                ) => Some(Message::Input(InputEvent::TabPressed(modifiers.shift()))),
                _ => None,
            }),
        ])
    }

    fn theme(&self) -> Self::Theme {
        Theme::Dark
    }
}
