use iced::widget::{text, Text};
use iced::{alignment, Font, Length};

const ICONS: Font = Font::External {
    name: "Icons",
    bytes: include_bytes!("../font/Font Awesome 6 Free-Solid-900.otf"),
};

fn icon(unicode: char) -> Text<'static> {
    text(unicode.to_string())
        .font(ICONS)
        .width(Length::Units(20))
        .horizontal_alignment(alignment::Horizontal::Center)
}

pub fn edit() -> Text<'static> {
    icon('\u{F303}')
}

pub fn delete() -> Text<'static> {
    icon('\u{F1F8}')
}

pub fn refresh() -> Text<'static> {
    icon('\u{F2F1}')
}

/*
pub fn logout() -> Text<'static> {
    icon('\u{F08B}')
}*/

pub fn xmark() -> Text<'static> {
    icon('\u{F00D}')
}

pub fn hourglass() -> Text<'static> {
    icon('\u{E41B}')
}

pub fn settings() -> Text<'static> {
    icon('\u{F013}')
}

/*
pub fn chevron_right() -> Text<'static> {
    icon('\u{F054}')
}*/

pub fn chevron_left() -> Text<'static> {
    icon('\u{F053}')
}
