pub const WIDGET_PADDING: u16 = 15;
pub const ELEMENT_PADDING: u16 = 20;
pub const ELEMENT_SPACING: u16 = 20;
