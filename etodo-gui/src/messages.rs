use crate::config::AppConfig;
use crate::views::{TaskFilter, TaskMessage};

#[derive(Debug, Clone)]
pub enum InputEvent {
    TabPressed(bool),
}

#[derive(Debug, Clone)]
pub enum Message {
    AppConfigLoaded(Result<AppConfig, String>),
    AppConfigSaved(Result<(), String>),
    ClientReady(crate::client::ClientSender),
    ClientReply(crate::ClientReply),
    EmailInputChanged(String),
    PasswordCurrentInputChanged(etodo_api::Secret<String>),
    PasswordInputChanged(etodo_api::Secret<String>),
    PasswordRepeatInputChanged(etodo_api::Secret<String>),
    ServerInputChanged(String),
    LoginPressed,
    RegisterPressed,
    CancelPressed,
    ConnectPressed,
    Input(InputEvent),
    TaskInputChanged(String),
    TaskEditInputChanged(String),
    TaskCreatePressed,
    TaskMessage((usize, TaskMessage)),
    TaskFilterChanged(TaskFilter),
    TaskRefreshPressed,
    SettingsPressed,
    UpdateEmailPressed,
    UpdatePasswordPressed,
    DeleteUserPressed,
    PopView,
    ToastDismiss,
}
