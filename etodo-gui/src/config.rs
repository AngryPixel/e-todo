use std::path::PathBuf;

use etodo_api::domain::User;
use etodo_api::{CookieStore, ExposeSecret, Secret};
use tokio::io::AsyncWriteExt;

#[derive(Debug, serde::Serialize, serde::Deserialize, Clone, Default)]
pub struct AppConfig {
    pub server_url: String,
    pub user: Option<User>,
}

impl AppConfig {
    fn config_file_path() -> Result<PathBuf, String> {
        let config_dir = if let Some(d) = dirs::config_dir() {
            d
        } else {
            return Err("Failed to get config dir".to_string());
        };

        Ok(config_dir.join("etodo-gui").join("config.json"))
    }

    pub async fn load() -> Result<AppConfig, String> {
        let config_file = Self::config_file_path()?;

        let contents = match tokio::fs::read_to_string(&config_file).await {
            Ok(c) => c,
            Err(e) => {
                if e.kind() == tokio::io::ErrorKind::NotFound {
                    log::debug!("App config file does not exists");
                    return Ok(AppConfig::default());
                }
                return Err("Failed to read config file".to_string());
            }
        };

        let app_config = serde_json::from_str::<AppConfig>(&contents)
            .map_err(|_| "Failed to parser config file".to_string())?;

        Ok(app_config)
    }

    pub async fn save(config: AppConfig) -> Result<(), String> {
        log::debug!("Saving app config");
        let config_file = Self::config_file_path()?;

        tokio::fs::create_dir_all(config_file.parent().unwrap())
            .await
            .map_err(|_| "Failed to create config directory")?;

        let contents =
            serde_json::to_string(&config).map_err(|_| "Failed to generate config json")?;

        let mut file = tokio::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .mode(0o600)
            .open(config_file)
            .await
            .map_err(|_| "Failed to open config file for writing")?;

        file.write_all(contents.as_bytes())
            .await
            .map_err(|_| "Failed to write config file".to_string())?;

        log::debug!("App config saved");
        Ok(())
    }
}

const KEYRING_SERVICE_NAME: &str = "etodo-gui";
const KEYRING_USER_PASSWORD: &str = "etodo-gui-user-password";
const KEYRING_USER_COOKIES: &str = "etodo-gui-user-cookies";

fn kr_user_password_entry() -> keyring::Entry {
    keyring::Entry::new(KEYRING_SERVICE_NAME, KEYRING_USER_PASSWORD)
}

fn kr_cookies_entry() -> keyring::Entry {
    keyring::Entry::new(KEYRING_SERVICE_NAME, KEYRING_USER_COOKIES)
}

fn kr_delete_entry(entry: keyring::Entry) -> keyring::Result<()> {
    if let Err(e) = entry.delete_password() {
        return if matches!(e, keyring::Error::NoEntry) {
            Ok(())
        } else {
            Err(e)
        };
    }

    Ok(())
}

pub fn kr_store_user_password(password: Secret<String>) -> keyring::Result<()> {
    log::debug!("Storing user password in key ring");
    kr_user_password_entry().set_password(password.expose_secret())
}

pub fn kr_load_user_password() -> keyring::Result<Secret<String>> {
    log::debug!("Getting user password from keyring");
    kr_user_password_entry().get_password().map(Secret::new)
}

pub fn kr_delete_user_password() -> keyring::Result<()> {
    log::debug!("Deleting user password from keyring");
    kr_delete_entry(kr_user_password_entry())
}

pub fn kr_load_cookies() -> Result<CookieStore, String> {
    log::debug!("Getting cookies from keyring");
    match kr_cookies_entry().get_password() {
        Err(e) => {
            if let keyring::Error::NoEntry = e {
                Ok(CookieStore::new())
            } else {
                Err(e.to_string())
            }
        }
        Ok(cookie_json) => {
            #[derive(serde::Deserialize)]
            struct ConfidentialFromJson {
                cookie_store: Vec<u8>,
            }

            let object = serde_json::from_str::<ConfidentialFromJson>(&cookie_json)
                .map_err(|_| "Failed to load confidential")?;

            CookieStore::from_reader(object.cookie_store.as_slice())
                .map_err(|_| "Failed to load cookie store".to_string())
        }
    }
}

pub fn kr_store_cookies(cookie_store: CookieStore) -> Result<(), String> {
    log::debug!("Saving cookies to key ring");
    let mut cookie_store_bytes: Vec<u8> = Vec::new();
    cookie_store
        .save(&mut cookie_store_bytes)
        .map_err(|_| "Failed to serialize cookie store".to_string())?;

    #[derive(serde::Serialize)]
    struct ConfidentialToJson {
        cookie_store: Vec<u8>,
    }

    let cookie_store_json = serde_json::to_string(&ConfidentialToJson {
        cookie_store: cookie_store_bytes,
    })
    .map_err(|_| "Failed to generate cookie json".to_string())?;

    kr_cookies_entry()
        .set_password(&cookie_store_json)
        .map_err(|e| e.to_string())
}

pub fn kr_delete_cookies() -> keyring::Result<()> {
    log::debug!("Deleting cookies from key ring");
    kr_delete_entry(kr_cookies_entry())
}
