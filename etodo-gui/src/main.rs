extern crate core;

mod app;
mod client;
mod config;
mod icons;
mod messages;
mod style;
mod views;

use app::*;
use client::*;
use iced::window::Position;
use iced::{window, Application, Settings};
use messages::*;
use style::*;

use crate::app::App;

fn main() -> iced::Result {
    // Disable logs for everything but this crate: RUST_LOG=none,etodo_gui=debug
    env_logger::init();
    log::info!("Starting application");

    App::run(Settings {
        id: Some("dev.lbeernaert.e-todo".to_string()),
        window: window::Settings {
            size: (500, 720),
            position: Position::Centered,
            min_size: Some((500, 400)),
            max_size: None,
            resizable: true,
            decorations: true,
            transparent: false,
            always_on_top: false,
            visible: true,
            icon: None,
        },
        flags: (),
        default_font: Default::default(),
        default_text_size: 20,
        text_multithreading: false,
        antialiasing: false,
        exit_on_close_request: true,
        try_opengles_first: false,
    })
}
