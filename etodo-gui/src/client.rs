use etodo_api::domain::{Email, Task, TaskId, User};
use etodo_api::{CookieStore, Error, Secret};
use iced::futures::channel::mpsc;
use iced::futures::StreamExt;
use iced::Subscription;

use crate::{config, Message};

#[derive(Debug, Clone)]
pub enum ClientReply {
    Configure(Result<(), Error>),
    ConnectToServer(Result<bool, Error>),
    Login(Result<User, Error>),
    CreateAccount(Result<(), Error>),
    CreateTask(Result<Task, Error>),
    GetAllTasks(Result<Vec<Task>, Error>),
    UpdateTaskFinished(Result<(TaskId, bool), Error>),
    UpdateTaskContent(Result<TaskId, Error>),
    DeleteTask(Result<TaskId, Error>),
    UpdateEmail(Result<(), Error>),
    UpdatePassword(Result<User, Error>),
    Logout(Result<(), Error>),
    DeleteAccount(Result<(), Error>),
}

#[derive(Debug)]
pub enum ClientRequest {
    Configure(String, Option<User>),
    ConnectToServer(String),
    Login((Email, Secret<String>)),
    CreateAccount((Email, Secret<String>)),
    CreateTask(Vec<u8>),
    GetAllTasks,
    UpdateTaskFinished((TaskId, bool)),
    UpdateTaskContent((TaskId, String)),
    DeleteTask(TaskId),
    UpdateEmail(Email),
    UpdatePassword((Secret<String>, Secret<String>)),
    Logout,
    DeleteAccount,
}

impl ClientRequest {
    pub fn new_server_test(uri: String) -> Self {
        Self::ConnectToServer(uri)
    }
}

#[allow(clippy::large_enum_variant)]
enum SubscriptionState {
    Starting,
    Ready(ClientState),
}

struct ClientState {
    receiver: mpsc::Receiver<ClientRequest>,
    client: Option<etodo_api::Client>,
    cookie_store: CookieStore,
}

impl Drop for ClientState {
    fn drop(&mut self) {
        if let Err(e) = config::kr_store_cookies(self.cookie_store.clone()) {
            log::error!("Failed to save cookies on drop: {}", e);
        }
    }
}

impl ClientState {
    fn new(receiver: mpsc::Receiver<ClientRequest>) -> Self {
        Self {
            receiver,
            client: None,
            cookie_store: CookieStore::default(),
        }
    }
}

fn new_api_client(uri: String, cookie_store: CookieStore) -> Result<etodo_api::Client, Error> {
    etodo_api::ClientBuilder::new()
        .server_address(&uri)
        .cookie_store(cookie_store)
        .timeout(std::time::Duration::from_secs(5))
        .connect_timeout(std::time::Duration::from_secs(5))
        .build()
}

fn new_api_client_with_user(
    uri: String,
    user: User,
    cookie_store: CookieStore,
) -> Result<etodo_api::Client, Error> {
    etodo_api::ClientBuilder::new()
        .server_address(&uri)
        .cookie_store(cookie_store)
        .user(user)
        .timeout(std::time::Duration::from_secs(5))
        .connect_timeout(std::time::Duration::from_secs(5))
        .build()
}

pub type ClientSender = mpsc::Sender<ClientRequest>;

pub fn create_subscription() -> Subscription<Message> {
    struct AppSubscription;

    iced::subscription::unfold(
        std::any::TypeId::of::<AppSubscription>(),
        SubscriptionState::Starting,
        |state| async move {
            match state {
                SubscriptionState::Starting => {
                    log::debug!("Creating client request channel");
                    let (sender, receiver) = mpsc::channel(5);

                    (
                        Some(Message::ClientReady(sender)),
                        SubscriptionState::Ready(ClientState::new(receiver)),
                    )
                }

                SubscriptionState::Ready(mut client_state) => {
                    let request = client_state.receiver.select_next_some().await;

                    match request {
                        ClientRequest::ConnectToServer(uri) => {
                            handle_server_test(client_state, uri).await
                        }
                        ClientRequest::Login((email, pwd)) => {
                            handle_login(client_state, email, pwd).await
                        }
                        ClientRequest::CreateAccount((email, pwd)) => {
                            handle_create_account(client_state, email, pwd).await
                        }
                        ClientRequest::CreateTask(task) => {
                            handle_create_task(client_state, task).await
                        }
                        ClientRequest::GetAllTasks => handle_get_all_tasks(client_state).await,
                        ClientRequest::UpdateTaskFinished((task_id, value)) => {
                            handle_update_task_finished(client_state, task_id, value).await
                        }
                        ClientRequest::DeleteTask(task_id) => {
                            handle_delete_task(client_state, task_id).await
                        }
                        ClientRequest::UpdateTaskContent((task_id, content)) => {
                            handle_update_task_content(client_state, task_id, content).await
                        }
                        ClientRequest::Logout => handle_logout(client_state).await,
                        ClientRequest::Configure(url, user) => {
                            handle_configure(url, client_state, user)
                        }
                        ClientRequest::UpdateEmail(e) => handle_update_email(client_state, e).await,
                        ClientRequest::UpdatePassword((old, new)) => {
                            handle_update_password(client_state, old, new).await
                        }
                        ClientRequest::DeleteAccount => handle_delete_account(client_state).await,
                    }
                }
            }
        },
    )
}

type HandleFnOutput = (Option<Message>, SubscriptionState);

fn handle_configure(
    server_url: String,
    mut client_state: ClientState,
    user: Option<User>,
) -> HandleFnOutput {
    log::debug!("Configuring client from app config");

    client_state.cookie_store = match config::kr_load_cookies() {
        Ok(c) => c,
        Err(e) => {
            log::error!(
                "Failed to load cookies from keyring, will use empty jar: {}",
                e
            );
            CookieStore::default()
        }
    };

    let client = if !server_url.is_empty() {
        if let Some(user) = user {
            new_api_client_with_user(server_url, user, client_state.cookie_store.clone())
        } else {
            new_api_client(server_url, client_state.cookie_store.clone())
        }
    } else {
        return (
            Some(Message::ClientReply(ClientReply::Configure(Ok(())))),
            SubscriptionState::Ready(client_state),
        );
    };

    match client {
        Ok(c) => {
            client_state.client = Some(c);
            (
                Some(Message::ClientReply(ClientReply::Configure(Ok(())))),
                SubscriptionState::Ready(client_state),
            )
        }
        Err(e) => (
            Some(Message::ClientReply(ClientReply::Configure(Err(e)))),
            SubscriptionState::Ready(client_state),
        ),
    }
}

async fn handle_server_test(mut client_state: ClientState, uri: String) -> HandleFnOutput {
    log::debug!("Check Server Uri");
    let client = match new_api_client(uri, client_state.cookie_store.clone()) {
        Ok(c) => c,
        Err(e) => {
            return (
                Some(Message::ClientReply(ClientReply::ConnectToServer(Err(e)))),
                SubscriptionState::Ready(client_state),
            );
        }
    };

    let result = client.check_server_reachable().await;

    if result.is_ok() {
        client_state.client = Some(client)
    }

    (
        Some(Message::ClientReply(ClientReply::ConnectToServer(result))),
        SubscriptionState::Ready(client_state),
    )
}

async fn handle_login(
    mut client_state: ClientState,
    email: Email,
    password: Secret<String>,
) -> HandleFnOutput {
    log::debug!("Performing login");
    if client_state.client.is_none() {
        return (
            Some(Message::ClientReply(ClientReply::Login(Err(
                Error::UnexpectedError,
            )))),
            SubscriptionState::Ready(client_state),
        );
    }

    let client = client_state.client.as_mut().unwrap();

    if let Err(e) = client.login(&email, &password).await {
        return (
            Some(Message::ClientReply(ClientReply::Login(Err(e)))),
            SubscriptionState::Ready(client_state),
        );
    }

    (
        Some(Message::ClientReply(ClientReply::Login(Ok(client
            .user()
            .unwrap()
            .clone())))),
        SubscriptionState::Ready(client_state),
    )
}

async fn handle_create_account(
    mut client_state: ClientState,
    email: Email,
    password: Secret<String>,
) -> HandleFnOutput {
    log::debug!("Creating new account");
    if client_state.client.is_none() {
        return (
            Some(Message::ClientReply(ClientReply::Login(Err(
                Error::UnexpectedError,
            )))),
            SubscriptionState::Ready(client_state),
        );
    }

    let client = client_state.client.as_mut().unwrap();

    let r = client.create_user(email, password).await.map(|_| ());

    (
        Some(Message::ClientReply(ClientReply::CreateAccount(r))),
        SubscriptionState::Ready(client_state),
    )
}

async fn handle_create_task(mut client_state: ClientState, task: Vec<u8>) -> HandleFnOutput {
    log::debug!("Creating new task");
    if client_state.client.is_none() {
        return (
            Some(Message::ClientReply(ClientReply::CreateTask(Err(
                Error::UnexpectedError,
            )))),
            SubscriptionState::Ready(client_state),
        );
    }

    let client = client_state.client.as_mut().unwrap();

    let r = client.add_task(task).await;

    (
        Some(Message::ClientReply(ClientReply::CreateTask(r))),
        SubscriptionState::Ready(client_state),
    )
}

async fn handle_get_all_tasks(mut client_state: ClientState) -> HandleFnOutput {
    log::debug!("Getting all tasks");
    if client_state.client.is_none() {
        return (
            Some(Message::ClientReply(ClientReply::GetAllTasks(Err(
                Error::UnexpectedError,
            )))),
            SubscriptionState::Ready(client_state),
        );
    }

    let client = client_state.client.as_mut().unwrap();

    let r = client.get_all_tasks().await;

    (
        Some(Message::ClientReply(ClientReply::GetAllTasks(r))),
        SubscriptionState::Ready(client_state),
    )
}

async fn handle_update_task_finished(
    mut client_state: ClientState,
    task_id: TaskId,
    value: bool,
) -> HandleFnOutput {
    log::debug!("Update task finished state");
    if client_state.client.is_none() {
        return (
            Some(Message::ClientReply(ClientReply::UpdateTaskFinished(Err(
                Error::UnexpectedError,
            )))),
            SubscriptionState::Ready(client_state),
        );
    }

    let client = client_state.client.as_mut().unwrap();

    let r = client
        .update_task(task_id, Some(value), None)
        .await
        .map(|_| (task_id, value));

    (
        Some(Message::ClientReply(ClientReply::UpdateTaskFinished(r))),
        SubscriptionState::Ready(client_state),
    )
}

async fn handle_update_task_content(
    mut client_state: ClientState,
    task_id: TaskId,
    content: String,
) -> HandleFnOutput {
    log::debug!("Update task content");
    if client_state.client.is_none() {
        return (
            Some(Message::ClientReply(ClientReply::UpdateTaskContent(Err(
                Error::UnexpectedError,
            )))),
            SubscriptionState::Ready(client_state),
        );
    }

    let client = client_state.client.as_mut().unwrap();

    if client.user().is_none() {
        return (
            Some(Message::ClientReply(ClientReply::UpdateTaskContent(Err(
                Error::NotLoggedIn,
            )))),
            SubscriptionState::Ready(client_state),
        );
    }

    let task_encrypted: Vec<u8> = if let Ok(b) = client
        .user()
        .unwrap()
        .public_key
        .encrypt(content.as_bytes())
    {
        b
    } else {
        log::error!("Failed to encrypt task data");
        return (
            Some(Message::ClientReply(ClientReply::UpdateTaskContent(Err(
                Error::CryptoError,
            )))),
            SubscriptionState::Ready(client_state),
        );
    };

    let r = client
        .update_task(task_id, None, Some(&task_encrypted))
        .await
        .map(|_| task_id);

    (
        Some(Message::ClientReply(ClientReply::UpdateTaskContent(r))),
        SubscriptionState::Ready(client_state),
    )
}

async fn handle_delete_task(mut client_state: ClientState, task_id: TaskId) -> HandleFnOutput {
    log::debug!("Deleting task");
    if client_state.client.is_none() {
        return (
            Some(Message::ClientReply(ClientReply::DeleteTask(Err(
                Error::UnexpectedError,
            )))),
            SubscriptionState::Ready(client_state),
        );
    }

    let client = client_state.client.as_mut().unwrap();

    let r = client.delete_tasks(&[task_id]).await.map(|_| task_id);

    (
        Some(Message::ClientReply(ClientReply::DeleteTask(r))),
        SubscriptionState::Ready(client_state),
    )
}

async fn handle_logout(mut client_state: ClientState) -> HandleFnOutput {
    log::debug!("Logging out");
    if client_state.client.is_none() {
        return (
            Some(Message::ClientReply(ClientReply::Logout(Err(
                Error::UnexpectedError,
            )))),
            SubscriptionState::Ready(client_state),
        );
    }

    let client = client_state.client.as_mut().unwrap();

    let r = client.logout().await;
    if r.is_ok() {
        if let Err(e) = config::kr_delete_cookies() {
            log::error!("Failed to delete cookies from keyring due to logout: {}", e);
        }
    }

    (
        Some(Message::ClientReply(ClientReply::Logout(r))),
        SubscriptionState::Ready(client_state),
    )
}

async fn handle_update_email(mut client_state: ClientState, email: Email) -> HandleFnOutput {
    log::debug!("Updating email");
    if client_state.client.is_none() {
        return (
            Some(Message::ClientReply(ClientReply::UpdateEmail(Err(
                Error::UnexpectedError,
            )))),
            SubscriptionState::Ready(client_state),
        );
    }

    let client = client_state.client.as_mut().unwrap();

    let r = client.update_email(email).await;
    (
        Some(Message::ClientReply(ClientReply::UpdateEmail(r))),
        SubscriptionState::Ready(client_state),
    )
}

async fn handle_update_password(
    mut client_state: ClientState,
    old: Secret<String>,
    new: Secret<String>,
) -> HandleFnOutput {
    log::debug!("Updating password");
    if client_state.client.is_none() {
        return (
            Some(Message::ClientReply(ClientReply::UpdatePassword(Err(
                Error::UnexpectedError,
            )))),
            SubscriptionState::Ready(client_state),
        );
    }

    let client = client_state.client.as_mut().unwrap();

    let r = client
        .update_password(old, new)
        .await
        .map(|_| client.user().unwrap().clone());
    (
        Some(Message::ClientReply(ClientReply::UpdatePassword(r))),
        SubscriptionState::Ready(client_state),
    )
}

async fn handle_delete_account(mut client_state: ClientState) -> HandleFnOutput {
    log::debug!("deleting account");
    if client_state.client.is_none() {
        return (
            Some(Message::ClientReply(ClientReply::DeleteAccount(Err(
                Error::UnexpectedError,
            )))),
            SubscriptionState::Ready(client_state),
        );
    }

    let client = client_state.client.as_mut().unwrap();

    let r = client.delete_user().await;
    if r.is_ok() {
        if let Err(e) = config::kr_store_cookies(client_state.cookie_store.clone()) {
            log::error!(
                "Failed to save cookies from keyring due to user account deletion: {}",
                e
            );
        }
    }
    (
        Some(Message::ClientReply(ClientReply::DeleteAccount(r))),
        SubscriptionState::Ready(client_state),
    )
}
