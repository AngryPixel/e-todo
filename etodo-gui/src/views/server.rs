use iced::widget::{button, container, horizontal_space, row, text, text_input};
use iced::{alignment, Alignment, Element, Length};
use iced_native::Command;

use crate::views::{LoginView, View, ViewAction};
use crate::{
    AppState, ClientReply, ClientRequest, Message, ELEMENT_PADDING, ELEMENT_SPACING, WIDGET_PADDING,
};

#[derive(Debug)]
pub struct ServerView {
    server_uri: String,
    server_input_id: text_input::Id,
}

impl ServerView {
    pub fn new() -> Self {
        Self {
            server_uri: String::new(),
            server_input_id: text_input::Id::unique(),
        }
    }

    pub fn with_url(server_uri: &str) -> Self {
        Self {
            server_uri: server_uri.to_string(),
            server_input_id: text_input::Id::unique(),
        }
    }
}

impl View for ServerView {
    fn title(&self) -> &str {
        "Sever"
    }

    fn on_enter(&mut self, _: &mut AppState) -> Command<Message> {
        text_input::focus(self.server_input_id.clone())
    }

    fn update(&mut self, app_state: &mut AppState, message: Message) -> ViewAction {
        match message {
            Message::ServerInputChanged(s) => {
                self.server_uri = s;
            }
            Message::ConnectPressed => {
                app_state.perform_client_request(ClientRequest::new_server_test(
                    self.server_uri.clone(),
                ));
            }
            Message::ClientReply(ClientReply::ConnectToServer(r)) => match r {
                Ok(b) => {
                    if !b {
                        app_state.toast.set_error("Failed to connect to server")
                    } else {
                        app_state.config.server_url = self.server_uri.clone();
                        return ViewAction::Push(Box::new(LoginView::new()));
                    }
                }
                Err(e) => app_state.toast.set_error(e.to_string()),
            },
            _ => {}
        }

        ViewAction::None
    }

    fn display(&self, app_state: &AppState) -> Element<'_, Message> {
        let server_label = text("Server: ")
            .horizontal_alignment(alignment::Horizontal::Left)
            .width(Length::Units(100));
        let server_input = text_input("Server Url ", &self.server_uri, Message::ServerInputChanged)
            .padding(WIDGET_PADDING)
            .id(self.server_input_id.clone())
            .width(Length::Fill);

        let server_row = row![server_label, server_input]
            .align_items(Alignment::Center)
            .spacing(ELEMENT_PADDING);

        let mut connect_button = button("Connect").padding(WIDGET_PADDING);
        if app_state.is_interactive() {
            connect_button = connect_button.on_press(Message::ConnectPressed);
        }

        let space = horizontal_space(Length::Fill);

        let button_row = row![space, connect_button]
            .align_items(Alignment::Center)
            .width(Length::Fill)
            .spacing(ELEMENT_SPACING);

        let elements = iced::widget::Column::new()
            .push(server_row)
            .push(button_row)
            .spacing(ELEMENT_SPACING)
            .width(Length::Units(500))
            .align_items(Alignment::Center);

        container(elements)
            .width(Length::Fill)
            .height(Length::Fill)
            .center_y()
            .center_x()
            .padding(ELEMENT_PADDING)
            .into()
    }
}
