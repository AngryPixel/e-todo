use etodo_api::domain::Email;
use etodo_api::{ExposeSecret, Secret};
use iced::widget::{button, container, horizontal_space, row, text, text_input, Column};
use iced::{alignment, Alignment, Element, Length};
use iced_native::Command;

use crate::views::{RegisterView, TasksView, View, ViewAction};
use crate::{
    config, AppState, ClientReply, ClientRequest, Message, ELEMENT_PADDING, ELEMENT_SPACING,
    WIDGET_PADDING,
};

#[derive(Debug)]
pub struct LoginView {
    email: String,
    password: Secret<String>,
    email_input_id: text_input::Id,
}

impl LoginView {
    pub fn new() -> Self {
        Self {
            email: String::new(),
            password: Secret::new(String::new()),
            email_input_id: text_input::Id::unique(),
        }
    }

    pub fn with_email(email: &Email) -> Self {
        Self {
            email: email.as_ref().to_string(),
            password: Secret::new(String::new()),
            email_input_id: text_input::Id::unique(),
        }
    }
}

impl View for LoginView {
    fn title(&self) -> &str {
        "Login"
    }

    fn on_enter(&mut self, _: &mut AppState) -> Command<Message> {
        text_input::focus(self.email_input_id.clone())
    }

    fn update(&mut self, app_state: &mut AppState, message: Message) -> ViewAction {
        match message {
            Message::EmailInputChanged(s) => {
                self.email = s;
            }
            Message::PasswordInputChanged(s) => {
                self.password = s;
            }
            Message::RegisterPressed => {
                self.password = Secret::new(String::new());
                self.email.clear();
                return ViewAction::Push(Box::new(RegisterView::new()));
            }
            Message::LoginPressed => {
                let email = match Email::parse(&self.email) {
                    Ok(e) => e,
                    Err(_) => {
                        app_state.toast.set_error("Email is not valid");
                        return ViewAction::None;
                    }
                };

                app_state
                    .perform_client_request(ClientRequest::Login((email, self.password.clone())));

                return ViewAction::None;
            }
            Message::ClientReply(ClientReply::Login(r)) => match r {
                Ok(b) => {
                    self.email.clear();
                    app_state.config.user = Some(b);

                    if let Err(e) = config::kr_store_user_password(std::mem::replace(
                        &mut self.password,
                        Secret::new(String::new()),
                    )) {
                        log::error!("Failed to store password in keyring: {}", e);
                    }
                    return ViewAction::Push(Box::new(TasksView::new()));
                }
                Err(e) => match e {
                    etodo_api::Error::InvalidParameters => {
                        app_state.toast.set_error("Credentials Invalid");
                    }
                    _ => app_state.toast.set_error(e),
                },
            },
            _ => {}
        }
        ViewAction::None
    }

    fn display(&self, app_state: &AppState) -> Element<'_, Message> {
        let email_label = text("Email: ")
            .horizontal_alignment(alignment::Horizontal::Left)
            .width(Length::Units(100));
        let email_input = text_input("user email", &self.email, Message::EmailInputChanged)
            .padding(WIDGET_PADDING)
            .on_submit(Message::LoginPressed)
            .id(self.email_input_id.clone())
            .width(Length::Fill);

        let password_label = text("Password: ")
            .horizontal_alignment(alignment::Horizontal::Left)
            .width(Length::Units(100));
        let password_input = text_input("user password", self.password.expose_secret(), |x| {
            Message::PasswordInputChanged(Secret::new(x))
        })
        .on_submit(Message::LoginPressed)
        .padding(WIDGET_PADDING)
        .password()
        .width(Length::Fill);

        let login_row = row![email_label, email_input]
            .align_items(Alignment::Center)
            .width(Length::Fill)
            .spacing(ELEMENT_SPACING);
        let password_row = row![password_label, password_input]
            .align_items(Alignment::Center)
            .width(Length::Fill)
            .spacing(ELEMENT_SPACING);

        let mut login_button = button("Login")
            .padding(WIDGET_PADDING)
            .width(Length::Shrink);
        if app_state.is_interactive() {
            login_button = login_button.on_press(Message::LoginPressed);
        }

        let mut register_button = button("Register")
            .padding(WIDGET_PADDING)
            .width(Length::Shrink)
            .style(iced::theme::Button::Secondary);
        if app_state.is_interactive() {
            register_button = register_button.on_press(Message::RegisterPressed);
        }

        let space = horizontal_space(Length::Fill);

        let button_row = row![space, register_button, login_button]
            .align_items(Alignment::Center)
            .width(Length::Fill)
            .spacing(ELEMENT_SPACING);

        let elements = Column::new()
            .push(login_row)
            .push(password_row)
            .push(button_row)
            .spacing(ELEMENT_SPACING)
            .width(Length::Units(500))
            .align_items(Alignment::Center);

        container(elements)
            .width(Length::Fill)
            .height(Length::Fill)
            .center_y()
            .center_x()
            .padding(ELEMENT_PADDING)
            .into()
    }
}
