use iced::alignment::Horizontal;
use iced::theme::{Container, Text};
use iced::widget::{button, container, horizontal_space, row};
use iced::{Alignment, Color, Element, Length, Theme};

use crate::{icons, Message, ELEMENT_PADDING};

#[derive(Debug)]
pub enum Toast {
    None,
    Error(String),
    Info(String),
}

const INFO_COLOR: Color = Color {
    r: 0.5,
    g: 0.5,
    b: 1.0,
    a: 1.0,
};
const ERROR_COLOR: Color = Color {
    r: 1.0,
    g: 0.0,
    b: 0.0,
    a: 1.0,
};

fn container_style_info(_: &Theme) -> container::Appearance {
    container::Appearance {
        text_color: None,
        background: None,
        border_radius: 0.0,
        border_width: 1.0,
        border_color: INFO_COLOR,
    }
}

fn container_style_error(_: &Theme) -> container::Appearance {
    container::Appearance {
        text_color: None,
        background: None,
        border_radius: 0.0,
        border_width: 1.0,
        border_color: ERROR_COLOR,
    }
}

impl Toast {
    pub fn clear(&mut self) {
        *self = Toast::None
    }

    pub fn set_error<T: ToString>(&mut self, err: T) {
        *self = Toast::Error(err.to_string())
    }

    pub fn set_info<T: ToString>(&mut self, err: T) {
        *self = Toast::Info(err.to_string())
    }

    fn to_element(
        text: &str,
        color: Color,
        container_style: fn(&Theme) -> container::Appearance,
    ) -> Element<Message> {
        let dismiss_button = button(icons::xmark().style(Text::Color(color)))
            .padding(ELEMENT_PADDING / 2)
            .on_press(Message::ToastDismiss)
            .width(Length::FillPortion(1))
            .style(iced::theme::Button::Text);
        let space = horizontal_space(Length::FillPortion(1));
        let text = iced::widget::text(text)
            .horizontal_alignment(Horizontal::Center)
            .width(Length::FillPortion(10))
            .style(Text::Color(color));

        let row = row![space, text, dismiss_button]
            .align_items(Alignment::Center)
            .width(Length::Fill);

        container(
            container(row)
                .width(Length::Fill)
                .style(Container::Custom(container_style)),
        )
        .width(Length::Fill)
        .padding(ELEMENT_PADDING / 2)
        .into()
    }

    pub fn display(&self) -> Option<Element<Message>> {
        match self {
            Toast::None => None,
            Toast::Error(e) => Some(Self::to_element(e, ERROR_COLOR, container_style_error)),
            Toast::Info(i) => Some(Self::to_element(i, INFO_COLOR, container_style_info)),
        }
    }
}
