use etodo_api::crypto::{CryptoError, PrivateKey, PublicKey};
use etodo_api::domain::{Task, TaskId};
use etodo_api::{ExposeSecret, Secret};
use iced::widget::{column, row, text_input, Column};
use iced::{Alignment, Element, Length};
use iced_native::widget::{button, checkbox, container, horizontal_space, scrollable};
use iced_native::Command;

use crate::config::AppConfig;
use crate::views::{SettingsView, View, ViewAction};
use crate::{
    config, icons, AppState, ClientReply, ClientRequest, Message, ELEMENT_PADDING, ELEMENT_SPACING,
    WIDGET_PADDING,
};

#[derive(Debug, Clone)]
pub struct DecryptedTask {
    id: TaskId,
    content: String,
    finished: bool,
}

impl DecryptedTask {
    pub fn new(id: TaskId, content: String, finished: bool) -> Self {
        Self {
            id,
            content,
            finished,
        }
    }

    pub fn from_task(
        task: &Task,
        public_key: &PublicKey,
        private_key: &Secret<PrivateKey>,
    ) -> Result<Self, CryptoError> {
        match private_key.expose_secret().decrypt(public_key, &task.data) {
            Ok(bytes) => {
                if let Ok(s) = String::from_utf8(bytes) {
                    Ok(DecryptedTask::new(task.id, s, task.finished))
                } else {
                    Err(CryptoError::TypeConversionFailed)
                }
            }
            Err(e) => Err(e),
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum TaskFilter {
    All,
    Done,
    Todo,
}

#[derive(Debug)]
pub struct TasksView {
    new_task_input: String,
    tasks: Vec<DecryptedTask>,
    create_task_input_id: text_input::Id,
    task_in_edit: Option<usize>,
    task_edit_state: String,
    task_filter: TaskFilter,
}

impl TasksView {
    pub fn new() -> Self {
        Self {
            new_task_input: String::new(),
            tasks: Vec::new(),
            create_task_input_id: text_input::Id::unique(),
            task_in_edit: None,
            task_edit_state: String::new(),
            task_filter: TaskFilter::All,
        }
    }

    fn handle_error(app_state: &mut AppState, error: etodo_api::Error) -> ViewAction {
        let action = if error == etodo_api::Error::NotLoggedIn {
            app_state.toast.set_error("Session Expired or Invalid");
            ViewAction::Pop
        } else {
            ViewAction::None
        };

        app_state.toast.set_error(error);

        action
    }
}

async fn noop() {}

impl View for TasksView {
    fn title(&self) -> &str {
        "Todo"
    }

    fn on_enter(&mut self, app_state: &mut AppState) -> Command<Message> {
        if app_state.config.user.is_none() {
            self.tasks.clear();
            return Command::perform(noop(), |_| Message::PopView);
        }

        if self.tasks.is_empty() {
            app_state.perform_client_request(ClientRequest::GetAllTasks);
        }

        Command::batch([
            text_input::focus(self.create_task_input_id.clone()),
            Command::perform(
                AppConfig::save(app_state.config.clone()),
                Message::AppConfigSaved,
            ),
        ])
    }

    fn on_exit(&mut self, state: &mut AppState) -> Command<Message> {
        Command::perform(
            AppConfig::save(state.config.clone()),
            Message::AppConfigSaved,
        )
    }

    fn update(&mut self, app_state: &mut AppState, message: Message) -> ViewAction {
        if app_state.config.user.is_none() {
            return ViewAction::Pop;
        }

        match message {
            Message::PopView => {
                app_state.perform_client_request(ClientRequest::Logout);
                if let Err(e) = config::kr_delete_user_password() {
                    log::error!("Failed to delete password from key ring: {}", e);
                }
                app_state.config.user = None;
                return ViewAction::Pop;
            }
            Message::TaskFilterChanged(filter) => {
                self.task_filter = filter;
            }
            Message::TaskInputChanged(s) => {
                self.new_task_input = s;
            }
            Message::TaskEditInputChanged(s) => {
                self.task_edit_state = s;
            }
            Message::TaskCreatePressed => {
                if self.new_task_input.is_empty() {
                    app_state.toast.set_error("Empty task input");
                    return ViewAction::None;
                }

                let encrypted = match app_state
                    .config
                    .user
                    .as_ref()
                    .unwrap()
                    .public_key
                    .encrypt(self.new_task_input.as_bytes())
                {
                    Ok(e) => e,
                    Err(_) => {
                        app_state.toast.set_error("Failed to encrypt task message");
                        return ViewAction::None;
                    }
                };

                app_state.perform_client_request(ClientRequest::CreateTask(encrypted));
            }
            Message::TaskMessage((index, message)) => match message {
                TaskMessage::FinishedToggled(task_id, value) => {
                    app_state.perform_client_request(ClientRequest::UpdateTaskFinished((
                        task_id, value,
                    )));
                }
                TaskMessage::Delete(task_id) => {
                    app_state.perform_client_request(ClientRequest::DeleteTask(task_id));
                }
                TaskMessage::EditBegin => {
                    if let Some(t) = self.tasks.get(index) {
                        self.task_edit_state = t.content.clone();
                        self.task_in_edit = Some(index);
                        return ViewAction::Command(text_input::focus(text_input::Id::new(
                            t.id.to_string(),
                        )));
                    }
                }
                TaskMessage::EditEnd(task_id) => {
                    if let Some(t) = self.tasks.get(index) {
                        if t.content == self.task_edit_state {
                            self.task_in_edit = None;
                            return ViewAction::None;
                        }
                    }

                    app_state.perform_client_request(ClientRequest::UpdateTaskContent((
                        task_id,
                        self.task_edit_state.clone(),
                    )));
                }
            },
            Message::SettingsPressed => {
                return ViewAction::Push(Box::new(SettingsView::new()));
            }
            Message::TaskRefreshPressed => {
                app_state.perform_client_request(ClientRequest::GetAllTasks);
            }

            Message::ClientReply(r) => match r {
                ClientReply::CreateTask(r) => match r {
                    Ok(t) => {
                        let (pub_key, priv_key) = match app_state.unlock_user_private_key() {
                            Ok(k) => k,
                            Err(e) => {
                                app_state.toast.set_error(e);
                                return ViewAction::None;
                            }
                        };

                        let decrypted_task = match DecryptedTask::from_task(&t, &pub_key, &priv_key)
                        {
                            Ok(dt) => dt,
                            Err(e) => {
                                app_state.toast.set_error(e);
                                return ViewAction::None;
                            }
                        };

                        self.tasks.push(decrypted_task);
                        self.new_task_input.clear();
                    }
                    Err(e) => {
                        return Self::handle_error(app_state, e);
                    }
                },
                ClientReply::GetAllTasks(r) => {
                    let tasks = match r {
                        Ok(tasks) => tasks,
                        Err(e) => {
                            return Self::handle_error(app_state, e);
                        }
                    };

                    let (pub_key, priv_key) = match app_state.unlock_user_private_key() {
                        Ok(k) => k,
                        Err(e) => {
                            app_state.toast.set_error(e);
                            return ViewAction::None;
                        }
                    };

                    self.tasks.clear();
                    self.tasks.reserve(tasks.len());

                    for t in tasks {
                        let decrypted_task = match DecryptedTask::from_task(&t, &pub_key, &priv_key)
                        {
                            Ok(dt) => dt,
                            Err(e) => {
                                self.tasks.clear();
                                app_state.toast.set_error(e);
                                return ViewAction::None;
                            }
                        };
                        self.tasks.push(decrypted_task);
                    }
                }
                ClientReply::UpdateTaskFinished(r) => match r {
                    Err(e) => {
                        return Self::handle_error(app_state, e);
                    }
                    Ok((task_id, value)) => {
                        for task in &mut self.tasks {
                            if task.id == task_id {
                                task.finished = value;
                                break;
                            }
                        }
                    }
                },
                ClientReply::DeleteTask(r) => match r {
                    Ok(task_id) => {
                        let index = {
                            let mut index = None;
                            for (i, task) in self.tasks.iter().enumerate() {
                                if task.id == task_id {
                                    index = Some(i);
                                    break;
                                }
                            }
                            index
                        };

                        if let Some(index) = index {
                            self.tasks.remove(index);
                        }

                        self.task_in_edit = None;
                        self.task_edit_state.clear();
                    }
                    Err(e) => {
                        return Self::handle_error(app_state, e);
                    }
                },
                ClientReply::UpdateTaskContent(r) => match r {
                    Ok(task_id) => {
                        for task in &mut self.tasks {
                            if task.id == task_id {
                                task.content = self.task_edit_state.clone();
                                break;
                            }
                        }

                        self.task_edit_state.clear();
                        self.task_in_edit = None;
                    }
                    Err(e) => {
                        return Self::handle_error(app_state, e);
                    }
                },
                ClientReply::Logout(r) => match r {
                    Ok(_) => {
                        if let Err(e) = config::kr_delete_user_password() {
                            log::error!("Failed to delete password from key ring: {}", e);
                        }
                        app_state.config.user = None;
                        return ViewAction::Pop;
                    }
                    Err(e) => {
                        return Self::handle_error(app_state, e);
                    }
                },
                _ => {}
            },
            _ => {}
        }

        ViewAction::None
    }

    fn display(&self, app_state: &AppState) -> Element<Message> {
        let input = {
            let mut input = text_input(
                "What needs to be done?",
                &self.new_task_input,
                Message::TaskInputChanged,
            )
            .id(self.create_task_input_id.clone())
            .size(30)
            .padding(ELEMENT_PADDING);
            if app_state.is_interactive() {
                input = input.on_submit(Message::TaskCreatePressed);
            }

            input
        };

        let mut column = column![input];
        column = column.spacing(ELEMENT_SPACING);

        column = column.push(build_filter_view(self.task_filter));

        let tasks: Vec<Element<Message>> = self
            .tasks
            .iter()
            .filter(|&task| match self.task_filter {
                TaskFilter::All => true,
                TaskFilter::Todo => !task.finished,
                TaskFilter::Done => task.finished,
            })
            .enumerate()
            .map(|(index, task)| {
                build_task_view(
                    &self.task_edit_state,
                    index,
                    task,
                    self.task_in_edit,
                    app_state.is_interactive(),
                )
            })
            .collect();

        let task_list = Column::with_children(tasks)
            .spacing(ELEMENT_SPACING)
            .padding(ELEMENT_PADDING);

        let task_container = scrollable(task_list);

        column = column.push(task_container);

        container(column)
            .width(Length::Fill)
            .height(Length::Fill)
            .center_x()
            .padding(ELEMENT_PADDING)
            .into()
    }
}

#[derive(Debug, Clone)]
pub enum TaskMessage {
    FinishedToggled(TaskId, bool),
    Delete(TaskId),
    EditBegin,
    EditEnd(TaskId),
}

fn build_task_view<'a>(
    edit_state: &'a str,
    index: usize,
    task: &'a DecryptedTask,
    edit_index: Option<usize>,
    interactive: bool,
) -> Element<'a, Message> {
    let space = horizontal_space(Length::Units(ELEMENT_SPACING));

    if edit_index == Some(index) {
        let text_box = text_input("Update task", edit_state, Message::TaskEditInputChanged)
            .width(Length::Fill)
            .padding(WIDGET_PADDING)
            .id(text_input::Id::new(task.id.to_string()))
            .on_submit(Message::TaskMessage((index, TaskMessage::EditEnd(task.id))));

        let mut delete_button = button(icons::delete())
            .style(iced::theme::Button::Destructive)
            .padding(ELEMENT_PADDING);
        if interactive {
            delete_button =
                delete_button.on_press(Message::TaskMessage((index, TaskMessage::Delete(task.id))))
        }

        row![text_box, space, delete_button]
            .width(Length::Fill)
            .align_items(Alignment::Center)
            .into()
    } else {
        let check_box = checkbox(&task.content, task.finished, move |b| {
            Message::TaskMessage((index, TaskMessage::FinishedToggled(task.id, b)))
        })
        .spacing(ELEMENT_SPACING)
        .width(Length::Fill)
        .style(iced::theme::Checkbox::Primary);

        let mut edit_button = button(icons::edit())
            .style(iced::theme::Button::Text)
            .padding(ELEMENT_PADDING);
        if interactive {
            edit_button =
                edit_button.on_press(Message::TaskMessage((index, TaskMessage::EditBegin)))
        }

        row![check_box, space, edit_button]
            .width(Length::Fill)
            .align_items(Alignment::Center)
            .into()
    }
}

fn build_filter_view(filter: TaskFilter) -> Element<'static, Message> {
    let all_button = button("All")
        .padding(ELEMENT_PADDING / 2)
        .style(if filter == TaskFilter::All {
            iced::theme::Button::Primary
        } else {
            iced::theme::Button::Text
        })
        .on_press(Message::TaskFilterChanged(TaskFilter::All));

    let todo_button = button("To Do")
        .padding(ELEMENT_PADDING / 2)
        .style(if filter == TaskFilter::Todo {
            iced::theme::Button::Primary
        } else {
            iced::theme::Button::Text
        })
        .on_press(Message::TaskFilterChanged(TaskFilter::Todo));

    let completed_button = button("Done")
        .padding(ELEMENT_PADDING / 2)
        .style(if filter == TaskFilter::Done {
            iced::theme::Button::Primary
        } else {
            iced::theme::Button::Text
        })
        .on_press(Message::TaskFilterChanged(TaskFilter::Done));

    let settings_button = button(icons::settings())
        .padding(ELEMENT_PADDING / 2)
        .style(iced::theme::Button::Text)
        .on_press(Message::SettingsPressed);

    let refresh_button = button(icons::refresh())
        .padding(ELEMENT_PADDING / 2)
        .style(iced::theme::Button::Text)
        .on_press(Message::TaskRefreshPressed);

    let space1 = horizontal_space(Length::Fill);
    let space2 = horizontal_space(Length::Fill);

    row![
        refresh_button,
        space1,
        all_button,
        todo_button,
        completed_button,
        space2,
        settings_button,
    ]
    .align_items(Alignment::Center)
    .spacing(ELEMENT_SPACING / 2)
    .into()
}
