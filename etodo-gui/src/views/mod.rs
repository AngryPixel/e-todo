mod login;
mod register;
mod server;
mod settings;
mod tasks;
mod toast;

use std::fmt::Debug;

use iced::{Command, Element};
pub use login::*;
pub use register::*;
pub use server::*;
pub use settings::*;
pub use tasks::*;
pub use toast::*;

use crate::Message;

pub enum ViewAction {
    None,
    Push(Box<dyn View>),
    Pop,
    Command(Command<Message>),
}

pub trait View: Debug {
    fn title(&self) -> &str;

    fn update(&mut self, state: &mut crate::AppState, message: Message) -> ViewAction;

    fn display(&self, state: &crate::AppState) -> Element<'_, Message>;

    fn on_enter(&mut self, _: &mut crate::AppState) -> Command<Message> {
        Command::none()
    }

    fn on_exit(&mut self, _: &mut crate::AppState) -> Command<Message> {
        Command::none()
    }
}
