use etodo_api::domain::Email;
use etodo_api::{ExposeSecret, Secret};
use iced::widget::{button, container, row, text, text_input, Column};
use iced::{alignment, Alignment, Element, Length};
use iced_native::widget::{scrollable, vertical_space};
use iced_native::Command;

use crate::config::AppConfig;
use crate::views::{View, ViewAction};
use crate::{
    config, AppState, ClientReply, ClientRequest, Message, ELEMENT_PADDING, ELEMENT_SPACING,
    WIDGET_PADDING,
};

#[derive(Debug)]
pub struct SettingsView {
    email: String,
    password_old: Secret<String>,
    password_new: Secret<String>,
    password_new_repeat: Secret<String>,
}

impl SettingsView {
    pub fn new() -> Self {
        Self {
            email: String::new(),
            password_old: Secret::new(String::new()),
            password_new: Secret::new(String::new()),
            password_new_repeat: Secret::new(String::new()),
        }
    }
}

impl View for SettingsView {
    fn title(&self) -> &str {
        "Settings"
    }

    fn update(&mut self, app_state: &mut crate::AppState, message: Message) -> ViewAction {
        match message {
            Message::EmailInputChanged(s) => {
                self.email = s;
            }
            Message::PasswordCurrentInputChanged(s) => {
                self.password_old = s;
            }
            Message::PasswordInputChanged(s) => {
                self.password_new = s;
            }
            Message::PasswordRepeatInputChanged(s) => {
                self.password_new_repeat = s;
            }
            Message::UpdateEmailPressed => {
                let email = match Email::parse(&self.email) {
                    Ok(e) => e,
                    Err(_) => {
                        app_state.toast.set_error("Email is not valid");
                        return ViewAction::None;
                    }
                };

                app_state.perform_client_request(ClientRequest::UpdateEmail(email));
            }
            Message::UpdatePasswordPressed => {
                if self.password_new.expose_secret().is_empty()
                    || self.password_new_repeat.expose_secret().is_empty()
                    || self.password_old.expose_secret().is_empty()
                {
                    app_state.toast.set_error("Password can't be empty");
                    return ViewAction::None;
                }

                if self.password_new_repeat.expose_secret() != self.password_new.expose_secret() {
                    app_state.toast.set_error("New passwords don't match");
                    return ViewAction::None;
                }

                app_state.perform_client_request(ClientRequest::UpdatePassword((
                    self.password_old.clone(),
                    self.password_new.clone(),
                )));
            }
            Message::DeleteUserPressed => {
                app_state.perform_client_request(ClientRequest::DeleteAccount);
            }
            Message::ClientReply(reply) => match reply {
                ClientReply::UpdateEmail(r) => match r {
                    Ok(_) => {
                        app_state.config.user.as_mut().unwrap().email =
                            Email::parse(&self.email).unwrap();
                        self.email.clear();
                        app_state.toast.set_info("Email updated successfully");
                        return ViewAction::Command(Command::perform(
                            AppConfig::save(app_state.config.clone()),
                            Message::AppConfigSaved,
                        ));
                    }
                    Err(e) => {
                        app_state.toast.set_error(e);
                    }
                },
                ClientReply::UpdatePassword(r) => match r {
                    Ok(user) => {
                        app_state.toast.set_info("Password updated successfully");
                        app_state.config.user = Some(user);
                        if let Err(e) = config::kr_store_user_password(std::mem::replace(
                            &mut self.password_new,
                            Secret::new(String::new()),
                        )) {
                            log::error!("Failed to store password in keyring: {}", e);
                        }
                        self.password_old = Secret::new(String::new());
                        self.password_new_repeat = Secret::new(String::new());
                        return ViewAction::Command(Command::perform(
                            AppConfig::save(app_state.config.clone()),
                            Message::AppConfigSaved,
                        ));
                    }
                    Err(e) => {
                        app_state.toast.set_error(e);
                    }
                },
                ClientReply::DeleteAccount(r) => match r {
                    Ok(_) => {
                        if let Err(e) = config::kr_delete_user_password() {
                            log::error!("Failed to delete password from key ring: {}", e);
                        }
                        app_state.toast.set_info("Account deleted successfully");
                        app_state.config.user = None;
                        return ViewAction::Pop;
                    }
                    Err(e) => {
                        app_state.toast.set_error(e);
                    }
                },
                _ => {}
            },
            _ => {}
        }
        ViewAction::None
    }

    fn display(&self, app_state: &AppState) -> Element<'_, Message> {
        let email_label = text("Email: ")
            .horizontal_alignment(alignment::Horizontal::Left)
            .width(Length::Units(150));
        let email_input = text_input("New email address", &self.email, Message::EmailInputChanged)
            .padding(WIDGET_PADDING)
            .width(Length::Fill);

        let password_current_label = text("Password(Current): ")
            .horizontal_alignment(alignment::Horizontal::Left)
            .width(Length::Units(150));

        let password_current_input =
            text_input("current password", self.password_old.expose_secret(), |x| {
                Message::PasswordCurrentInputChanged(Secret::new(x))
            })
            .padding(WIDGET_PADDING)
            .password()
            .width(Length::Fill);

        let password_input = text_input("new password", self.password_new.expose_secret(), |x| {
            Message::PasswordInputChanged(Secret::new(x))
        })
        .padding(WIDGET_PADDING)
        .password()
        .width(Length::Fill);

        let password_label = text("Password: ")
            .horizontal_alignment(alignment::Horizontal::Left)
            .width(Length::Units(150));
        let password_repeat_label = text("Password(Repeat): ")
            .horizontal_alignment(alignment::Horizontal::Left)
            .width(Length::Units(150));
        let password_repeat_input = text_input(
            "new password (repeat)",
            self.password_new_repeat.expose_secret(),
            |x| Message::PasswordRepeatInputChanged(Secret::new(x)),
        )
        .padding(WIDGET_PADDING)
        .password()
        .width(Length::Fill);

        let email_row = row![email_label, email_input]
            .align_items(Alignment::Center)
            .width(Length::Fill)
            .spacing(ELEMENT_SPACING);
        let password_current_row = row![password_current_label, password_current_input]
            .align_items(Alignment::Center)
            .width(Length::Fill)
            .spacing(ELEMENT_SPACING);
        let password_row = row![password_label, password_input]
            .align_items(Alignment::Center)
            .width(Length::Fill)
            .spacing(ELEMENT_SPACING);
        let password_repeat_row = row![password_repeat_label, password_repeat_input]
            .align_items(Alignment::Center)
            .width(Length::Fill)
            .spacing(ELEMENT_SPACING);

        let mut update_email_button = button(
            text("Update Email")
                .horizontal_alignment(alignment::Horizontal::Center)
                .width(Length::Fill),
        )
        .padding(WIDGET_PADDING)
        .width(Length::Fill);
        if app_state.is_interactive() && !self.email.is_empty() {
            update_email_button = update_email_button.on_press(Message::UpdateEmailPressed);
        }

        let mut update_password_button = button(
            text("Update Password")
                .horizontal_alignment(alignment::Horizontal::Center)
                .width(Length::Fill),
        )
        .padding(WIDGET_PADDING)
        .width(Length::Fill);
        if app_state.is_interactive() {
            update_password_button =
                update_password_button.on_press(Message::UpdatePasswordPressed);
        }

        let mut delete_account_button = button(
            text("Delete Account")
                .horizontal_alignment(alignment::Horizontal::Center)
                .width(Length::Fill),
        )
        .padding(WIDGET_PADDING)
        .width(Length::Fill)
        .style(iced::theme::Button::Destructive);
        if app_state.is_interactive() {
            delete_account_button = delete_account_button.on_press(Message::DeleteUserPressed);
        }

        let space1 = vertical_space(Length::Units(ELEMENT_SPACING));
        let space2 = vertical_space(Length::Units(ELEMENT_SPACING));

        let elements = Column::new()
            .push(email_row)
            .push(update_email_button)
            .push(space1)
            .push(password_current_row)
            .push(password_row)
            .push(password_repeat_row)
            .push(update_password_button)
            .push(space2)
            .push(delete_account_button)
            .spacing(ELEMENT_SPACING)
            .width(Length::Units(500))
            .align_items(Alignment::Center);

        container(scrollable(elements))
            .width(Length::Fill)
            .height(Length::Fill)
            .center_y()
            .center_x()
            .padding(ELEMENT_PADDING)
            .into()
    }
}
