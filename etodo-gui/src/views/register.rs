use etodo_api::domain::Email;
use etodo_api::{ExposeSecret, Secret};
use iced::widget::{button, container, horizontal_space, row, text, text_input, Column};
use iced::{alignment, Alignment, Element, Length};

use crate::views::{View, ViewAction};
use crate::{
    AppState, ClientReply, ClientRequest, Message, ELEMENT_PADDING, ELEMENT_SPACING, WIDGET_PADDING,
};

#[derive(Debug)]
pub struct RegisterView {
    email: String,
    password: Secret<String>,
    password_repeat: Secret<String>,
}

impl RegisterView {
    pub fn new() -> Self {
        Self {
            email: String::new(),
            password: Secret::new(String::new()),
            password_repeat: Secret::new(String::new()),
        }
    }
}

impl View for RegisterView {
    fn title(&self) -> &str {
        "Registration"
    }

    fn update(&mut self, app_state: &mut AppState, message: Message) -> ViewAction {
        match message {
            Message::EmailInputChanged(s) => {
                self.email = s;
            }
            Message::PasswordInputChanged(s) => {
                self.password = s;
            }
            Message::PasswordRepeatInputChanged(s) => {
                self.password_repeat = s;
            }
            Message::CancelPressed => {
                return ViewAction::Pop;
            }
            Message::RegisterPressed => {
                let email = match Email::parse(&self.email) {
                    Ok(e) => e,
                    Err(_) => {
                        app_state.toast.set_error("Email is not valid");
                        return ViewAction::None;
                    }
                };

                if self.password.expose_secret().is_empty()
                    || self.password_repeat.expose_secret().is_empty()
                {
                    app_state.toast.set_error("Password can't be empty");
                    return ViewAction::None;
                }

                if self.password_repeat.expose_secret() != self.password.expose_secret() {
                    app_state.toast.set_error("Passwords don't match");
                    return ViewAction::None;
                }

                app_state.perform_client_request(ClientRequest::CreateAccount((
                    email,
                    self.password.clone(),
                )));

                return ViewAction::None;
            }
            Message::ClientReply(ClientReply::CreateAccount(r)) => match r {
                Ok(_) => {
                    app_state.toast.set_info("Account successfully created");
                    return ViewAction::Pop;
                }
                Err(e) => app_state.toast.set_error(e),
            },
            _ => {}
        }
        ViewAction::None
    }

    fn display(&self, app_state: &AppState) -> Element<'_, Message> {
        let email_label = text("Email: ")
            .horizontal_alignment(alignment::Horizontal::Left)
            .width(Length::Units(150));
        let email_input = text_input("user email", &self.email, Message::EmailInputChanged)
            .padding(WIDGET_PADDING)
            .on_submit(Message::RegisterPressed)
            .width(Length::Fill);

        let password_label = text("Password: ")
            .horizontal_alignment(alignment::Horizontal::Left)
            .width(Length::Units(150));
        let password_input = text_input("user password", self.password.expose_secret(), |x| {
            Message::PasswordInputChanged(Secret::new(x))
        })
        .padding(WIDGET_PADDING)
        .password()
        .on_submit(Message::RegisterPressed)
        .width(Length::Fill);

        let password_repeat_label = text("Password(Repeat): ")
            .horizontal_alignment(alignment::Horizontal::Left)
            .width(Length::Units(150));
        let password_repeat_input = text_input(
            "user password (repeat)",
            self.password_repeat.expose_secret(),
            |x| Message::PasswordRepeatInputChanged(Secret::new(x)),
        )
        .padding(WIDGET_PADDING)
        .password()
        .on_submit(Message::RegisterPressed)
        .width(Length::Fill);

        let login_row = row![email_label, email_input]
            .align_items(Alignment::Center)
            .width(Length::Fill)
            .spacing(ELEMENT_SPACING);
        let password_row = row![password_label, password_input]
            .align_items(Alignment::Center)
            .width(Length::Fill)
            .spacing(ELEMENT_SPACING);
        let password_repeat_row = row![password_repeat_label, password_repeat_input]
            .align_items(Alignment::Center)
            .width(Length::Fill)
            .spacing(ELEMENT_SPACING);

        let mut register_button = button("Register")
            .padding(WIDGET_PADDING)
            .width(Length::Shrink);
        if app_state.is_interactive() {
            register_button = register_button.on_press(Message::RegisterPressed);
        }

        let mut cancel_button = button("Cancel")
            .padding(WIDGET_PADDING)
            .width(Length::Shrink)
            .style(iced::theme::Button::Secondary);
        if app_state.is_interactive() {
            cancel_button = cancel_button.on_press(Message::CancelPressed);
        }

        let space = horizontal_space(Length::Fill);

        let button_row = row![space, cancel_button, register_button]
            .align_items(Alignment::Center)
            .width(Length::Fill)
            .spacing(ELEMENT_SPACING);

        let elements = Column::new()
            .push(login_row)
            .push(password_row)
            .push(password_repeat_row)
            .push(button_row)
            .spacing(ELEMENT_SPACING)
            .width(Length::Units(500))
            .align_items(Alignment::Center);

        container(elements)
            .width(Length::Fill)
            .height(Length::Fill)
            .center_y()
            .center_x()
            .padding(ELEMENT_PADDING)
            .into()
    }
}
