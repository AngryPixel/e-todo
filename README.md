# E-Todo : End-to-End Encrypted Task/Todo List Service

End-to-end encrypted Task/Todo list which can easily be deployed to any VPS/Server with minimal configuration 
and dependencies.

All task data is locally encrypted on the client before being sent to the server.

## Disclaimer

This code has not been audited and I am not a specialist in the field of cryptography or backend 
development. Use the code in this repository at your own risk.

## Outline

* [etodo-core](etodo-core): Shared domain types and cryptographic routines 
* [etodo-api](etodo-api): Client API to interact with a etodo-server
* [etodo-server](etodo-server): Server for the todo service
* [etodo-gui](etodo-gui): Example desktop client


See each folder for more details about each project.