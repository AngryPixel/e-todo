PRAGMA foreign_keys = ON;

-- Create User Table
CREATE TABLE users(
                      id uuid NOT NULL PRIMARY KEY,
                      password TEXT NOT NULL,
                      email TEXT NOT NULL UNIQUE,
                      public_key BINARY NOT NULL,
                      encryption_key BINARY NOT NULL,
                      encryption_key_nonce BINARY NOT NULL,
                      encryption_salt BINARY NOT NULL,
                      private_key BINARY NOT NULL,
                      private_key_nonce BINARY NOT NULL,
                      login_salt BINARY NOT NULL
);

-- Create TodoList table
create TABLE todo(
                     id uuid NOT NULL PRIMARY KEY,
                     user_id uuid NOT NULL,
                     created TEXT NOT NULL, -- sqlite has no date/time field
                     last_updated TEXT NOT NULL, -- sqlite has no date/time field
                     finished INTEGER DEFAULT 0 NOT NULL,
                     data BINARY NOT NULL,
                     FOREIGN KEY (user_id)
                         REFERENCES users(id)
                         ON DELETE CASCADE
                         ON UPDATE CASCADE
);
