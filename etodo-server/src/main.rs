use etodo_server::logging::init_fmt_logger;
use etodo_server::Application;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    init_fmt_logger();
    let config = etodo_server::get_configuration()?;

    let app = Application::build(&config).await?;
    app.run_to_completion().await?;
    Ok(())
}
