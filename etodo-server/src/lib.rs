mod configuration;
pub mod db;
mod domain;
pub mod logging;
mod routes;
mod startup;

pub use configuration::*;
pub use routes::*;
pub use startup::*;
use tokio::task::JoinHandle;

// Trait bounds copied from `spawn_blocking`
pub fn spawn_blocking_with_tracing<F, R>(f: F) -> JoinHandle<R>
where
    F: FnOnce() -> R + Send + 'static,
    R: Send + 'static,
{
    let current_span = tracing::Span::current();
    tokio::task::spawn_blocking(move || current_span.in_scope(f))
}
