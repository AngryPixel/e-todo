use actix_web::{post, HttpResponse, Responder};

use crate::domain::UserSession;

#[post("/logout")]
pub(crate) async fn logout(session: UserSession) -> actix_web::Result<impl Responder> {
    session.log_out();

    Ok(HttpResponse::Ok().finish())
}
