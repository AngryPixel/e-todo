mod delete;
mod get;
mod patch;
mod post;

use actix_web::http::StatusCode;
use actix_web::ResponseError;
pub use delete::*;
pub use get::*;
pub use patch::*;
pub use post::*;

#[derive(thiserror::Error, Debug)]
pub enum UserError {
    #[error("User not authenticated")]
    NotAuthenticated(#[source] anyhow::Error),
    #[error("User not found")]
    NotFound,
    #[error("Invalid parameters")]
    InvalidParameters(#[source] anyhow::Error),
    #[error("Something went wrong")]
    UnexpectedError(#[from] anyhow::Error),
    #[error("Already registered")]
    AlreadyRegistered,
}

impl ResponseError for UserError {
    fn status_code(&self) -> StatusCode {
        match self {
            UserError::AlreadyRegistered => StatusCode::CONFLICT,
            UserError::NotFound => StatusCode::NOT_FOUND,
            UserError::NotAuthenticated(_) => StatusCode::UNAUTHORIZED,
            UserError::InvalidParameters(_) => StatusCode::BAD_REQUEST,
            UserError::UnexpectedError(_) => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}
