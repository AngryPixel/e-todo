use actix_web::{patch, web, HttpResponse, Responder};
use anyhow::anyhow;
use etodo_core::crypto::{Encrypted, EncryptionKey, Nonce, Salt};
use etodo_core::domain::Email;
use secrecy::{ExposeSecret, Secret};

use crate::db::DBClient;
use crate::domain::{AuthSaltGenerator, AuthType, AuthenticatedUser};
use crate::UserError;

#[derive(serde::Deserialize)]
pub struct PatchUserPassword {
    old_password: Secret<String>,
    new_password: Secret<String>,
    encrypted_key: Encrypted<EncryptionKey>,
    encryption_nonce: Nonce,
    encryption_salt: Salt,
}

#[derive(serde::Deserialize)]
pub struct PatchUserEmail {
    email: String,
}

#[tracing::instrument(name="Patching user email",skip(db, user, update), fields(user_id=tracing::field::Empty))]
#[patch("/user/email")]
pub(crate) async fn patch_user_email(
    db: web::Data<DBClient>,
    user: AuthenticatedUser,
    update: web::Json<PatchUserEmail>,
) -> Result<impl Responder, UserError> {
    tracing::Span::current().record("user_id", &tracing::field::display(user.id()));

    let email = Email::parse(&update.email)
        .map_err(|_| UserError::InvalidParameters(anyhow!("Invalid email")))?;

    if !db.update_user_email(user.id(), &email).await? {
        return Err(UserError::NotFound);
    }

    Ok(HttpResponse::Ok().finish())
}

#[tracing::instrument(name="Patching user password ",skip(db, user, update, salt_generator), fields(user_id=tracing::field::Empty))]
#[patch("/user/password")]
pub(crate) async fn patch_user_password(
    db: web::Data<DBClient>,
    user: AuthenticatedUser,
    update: web::Json<PatchUserPassword>,
    salt_generator: web::Data<AuthSaltGenerator>,
) -> Result<impl Responder, UserError> {
    tracing::Span::current().record("user_id", &tracing::field::display(user.id()));

    let current_salt = db.get_login_salt_from_user_id(user.id()).await?;
    let current_salt = if let Some(s) = current_salt {
        s
    } else {
        return Err(UserError::NotFound);
    };

    let update_salt = {
        if let Some(salt) = salt_generator
            .consume(&user.id().to_string(), AuthType::PasswordUpdate)
            .map_err(|e| UserError::InvalidParameters(e.into()))?
        {
            salt
        } else {
            return Err(UserError::InvalidParameters(anyhow::anyhow!(
                "No salt request for this email"
            )));
        }
    };

    let (user, password) = match db.get_user_by_id(user.id()).await? {
        Some(u) => u,
        None => return Err(UserError::NotFound),
    };

    if !etodo_core::crypto::hash_password_server_verify_with_salt(
        &password,
        update.old_password.expose_secret(),
        &current_salt,
    )
    .map_err(|_| UserError::UnexpectedError(anyhow::anyhow!("Failed to verify old password")))?
    {
        return Err(UserError::InvalidParameters(anyhow!(
            "Password doesn't match"
        )));
    }

    let new_password = etodo_core::crypto::hash_password_server_with_salt(
        update.new_password.expose_secret(),
        &update_salt,
    )
    .map_err(|e| UserError::UnexpectedError(e.into()))?;

    if !db
        .update_user_password(
            &user.id,
            &new_password,
            &update.encrypted_key,
            &update.encryption_nonce,
            &update.encryption_salt,
            &update_salt,
        )
        .await
        .map_err(UserError::UnexpectedError)?
    {
        return Err(UserError::NotFound);
    }

    Ok(HttpResponse::Ok().finish())
}
