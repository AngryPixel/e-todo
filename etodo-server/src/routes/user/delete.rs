use actix_web::{delete, web, HttpResponse, Responder};

use crate::db::DBClient;
use crate::domain::AuthenticatedUser;
use crate::UserError;

#[tracing::instrument(name="Deleting user",skip(db, user),
fields(user_id=tracing::field::Empty))]
#[delete("/user")]
pub(crate) async fn delete_user(
    db: web::Data<DBClient>,
    user: AuthenticatedUser,
) -> Result<impl Responder, UserError> {
    tracing::Span::current().record("user_id", &tracing::field::display(user.id()));

    db.delete_user(user.id()).await?;

    Ok(HttpResponse::Ok().finish())
}
