use actix_web::{get, web, Responder};
use etodo_core::crypto::Salt;
use etodo_core::domain::User;

use crate::db::DBClient;
use crate::domain::{AuthSaltGenerator, AuthType, AuthenticatedUser};
use crate::UserError;

#[tracing::instrument(name="Getting user",skip(db, user), fields(user_id=tracing::field::Empty))]
#[get("/user")]
pub(crate) async fn get_user(
    db: web::Data<DBClient>,
    user: AuthenticatedUser,
) -> Result<Option<impl Responder>, UserError> {
    tracing::Span::current().record("user_id", &tracing::field::display(user.id()));

    let user = db.get_user_by_id(user.id()).await?;

    Ok(user.map(|(u, _)| web::Json::<User>(u)))
}

#[tracing::instrument(name = "Getting password change salt",
skip(user, salt_generator),
fields(user_id=tracing::field::Empty))]
#[get("/user/salt")]
pub(crate) async fn get_password_change_salt(
    user: AuthenticatedUser,
    salt_generator: web::Data<AuthSaltGenerator>,
) -> Result<impl Responder, UserError> {
    tracing::Span::current().record("user_id", &tracing::field::display(user.id()));
    let random_salt = Salt::random();

    salt_generator.insert(
        &user.id().to_string(),
        random_salt.clone(),
        AuthType::PasswordUpdate,
    );

    Ok(web::Json(random_salt))
}
