use actix_web::{post, web, HttpResponse, Responder};
use anyhow::anyhow;
use etodo_core::crypto::{Encrypted, EncryptionKey, Nonce, PrivateKey, PublicKey, Salt};
use etodo_core::domain::{Email, User, UserId};
use secrecy::{ExposeSecret, Secret};

use crate::db::DBClient;
use crate::domain::{AuthSaltGenerator, AuthType};
use crate::UserError;

#[derive(serde::Deserialize)]
pub struct AddUserData {
    email: String,
    public_key: PublicKey,
    encryption_key: Encrypted<EncryptionKey>,
    encryption_key_nonce: Nonce,
    private_key: Encrypted<PrivateKey>,
    private_key_nonce: Nonce,
    encryption_salt: Salt,
    password: Secret<String>,
}

#[tracing::instrument(name="Adding new task",skip(db, user, salt_generator),
fields(user_id=tracing::field::Empty, task_id=tracing::field::Empty))]
#[post("/user")]
pub async fn add_user(
    db: web::Data<DBClient>,
    user: web::Json<AddUserData>,
    salt_generator: web::Data<AuthSaltGenerator>,
) -> Result<impl Responder, UserError> {
    let email = Email::parse(&user.email)
        .map_err(|_| UserError::InvalidParameters(anyhow!("Invalid email")))?;

    let login_salt = {
        if let Some(salt) = salt_generator
            .consume(&user.email, AuthType::Login)
            .map_err(|e| UserError::InvalidParameters(e.into()))?
        {
            salt
        } else {
            return Err(UserError::InvalidParameters(anyhow::anyhow!(
                "No salt request for this email"
            )));
        }
    };

    if db.get_login_salt_from_user_email(&email).await?.is_some() {
        return Err(UserError::AlreadyRegistered);
    }

    let password = user.password.clone();

    let new_user = User::with(
        email.clone(),
        user.0.public_key,
        user.0.encryption_key,
        user.0.encryption_key_nonce,
        user.0.encryption_salt,
        user.0.private_key,
        user.0.private_key_nonce,
    );

    db.add_user(&new_user, password.expose_secret(), &login_salt)
        .await?;

    Ok(web::Json::<UserId>(new_user.id))
}

#[post("/user")]
pub async fn add_user_disabled() -> HttpResponse {
    HttpResponse::Forbidden().finish()
}
