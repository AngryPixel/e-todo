use actix_web::{patch, web, HttpResponse, Responder};
use anyhow::anyhow;
use chrono::{DateTime, Utc};
use etodo_core::domain::TaskId;

use crate::db::DBClient;
use crate::domain::AuthenticatedUser;
use crate::TaskError;

#[derive(serde::Deserialize)]
pub struct UpdateTaskInput {
    finished: Option<bool>,
    updated: Option<DateTime<Utc>>,
    data: Option<Vec<u8>>,
}

#[tracing::instrument(name="Update task",skip(db, path, input,user),
fields(user_id=tracing::field::Empty, task_id=tracing::field::Empty))]
#[patch("/task/{task_id}")]
pub(crate) async fn update_task(
    db: web::Data<DBClient>,
    path: web::Path<TaskId>,
    user: AuthenticatedUser,
    input: web::Json<UpdateTaskInput>,
) -> Result<impl Responder, TaskError> {
    let task_id = path.into_inner();

    tracing::Span::current().record("user_id", &tracing::field::display(user.id()));
    tracing::Span::current().record("task_id", &tracing::field::display(&task_id));

    if input.finished.is_none() && input.updated.is_none() && input.data.is_none() {
        return Err(TaskError::InvalidParameters(anyhow!(
            "No parameters supplied"
        )));
    }

    if input.updated.is_some() && input.finished.is_none() && input.data.is_none() {
        return Err(TaskError::InvalidParameters(anyhow!(
            "When providing modification date a modified time must be provided"
        )));
    }

    let last_modified =
        if (input.finished.is_some() || input.data.is_some()) && input.updated.is_none() {
            Utc::now()
        } else {
            input.updated.unwrap()
        };

    let success = db
        .update_task(
            user.id(),
            &task_id,
            &input.finished,
            last_modified,
            &input.data,
        )
        .await?;

    if !success {
        return Ok(HttpResponse::NotFound().finish());
    }

    user.session().renew();

    Ok(HttpResponse::Ok().finish())
}
