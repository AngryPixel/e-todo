use actix_web::{get, web, Responder, Result};
use etodo_core::domain::{Task, TaskId};

use crate::db::DBClient;
use crate::domain::AuthenticatedUser;
use crate::TaskError;

#[tracing::instrument(name="Getting task by id",skip(db, path, user), fields(user_id=tracing::field::Empty,task_id=tracing::field::Empty))]
#[get("/task/{id}")]
pub(crate) async fn get_task(
    db: web::Data<DBClient>,
    path: web::Path<TaskId>,
    user: AuthenticatedUser,
) -> Result<Option<impl Responder>, TaskError> {
    let task_id = path.into_inner();

    tracing::Span::current().record("user_id", &tracing::field::display(user.id()));
    tracing::Span::current().record("task_id", &tracing::field::display(&task_id));

    let task = db.get_task(user.id(), &task_id).await.map_err(|e| {
        tracing::error!("Failed to get task: {:?}", e);
        e
    })?;

    Ok(task.map(web::Json::<Task>))
}

#[tracing::instrument(name="Getting all tasks", skip(db, user), fields(user_id=tracing::field::Empty))]
#[get("/task")]
pub(crate) async fn get_all_tasks(
    db: web::Data<DBClient>,
    user: AuthenticatedUser,
) -> Result<impl Responder, TaskError> {
    tracing::Span::current().record("user_id", &tracing::field::display(user.id()));

    let tasks = db.get_all_tasks(user.id()).await.map_err(|e| {
        tracing::error!("Failed to get all tasks: {:?}", e);
        e
    })?;

    Ok(web::Json(tasks))
}
