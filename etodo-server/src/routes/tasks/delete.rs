use actix_web::{delete, web, HttpResponse, Responder};
use etodo_core::domain::TaskId;

use crate::db::DBClient;
use crate::domain::AuthenticatedUser;
use crate::TaskError;

#[tracing::instrument(name="Deleting task",skip(db, user),
fields(user_id=tracing::field::Empty))]
#[delete("/task")]
pub(crate) async fn delete_task(
    db: web::Data<DBClient>,
    user: AuthenticatedUser,
    task_ids: web::Json<Vec<TaskId>>,
) -> Result<impl Responder, TaskError> {
    tracing::Span::current().record("user_id", &tracing::field::display(user.id()));

    if task_ids.len() != 0 {
        db.delete_tasks(user.id(), &task_ids).await?;
    }

    Ok(HttpResponse::Ok().finish())
}
