use actix_web::{post, web, Responder};
use chrono::{DateTime, Utc};
use etodo_core::domain::TaskId;

use crate::db::DBClient;
use crate::domain::AuthenticatedUser;
use crate::TaskError;

#[derive(serde::Deserialize)]
pub struct PostTaskInput {
    created: DateTime<Utc>,
    data: Vec<u8>,
}

#[tracing::instrument(name="Adding new task",skip(db, user, task_form),
fields(user_id=tracing::field::Empty, task_id=tracing::field::Empty))]
#[post("/task")]
pub(crate) async fn add_task(
    db: web::Data<DBClient>,
    user: AuthenticatedUser,
    task_form: web::Json<PostTaskInput>,
) -> Result<impl Responder, TaskError> {
    let task_id = TaskId::random();

    tracing::Span::current().record("user_id", &tracing::field::display(user.id()));
    tracing::Span::current().record("task_id", &tracing::field::display(&task_id.0));

    db.add_task(user.id(), &task_id, &task_form.created, &task_form.data)
        .await?;

    Ok(web::Json(task_id))
}
