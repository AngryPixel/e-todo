mod delete;
mod get;
mod put;
mod update;

use actix_web::http::StatusCode;
use actix_web::ResponseError;
pub use delete::*;
pub use get::*;
pub use put::*;
pub use update::*;

#[derive(thiserror::Error, Debug)]
pub enum TaskError {
    #[error("User not authenticated")]
    NotAuthenticated(#[source] anyhow::Error),
    #[error("Invalid parameters")]
    InvalidParameters(#[source] anyhow::Error),
    #[error("Something went wrong")]
    UnexpectedError(#[from] anyhow::Error),
}

impl ResponseError for TaskError {
    fn status_code(&self) -> StatusCode {
        match self {
            TaskError::NotAuthenticated(_) => StatusCode::UNAUTHORIZED,
            TaskError::InvalidParameters(_) => StatusCode::BAD_REQUEST,
            TaskError::UnexpectedError(_) => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}
