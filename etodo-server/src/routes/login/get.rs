use actix_web::{get, web, Responder};
use anyhow::anyhow;
use etodo_core::crypto::Salt;
use etodo_core::domain::Email;

use crate::db::DBClient;
use crate::domain::{AuthSaltGenerator, AuthType};
use crate::LoginError;

#[derive(serde::Deserialize)]
pub struct LoginSaltInput {
    email: String,
}

#[tracing::instrument(name = "Getting authentication salt", skip(db, input, salt_generator))]
#[get("/login/salt")]
pub(crate) async fn get_auth_salt(
    db: web::Data<DBClient>,
    input: web::Json<LoginSaltInput>,
    salt_generator: web::Data<AuthSaltGenerator>,
) -> Result<impl Responder, actix_web::Error> {
    let email = Email::parse(&input.email)
        .map_err(|_| LoginError::ValidationError(anyhow!("Invalid email")))?;

    let login_salt = db
        .get_login_salt_from_user_email(&email)
        .await
        .map_err(|e| actix_web::error::ErrorInternalServerError(LoginError::UnexpectedError(e)))?;

    // Always calculate salt to avoid timing issues;
    let random_salt = Salt::random();

    let salt = if let Some(s) = login_salt {
        s
    } else {
        random_salt
    };

    salt_generator.insert(&input.email, salt.clone(), AuthType::Login);

    Ok(web::Json(salt))
}
