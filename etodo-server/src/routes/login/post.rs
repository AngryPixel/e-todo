// Login steps:
// 1: Hash password on the client with some other data
// 2: Transmit that to server,
// 3: Perform the usual server side storage
// 4: Use simple token based authentication
// 5: Update task methods to use the token auth to retrieve user id rather than log parameters

use actix_web::{post, web, Responder};
use anyhow::anyhow;
use etodo_core::crypto::CryptoHash;
use etodo_core::domain::Email;

use crate::db::DBClient;
use crate::domain::{
    validate_credentials, AuthError, AuthSaltGenerator, AuthType, Credentials, DefaultPassword,
    UserSession,
};
use crate::LoginError;

#[derive(serde::Deserialize)]
pub struct LoginFormData {
    email: String,
    password: String,
}

#[tracing::instrument(skip(db, form, default_password, session, salt_generator),
fields(email=tracing::field::Empty, user_id=tracing::field::Empty))]
#[post("/login")]
pub(crate) async fn login(
    db: web::Data<DBClient>,
    default_password: web::Data<DefaultPassword>,
    form: web::Form<LoginFormData>,
    session: UserSession,
    salt_generator: web::Data<AuthSaltGenerator>,
) -> Result<impl Responder, LoginError> {
    tracing::Span::current().record("email", &tracing::field::display(&form.email));

    let email = Email::parse(&form.email)
        .map_err(|_| LoginError::ValidationError(anyhow!("Invalid email")))?;

    let login_salt = {
        if let Some(salt) = salt_generator
            .consume(&email.as_ref().to_string(), AuthType::Login)
            .map_err(|e| LoginError::AuthError(e.into()))?
        {
            salt
        } else {
            return Err(LoginError::AuthError(anyhow::anyhow!(
                "No salt request for this email"
            )));
        }
    };

    let hashed_password = CryptoHash::hash_with_salt(form.password.as_bytes(), &login_salt)
        .map_err(|_| LoginError::UnexpectedError(anyhow!("Failed to generate hash")))?;
    let credentials = Credentials {
        email,
        password: hex::encode(&hashed_password),
    };

    match validate_credentials(&db, credentials, &default_password).await {
        Ok(u) => {
            tracing::Span::current().record("user_id", &tracing::field::display(&u.id));
            session.renew();
            session
                .insert_user_id(u.id)
                .map_err(|e| LoginError::AuthError(e.into()))?;
            Ok(web::Json(u))
        }
        Err(e) => {
            let e = match e {
                AuthError::InvalidCredentials(_) => LoginError::AuthError(e.into()),
                AuthError::UnexpectedError(_) => LoginError::UnexpectedError(e.into()),
            };
            Err(e)
        }
    }
}
