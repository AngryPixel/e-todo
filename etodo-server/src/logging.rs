use tokio::task::JoinHandle;
use tracing::subscriber::set_global_default;
use tracing::Subscriber;
use tracing_subscriber::fmt::format::FmtSpan;
use tracing_subscriber::{EnvFilter, FmtSubscriber};

pub fn init_subscriber(subscriber: impl Subscriber + Send + Sync) {
    // `set_global_default` can be used by applications to specify what subscriber should be
    // used to process spans.
    set_global_default(subscriber).expect("Failed to set subscriber");
}

pub fn init_fmt_logger() {
    let env_filter = EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new("info"));
    let subscriber = FmtSubscriber::builder()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .with_env_filter(env_filter)
        // all spans/events with a level higher than TRACE (e.g, debug, info, warn, etc.)
        // will be written to stdout.
        .with_level(true)
        .with_file(false)
        .with_line_number(false)
        .pretty()
        // completes the builder.
        .finish();

    init_subscriber(subscriber)
}

// Trait bounds copied from `spawn_blocking`
pub fn spawn_blocking_with_tracing<F, R>(f: F) -> JoinHandle<R>
where
    F: FnOnce() -> R + Send + 'static,
    R: Send + 'static,
{
    let current_span = tracing::Span::current();
    tokio::task::spawn_blocking(move || current_span.in_scope(f))
}
