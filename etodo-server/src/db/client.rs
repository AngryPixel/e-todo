use std::str::FromStr;

use chrono::{DateTime, Utc};
use etodo_core::crypto;
use etodo_core::crypto::{Encrypted, EncryptionKey, Nonce, PrivKeyEncrypted, PublicKey, Salt};
use etodo_core::domain::{Email, Task, TaskId, User, UserId};
use sqlx::SqlitePool;
use tracing::Instrument;
use uuid::Uuid;

#[derive(Debug)]
pub struct DBClient(SqlitePool);

#[derive(sqlx::Type)]
#[sqlx(transparent)]
struct DBUuid(Uuid);

#[derive(sqlx::Type)]
#[sqlx(transparent)]
struct DBBinary(Vec<u8>);

#[derive(sqlx::Type)]
#[sqlx(transparent)]
struct UuidSlice<'a>(&'a [Uuid]);

const DB_MAX_OP_CHUNK: usize = 1000;

struct DBTask {
    db_id: DBUuid,
    created: String,
    last_updated: String,
    finished: i64,
    db_data: DBBinary,
}

impl DBTask {
    fn into_domain_type(self) -> Result<Task, anyhow::Error> {
        Ok(Task {
            id: TaskId(self.db_id.0),
            dt_created: DateTime::<Utc>::from_str(&self.created)?,
            dt_last_updated: DateTime::<Utc>::from_str(&self.last_updated)?,
            finished: self.finished != 0,
            data: PrivKeyEncrypted::with_data(self.db_data.0),
        })
    }
}

struct DBUser {
    db_id: DBUuid,
    email: String,
    password: String,
    db_public_key: DBBinary,
    db_encryption_key: DBBinary,
    db_encryption_key_nonce: DBBinary,
    db_encryption_salt: DBBinary,
    db_private_key: DBBinary,
    db_private_key_nonce: DBBinary,
}

impl DBUser {
    fn into_domain_type(self) -> Result<(User, String), anyhow::Error> {
        Ok((
            User {
                id: UserId(self.db_id.0),
                email: Email::parse(&self.email)
                    .map_err(|_| anyhow::anyhow!("Invalid email address"))?,
                public_key: PublicKey::try_from(self.db_public_key.0)
                    .map_err(|_| anyhow::anyhow!("Invalid binary data"))?,
                encryption_key: Encrypted::new(self.db_encryption_key.0),
                encryption_key_nonce: Nonce::try_from(self.db_encryption_key_nonce.0)
                    .map_err(|_| anyhow::anyhow!("Invalid binary data"))?,
                private_key: Encrypted::new(self.db_private_key.0),
                private_key_nonce: Nonce::try_from(self.db_private_key_nonce.0)
                    .map_err(|_| anyhow::anyhow!("Invalid binary data"))?,
                encryption_salt: Salt::try_from(self.db_encryption_salt.0)
                    .map_err(|_| anyhow::anyhow!("Invalid binary data"))?,
            },
            self.password,
        ))
    }
}

impl DBClient {
    pub async fn new(
        config: &crate::configuration::DatabaseSettings,
    ) -> Result<DBClient, sqlx::Error> {
        sqlx::sqlite::SqlitePoolOptions::new()
            .connect_with(config.get_options())
            .await
            .map(DBClient)
    }

    pub async fn run_migrations(&self) -> Result<(), sqlx::migrate::MigrateError> {
        sqlx::migrate!("./migrations").run(&self.0).await
    }

    pub async fn get_task(
        &self,
        user_id: &UserId,
        id: &TaskId,
    ) -> Result<Option<Task>, anyhow::Error> {
        let query_span = tracing::debug_span!("Get task by Id from database");
        // There's a bug with type resolution for Sqlite (https://github.com/launchbadge/sqlx/issues/1979)
        // for now we have to use this trick to force a specific type resolution.
        let task = sqlx::query_as!(
            DBTask,
            r#"SELECT id as "db_id!: DBUuid", created, last_updated, finished, data as 'db_data!:DBBinary' FROM todo WHERE id = ? AND user_id = ?"#,
            id.0,
            user_id.0
        )
        .fetch_optional(&self.0)
            .instrument(query_span)
        .await?;

        Ok(match task {
            Some(t) => Some(t.into_domain_type()?),
            _ => None,
        })
    }

    pub async fn get_all_tasks(&self, user_id: &UserId) -> Result<Vec<Task>, anyhow::Error> {
        let query_span = tracing::debug_span!("Get all tasks from the database");
        // There's a bug with type resolution for Sqlite (https://github.com/launchbadge/sqlx/issues/1979)
        // for now we have to use this trick to force a specific type resolution.
        let tasks = sqlx::query_as!(
            DBTask,
            r#"SELECT id as "db_id!: DBUuid", created, last_updated, finished, data as 'db_data!:DBBinary' FROM todo WHERE user_id = ?"#,
            user_id.0
        )
            .fetch_all(&self.0)
            .instrument(query_span)
            .await?;

        let mut result = Vec::with_capacity(tasks.len());
        for task in tasks.into_iter() {
            result.push(task.into_domain_type()?)
        }

        Ok(result)
    }

    pub async fn add_task_test(&self, user_id: &UserId, task: &Task) -> Result<(), anyhow::Error> {
        let data_bytes = task.data.as_ref();

        let dt_created_str = task.dt_created.to_string();
        let dt_updated_str = task.dt_last_updated.to_string();

        sqlx::query!(
            "INSERT INTO todo (id, user_id, created, last_updated, finished, data) \
        VALUES ($1, $2, $3, $4, $5, $6)",
            task.id.0,
            user_id.0,
            dt_created_str,
            dt_updated_str,
            task.finished,
            data_bytes,
        )
        .execute(&self.0)
        .await?;

        Ok(())
    }

    pub async fn delete_tasks(
        &self,
        user_id: &UserId,
        task_ids: &[TaskId],
    ) -> Result<(), anyhow::Error> {
        // limitation of sqlx, need to generate and bind the query ourself
        let query_str = format!(
            "DELETE FROM todo WHERE user_id =? AND id IN ( ?{})",
            ", ?".repeat(DB_MAX_OP_CHUNK.min(task_ids.len()) - 1)
        );
        let mut transaction = self.0.begin().await?;
        for tasks in task_ids.chunks(DB_MAX_OP_CHUNK) {
            let query_span = tracing::debug_span!("Removing task from database");
            let mut query = sqlx::query(&query_str).bind(user_id.0);
            for id in tasks {
                query = query.bind(id.0);
            }
            query
                .execute(&mut transaction)
                .instrument(query_span)
                .await?;
        }

        transaction.commit().await?;

        Ok(())
    }

    pub async fn update_task(
        &self,
        user_id: &UserId,
        task_id: &TaskId,
        finished: &Option<bool>,
        modified: DateTime<Utc>,
        data: &Option<Vec<u8>>,
    ) -> Result<bool, anyhow::Error> {
        let query_span = tracing::debug_span!("Updating task in db");
        let mut query_str = "UPDATE todo SET".to_string();

        if finished.is_some() {
            query_str.push_str(" finished = ?,");
        }

        query_str.push_str(" last_updated = ?");

        if data.is_some() {
            query_str.push_str(", data = ?");
        }

        query_str.push_str(" WHERE user_id = ? AND id = ?");

        let mut query = sqlx::query(&query_str);

        if let Some(f) = finished {
            query = query.bind(f);
        }

        query = query.bind(modified.to_string());

        if let Some(d) = data {
            query = query.bind(d);
        }

        let result = query
            .bind(user_id.0)
            .bind(task_id.0)
            .execute(&self.0)
            .instrument(query_span)
            .await?;

        Ok(result.rows_affected() == 1)
    }

    pub async fn add_task(
        &self,
        user_id: &UserId,
        task_id: &TaskId,
        created: &DateTime<Utc>,
        data: &[u8],
    ) -> Result<(), anyhow::Error> {
        let query_span = tracing::debug_span!("Adding task to database");
        let dt_created_str = created.to_string();

        sqlx::query!(
            "INSERT INTO todo (id, user_id, created, last_updated, finished, data) \
        VALUES ($1, $2, $3, $4, $5, $6)",
            task_id.0,
            user_id.0,
            dt_created_str,
            dt_created_str,
            false,
            data,
        )
        .execute(&self.0)
        .instrument(query_span)
        .await?;

        Ok(())
    }

    pub async fn add_user(
        &self,
        user: &User,
        password: &str,
        login_salt: &Salt,
    ) -> Result<(), anyhow::Error> {
        let password_hash = crypto::hash_password_server_with_salt(password, login_salt)
            .map_err(|_| anyhow::anyhow!("Failed to generate password hash"))?;

        let pub_key_bytes = user.public_key.as_ref();
        let encryption_key_bytes = user.encryption_key.as_ref();
        let encryption_key_nonce_bytes = user.encryption_key_nonce.as_ref();
        let encryption_salt_bytes = user.encryption_salt.as_ref();
        let private_key_bytes = user.private_key.as_ref();
        let private_key_nonce_bytes = user.private_key_nonce.as_ref();
        let login_salt_bytes = login_salt.as_ref();
        let email = user.email.as_ref();

        sqlx::query!(
            "INSERT INTO users (id, password, email, public_key, \
encryption_key, encryption_key_nonce, encryption_salt, private_key, private_key_nonce, login_salt)\
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)",
            user.id.0,
            password_hash,
            email,
            pub_key_bytes,
            encryption_key_bytes,
            encryption_key_nonce_bytes,
            encryption_salt_bytes,
            private_key_bytes,
            private_key_nonce_bytes,
            login_salt_bytes,
        )
        .execute(&self.0)
        .await?;

        Ok(())
    }

    pub async fn get_user_by_email(
        &self,
        user_email: &Email,
    ) -> Result<Option<(User, String)>, anyhow::Error> {
        let query_span = tracing::debug_span!("Get user from database");
        // There's a bug with type resolution for Sqlite (https://github.com/launchbadge/sqlx/issues/1979)
        // for now we have to use this trick to force a specific type resolution.
        let email = user_email.as_ref();

        let result = sqlx::query_as!(
            DBUser,
            r#"SELECT id as "db_id!: DBUuid", email, password, public_key as 'db_public_key!:DBBinary',
             encryption_key as 'db_encryption_key!: DBBinary',
             encryption_key_nonce as 'db_encryption_key_nonce!: DBBinary',
             encryption_salt as 'db_encryption_salt!: DBBinary',
             private_key as 'db_private_key!: DBBinary',
             private_key_nonce as 'db_private_key_nonce!: DBBinary'
             FROM users WHERE email =?"#,
            email
        )
        .fetch_optional(&self.0)
        .instrument(query_span)
        .await?;

        Ok(match result {
            Some(u) => Some(u.into_domain_type()?),
            _ => None,
        })
    }

    pub async fn get_login_salt_from_user_id(
        &self,
        user_id: &UserId,
    ) -> Result<Option<Salt>, anyhow::Error> {
        let query_span = tracing::debug_span!("Get user salt from database");

        struct DBSalt {
            salt: DBBinary,
        }

        let result = sqlx::query_as!(
            DBSalt,
            r#"SELECT login_salt as "salt!: DBBinary" FROM users WHERE id = ?"#,
            user_id.0
        )
        .fetch_optional(&self.0)
        .instrument(query_span)
        .await?;

        Ok(match result {
            Some(salt) => Some(
                Salt::try_from(salt.salt.0)
                    .map_err(|_| anyhow::anyhow!("Failed to convert data type"))?,
            ),
            _ => None,
        })
    }

    pub async fn get_login_salt_from_user_email(
        &self,
        email: &Email,
    ) -> Result<Option<Salt>, anyhow::Error> {
        let query_span = tracing::debug_span!("Get user salt from database");

        struct DBSalt {
            salt: DBBinary,
        }

        let email = email.as_ref();

        let result = sqlx::query_as!(
            DBSalt,
            r#"SELECT login_salt as "salt!: DBBinary" FROM users WHERE email = ?"#,
            email
        )
        .fetch_optional(&self.0)
        .instrument(query_span)
        .await?;

        Ok(match result {
            Some(salt) => Some(
                Salt::try_from(salt.salt.0)
                    .map_err(|_| anyhow::anyhow!("Failed to convert data type"))?,
            ),
            _ => None,
        })
    }

    pub async fn get_user_by_id(
        &self,
        user_id: &UserId,
    ) -> Result<Option<(User, String)>, anyhow::Error> {
        let query_span = tracing::debug_span!("Get user from database");
        // There's a bug with type resolution for Sqlite (https://github.com/launchbadge/sqlx/issues/1979)
        // for now we have to use this trick to force a specific type resolution.

        let result = sqlx::query_as!(
            DBUser,
            r#"SELECT id as "db_id!: DBUuid", email, password, public_key as 'db_public_key!:DBBinary',
             encryption_key as 'db_encryption_key!: DBBinary',
             encryption_key_nonce as 'db_encryption_key_nonce!: DBBinary',
             encryption_salt as 'db_encryption_salt!: DBBinary',
             private_key as 'db_private_key!: DBBinary',
             private_key_nonce as 'db_private_key_nonce!: DBBinary'
             FROM users WHERE id=?"#,
            user_id.0
        )
        .fetch_optional(&self.0)
        .instrument(query_span)
        .await?;

        Ok(match result {
            Some(u) => Some(u.into_domain_type()?),
            _ => None,
        })
    }

    pub async fn delete_user(&self, user_id: &UserId) -> Result<(), anyhow::Error> {
        let query_span = tracing::debug_span!("Deleting user from database");
        sqlx::query!("DELETE FROM users WHERE id = ?", user_id.0)
            .execute(&self.0)
            .instrument(query_span)
            .await?;
        Ok(())
    }

    pub async fn update_user_email(
        &self,
        user_id: &UserId,
        email: &Email,
    ) -> Result<bool, anyhow::Error> {
        let query_span = tracing::debug_span!("Updating user email in database");

        let email = email.as_ref();

        let result = sqlx::query!("UPDATE users SET email = ? WHERE id = ?", email, user_id.0)
            .execute(&self.0)
            .instrument(query_span)
            .await?;

        Ok(result.rows_affected() == 1)
    }

    pub async fn update_user_password(
        &self,
        user_id: &UserId,
        password: &str,
        encrypted_key: &Encrypted<EncryptionKey>,
        encrypted_key_nonce: &Nonce,
        encryption_salt: &Salt,
        login_salt: &Salt,
    ) -> Result<bool, anyhow::Error> {
        let query_span = tracing::debug_span!("Updating user password in database");

        let encrypted_key = encrypted_key.as_ref();
        let encrypted_key_nonce = encrypted_key_nonce.as_ref();
        let encryption_salt = encryption_salt.as_ref();
        let login_salt = login_salt.as_ref();

        let result = sqlx::query!(
            "UPDATE users SET password = ?, encryption_key =?,encryption_key_nonce =?, encryption_salt= ?, login_salt=? WHERE id = ?",
            password,
            encrypted_key,
            encrypted_key_nonce,
            encryption_salt,
            login_salt,
            user_id.0

        )
        .execute(&self.0)
        .instrument(query_span)
        .await?;

        Ok(result.rows_affected() == 1)
    }
}
