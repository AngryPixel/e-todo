use std::collections::HashMap;
use std::sync::Mutex;
use std::time::Duration;

use anyhow::Context;
use etodo_core::crypto::Salt;
use etodo_core::domain::{Email, User};

use crate::db::DBClient;
use crate::domain::DefaultPassword;

#[derive(thiserror::Error, Debug)]
pub enum AuthError {
    #[error("Authentication failed")]
    InvalidCredentials(#[source] anyhow::Error),
    #[error(transparent)]
    UnexpectedError(#[from] anyhow::Error),
}

pub struct Credentials {
    pub email: Email,
    pub password: String,
}

#[tracing::instrument(name = "Validate credentials", skip(credentials, db, default_password))]
pub async fn validate_credentials(
    db: &DBClient,
    credentials: Credentials,
    default_password: &DefaultPassword,
) -> Result<User, AuthError> {
    let (user, password) = {
        if let Some((u, p)) = db
            .get_user_by_email(&credentials.email)
            .await
            .map_err(AuthError::UnexpectedError)?
        {
            (Some(u), p)
        } else {
            (None, default_password.0.clone())
        }
    };

    // Spawn a blocking task to make sure the run-time can gracefully handle long password
    // hashing sessions without blocking all the executors
    crate::spawn_blocking_with_tracing(move || {
        if !etodo_core::crypto::hash_password_server_verify(&password, &credentials.password) {
            return Err(AuthError::InvalidCredentials(anyhow::anyhow!(
                "Invalid credentials"
            )));
        }
        Ok(())
    })
    .await
    .context("Failed to spawn blocking task")
    .map_err(AuthError::UnexpectedError)??;

    user.ok_or_else(|| anyhow::anyhow!("Invalid credentials"))
        .map_err(AuthError::InvalidCredentials)
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum AuthType {
    Login,
    PasswordUpdate,
}

#[derive(Debug)]
pub struct AuthSaltOp {
    auth_type: AuthType,
    salt: Salt,
    created: std::time::SystemTime,
}

#[derive(thiserror::Error, Debug)]
pub enum AuthSaltGeneratorError {
    #[error("Auth operations don't match")]
    InvalidOperation,
    #[error("The salt lifetime has expired")]
    Expired,
}

/// Authentication salt generator for client side hashing to provide server relieve.
#[derive(Debug, Default)]
pub struct AuthSaltGenerator(Mutex<HashMap<String, AuthSaltOp>>);

impl AuthSaltGenerator {
    const MAX_SALT_VALIDITY: Duration = Duration::from_secs(60);

    pub fn new() -> Self {
        Self(Mutex::new(HashMap::new()))
    }

    pub fn insert(&self, email: &str, salt: Salt, auth_type: AuthType) {
        let mut scope = self.0.lock().unwrap();
        scope.insert(
            email.to_string(),
            AuthSaltOp {
                auth_type,
                salt,
                created: std::time::SystemTime::now(),
            },
        );
    }

    pub fn consume(
        &self,
        email: &String,
        auth_type: AuthType,
    ) -> Result<Option<Salt>, AuthSaltGeneratorError> {
        let op = {
            let mut scope = self.0.lock().unwrap();
            scope.remove(email)
        };

        if let Some(op) = op {
            if op.auth_type != auth_type {
                return Err(AuthSaltGeneratorError::InvalidOperation);
            }

            if std::time::SystemTime::now()
                .duration_since(op.created)
                .map_err(|_| AuthSaltGeneratorError::Expired)?
                >= Self::MAX_SALT_VALIDITY
            {
                return Err(AuthSaltGeneratorError::Expired);
            }

            Ok(Some(op.salt))
        } else {
            Ok(None)
        }
    }
}
