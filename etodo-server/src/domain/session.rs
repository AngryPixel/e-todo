use actix_session::{Session, SessionExt, SessionGetError, SessionInsertError};
use actix_utils::future::{ready, Ready};
use actix_web::dev::Payload;
use actix_web::error::{ErrorInternalServerError, ErrorUnauthorized};
use actix_web::{FromRequest, HttpRequest};
use etodo_core::crypto::CryptoError;
use etodo_core::domain::UserId;
use uuid::Uuid;

pub struct UserSession(Session);

impl UserSession {
    const USER_ID_KEY: &'static str = "x-user-id";

    pub fn renew(&self) {
        self.0.renew();
    }

    pub fn insert_user_id(&self, user_id: UserId) -> Result<(), SessionInsertError> {
        self.0.insert(Self::USER_ID_KEY, user_id)
    }

    pub fn get_user_id(&self) -> Result<Option<UserId>, SessionGetError> {
        self.0.get(Self::USER_ID_KEY)
    }

    pub fn log_out(&self) {
        self.0.purge()
    }
}

impl FromRequest for UserSession {
    // Return the same error as the implementation of `FromRequest` for `Session`
    type Error = <Session as FromRequest>::Error;

    type Future = Ready<Result<UserSession, Self::Error>>;

    fn from_request(req: &HttpRequest, _payload: &mut Payload) -> Self::Future {
        ready(Ok(UserSession(req.get_session())))
    }
}

#[derive(Debug, Clone)]
pub struct DefaultPassword(pub String);

pub fn generate_missing_user_password() -> Result<DefaultPassword, CryptoError> {
    etodo_core::crypto::hash_password_server(&Uuid::new_v4().to_string()).map(DefaultPassword)
}

pub struct AuthenticatedUser {
    user_id: UserId,
    session: UserSession,
}

impl AuthenticatedUser {
    pub fn id(&self) -> &UserId {
        &self.user_id
    }

    pub fn session(&self) -> &UserSession {
        &self.session
    }
}

impl FromRequest for AuthenticatedUser {
    type Error = actix_web::Error;

    type Future = Ready<Result<AuthenticatedUser, Self::Error>>;

    fn from_request(req: &HttpRequest, _payload: &mut Payload) -> Self::Future {
        let session = UserSession(req.get_session());
        let user_id = session
            .get_user_id()
            .map_err(|_| ErrorInternalServerError(""));
        ready(match user_id {
            Ok(user_id) => {
                if let Some(u) = user_id {
                    Ok(AuthenticatedUser {
                        user_id: u,
                        session,
                    })
                } else {
                    Err(ErrorUnauthorized("unauthorized"))
                }
            }
            Err(e) => Err(e),
        })
    }
}
