use std::io::BufReader;
use std::net::TcpListener;

use actix_session::config::{
    CookieContentSecurity, PersistentSession, SessionLifecycle, TtlExtensionPolicy,
};
use actix_session::{storage::CookieSessionStore, SessionMiddleware};
use actix_web::cookie::time::Duration;
use actix_web::cookie::Key;
use actix_web::dev::Server;
use actix_web::{error, web, App, HttpResponse, HttpServer};
use rand::rngs::OsRng;
use rcgen::{date_time_ymd, Certificate, CertificateParams, DistinguishedName};
use rsa::pkcs8::EncodePrivateKey;
use rsa::RsaPrivateKey;
use rustls::server::NoClientAuth;
use rustls::{self};
use tracing_actix_web::TracingLogger;

use crate::domain::{generate_missing_user_password, AuthSaltGenerator};

pub struct Application {
    port: u16,
    server: Server,
}

impl Application {
    pub const MAX_JSON_PAYLOAD_SIZE: usize = 16 * 1024;

    pub async fn build(
        config: &crate::configuration::Configuration,
    ) -> Result<Self, anyhow::Error> {
        let ssl_config: Option<rustls::ServerConfig> = if config.application.use_ssl {
            Some(generate_self_signed_cert()?)
        } else {
            None
        };

        let db_client = web::Data::new(crate::db::DBClient::new(&config.database).await?);

        db_client.run_migrations().await?;

        let address = format!("{}:{}", config.application.host, config.application.port);
        let listener = TcpListener::bind(address)?;
        let port = listener.local_addr().unwrap().port();

        let missing_user_password = generate_missing_user_password()?;
        let session_key = Key::generate();

        let allow_user_creation = config.application.allow_user_creation;
        let mut server = HttpServer::new(move || {
            let json_config = web::JsonConfig::default()
                .limit(Self::MAX_JSON_PAYLOAD_SIZE)
                .error_handler(|err, _req| {
                    error::InternalError::from_response(err, HttpResponse::BadRequest().finish())
                        .into()
                });

            let app = App::new()
                .wrap(TracingLogger::default())
                .wrap(
                    SessionMiddleware::builder(CookieSessionStore::default(), session_key.clone())
                        .cookie_content_security(CookieContentSecurity::Private)
                        .session_lifecycle(SessionLifecycle::PersistentSession(
                            PersistentSession::default()
                                .session_ttl(Duration::weeks(1))
                                .session_ttl_extension_policy(TtlExtensionPolicy::OnStateChanges),
                        ))
                        .build(),
                )
                .app_data(json_config)
                .app_data(db_client.clone())
                .app_data(web::Data::new(missing_user_password.clone()))
                .app_data(web::Data::new(AuthSaltGenerator::new()))
                .route("/health_check", web::get().to(crate::routes::health_check))
                .service(crate::routes::get_all_tasks)
                .service(crate::routes::get_task)
                .service(crate::routes::add_task)
                .service(crate::routes::delete_task)
                .service(crate::routes::update_task)
                .service(crate::routes::login)
                .service(crate::routes::get_auth_salt)
                .service(crate::routes::logout)
                .service(crate::routes::get_user)
                .service(crate::routes::get_password_change_salt)
                .service(crate::routes::delete_user)
                .service(crate::routes::patch_user_email)
                .service(crate::routes::patch_user_password);

            if allow_user_creation {
                app.service(crate::routes::add_user)
            } else {
                app.service(crate::routes::add_user_disabled)
            }
        });

        server = if let Some(ssl_config) = ssl_config {
            server.listen_rustls(listener, ssl_config)?
        } else {
            server.listen(listener)?
        };

        tracing::info!("Starting server on port {:05}", port);

        Ok(Application {
            server: server.run(),
            port,
        })
    }

    pub async fn run_to_completion(self) -> Result<(), std::io::Error> {
        self.server.await
    }

    pub fn port(&self) -> u16 {
        self.port
    }
}

fn generate_self_signed_cert() -> Result<rustls::ServerConfig, anyhow::Error> {
    tracing::info!("Generating SSL Certificates");
    let mut params: CertificateParams = Default::default();
    params.not_before = date_time_ymd(2021, 5, 19);
    params.not_after = date_time_ymd(4096, 1, 1);
    params.distinguished_name = DistinguishedName::new();

    params.alg = &rcgen::PKCS_RSA_SHA256;

    let mut rng = OsRng;
    let bits = 2048;
    let private_key = RsaPrivateKey::new(&mut rng, bits)?;
    let private_key_der = private_key.to_pkcs8_der()?;
    let key_pair = rcgen::KeyPair::try_from(private_key_der.as_bytes()).unwrap();
    params.key_pair = Some(key_pair);

    let cert = Certificate::from_params(params)?;
    let pem_serialized = cert.serialize_pem()?;
    let key_serialized = cert.serialize_private_key_pem();

    let rustls_certs: Vec<rustls::Certificate> =
        rustls_pemfile::certs(&mut BufReader::new(pem_serialized.as_bytes()))
            .unwrap()
            .iter()
            .map(|v| rustls::Certificate(v.clone()))
            .collect();

    let rustls_private_key = load_private_key(key_serialized)?;

    rustls::ServerConfig::builder()
        .with_safe_default_cipher_suites()
        .with_safe_default_kx_groups()
        .with_protocol_versions(rustls::DEFAULT_VERSIONS)
        .map_err(|e| anyhow::anyhow!("Failed to validate rustls version config: {}", e))?
        .with_client_cert_verifier(NoClientAuth::new())
        .with_single_cert(rustls_certs, rustls_private_key)
        .map_err(|e| anyhow::anyhow!("Failed to generate rustls server config: {}", e))
}

fn load_private_key(key: String) -> Result<rustls::PrivateKey, anyhow::Error> {
    let mut reader = BufReader::new(key.as_bytes());

    loop {
        match rustls_pemfile::read_one(&mut reader).expect("cannot parse private key .pem file") {
            Some(rustls_pemfile::Item::RSAKey(key)) => return Ok(rustls::PrivateKey(key)),
            Some(rustls_pemfile::Item::PKCS8Key(key)) => return Ok(rustls::PrivateKey(key)),
            Some(rustls_pemfile::Item::ECKey(key)) => return Ok(rustls::PrivateKey(key)),
            None => break,
            _ => {}
        }
    }

    Err(anyhow::anyhow!("No supported private keys found"))
}
