use std::path::{Path, PathBuf};

use serde_aux::prelude::deserialize_number_from_string;
use sqlx::sqlite::{SqliteConnectOptions, SqliteJournalMode};
use sqlx::ConnectOptions;

#[derive(Debug, serde::Deserialize)]
pub struct Configuration {
    pub application: ApplicationSettings,
    pub database: DatabaseSettings,
}

#[derive(Debug, serde::Deserialize)]
pub struct DatabaseSettings {
    pub path: PathBuf,
}

#[derive(Debug, Clone, serde::Deserialize)]
pub struct ApplicationSettings {
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub port: u16,
    pub host: String,
    pub use_ssl: bool,
    pub allow_user_creation: bool,
}

impl DatabaseSettings {
    pub fn with_random_tmp_path() -> std::io::Result<SqliteConnectOptions> {
        let path_buf = std::env::temp_dir()
            .join(uuid::Uuid::new_v4().simple().to_string())
            .join("db.db");

        std::fs::create_dir_all(path_buf.parent().unwrap())?;

        Ok(Self::with_path(&path_buf))
    }

    pub fn with_path<T: AsRef<Path>>(path: T) -> SqliteConnectOptions {
        let mut options = SqliteConnectOptions::new()
            .filename(path)
            .create_if_missing(true)
            .journal_mode(SqliteJournalMode::Wal)
            .foreign_keys(true);
        options.disable_statement_logging();

        options
    }

    pub fn get_options(&self) -> SqliteConnectOptions {
        Self::with_path(&self.path)
    }
}

pub enum Environment {
    Local,
    Gitlab,
    Production,
}

impl Environment {
    pub fn as_str(&self) -> &'static str {
        match self {
            Environment::Local => "local",
            Environment::Gitlab => "gitlab",
            Environment::Production => "production",
        }
    }
}

impl TryFrom<String> for Environment {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        match value.to_lowercase().as_str() {
            "local" => Ok(Self::Local),
            "gitlab" => Ok(Self::Gitlab),
            "production" => Ok(Self::Production),
            other => Err(format!("{other} is not a supported environment")),
        }
    }
}

pub fn get_configuration() -> Result<Configuration, config::ConfigError> {
    let settings = config::Config::builder();
    let base_path = std::env::current_dir().expect("Failed to determine the current directory");
    let config_directory = base_path.join("configuration");

    // Detect env.
    let environment: Environment = std::env::var("ETODO_ENVIRONMENT")
        .unwrap_or_else(|_| "local".into())
        .try_into()
        .expect("Failed to parse ETODO_ENVIRONMENT");

    tracing::info!("Loading {} environment config file", environment.as_str());

    // Read the default configuration.
    let config = settings
        .add_source(config::File::from(config_directory.join("base")).required(true))
        .add_source(config::File::from(config_directory.join(environment.as_str())).required(true))
        // Add in environment variable with `ETODO` prefix and `__` as separator.
        .add_source(config::Environment::with_prefix("ETODO").separator("__"))
        .build()?;

    config.try_deserialize::<Configuration>()
}
