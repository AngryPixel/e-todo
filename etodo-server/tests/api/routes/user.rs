use etodo_core::domain::Email;
use secrecy::{ExposeSecret, Secret};

use crate::helpers::TestApp;

#[tokio::test]
async fn get_user_without_login_returns_401() {
    let app = TestApp::spawn_app().await;

    let response = app.get_user().await.expect_err("Get did not fail");

    assert_eq!(401, response.status().as_u16());
}

#[tokio::test]
async fn get_user_matches() {
    let app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    let user = app.get_user().await.expect("Failed to get user");

    assert_eq!(user, app.test_user.user);
}

#[tokio::test]
async fn delete_removes_all_existing_tasks() {
    let app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    app.test_user
        .generate_test_item_and_add_to_db("Foo", &app.db)
        .await;

    app.delete_test_user().await;

    let tasks = app
        .db
        .get_all_tasks(&app.test_user.user.id)
        .await
        .expect("Failed to execute db request");

    assert!(tasks.is_empty());
}

#[tokio::test]
async fn check_password_update_updates_user_info() {
    let mut app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    app.update_test_user_pwd()
        .await
        .expect("Failed update user password");

    let user = app.get_user().await.expect("Failed to get user");

    assert_eq!(user, app.test_user.user);
}

#[tokio::test]
async fn check_password_update_works_on_next_login() {
    let mut app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    app.update_test_user_pwd()
        .await
        .expect("Failed update user password");

    app.post_logout().await;

    app.post_login_with_test_user()
        .await
        .expect("Failed to login");
}

#[tokio::test]
async fn check_email_update_updates_user_info() {
    let mut app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    app.update_test_user_email()
        .await
        .expect("Failed update user email");

    let user = app.get_user().await.expect("Failed to get user");

    assert_eq!(user, app.test_user.user);
}

#[tokio::test]
async fn check_password_update_allows_to_decrypt_previous_task() {
    let mut app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    let text = "Foo";

    let task = app
        .test_user
        .generate_test_item_and_add_to_db(text, &app.db)
        .await;

    let new_password = app
        .update_test_user_pwd()
        .await
        .expect("Failed update user password");

    let user = app.get_user().await.expect("Failed to get user");

    let private_key = app
        .test_user
        .user
        .unlock_private_key(&new_password)
        .expect("Failed to unlock private key");

    let decrypted_data = {
        let decrypted_data: Vec<u8> = private_key
            .expose_secret()
            .decrypt(&user.public_key, &task.data)
            .expect("Failed to decrypt data");
        String::from_utf8(decrypted_data).unwrap()
    };

    assert_eq!(decrypted_data, text);
}

#[tokio::test]
async fn test_create_user_and_login() {
    let app = TestApp::spawn_app().await;
    let password = Secret::new("mypassword".to_string());
    let email = Email::parse("foo@bat.com").expect("invalid email");

    let user = app
        .post_create_user(&email, &password, false)
        .await
        .expect("Failed to create user");

    let logged_in_user = app
        .post_login_with(&email, &password)
        .await
        .expect("Failed to login");

    assert_eq!(logged_in_user, user);
}

#[tokio::test]
async fn test_create_user_same_user_returns_409() {
    let app = TestApp::spawn_app().await;
    let password = Secret::new("mypassword".to_string());

    let response = app
        .post_create_user(&app.test_user.user.email, &password, false)
        .await
        .expect_err("Expected request failure");

    assert_eq!(409, response.status().as_u16());
}

#[tokio::test]
async fn test_create_user_without_salt_returns_400() {
    let app = TestApp::spawn_app().await;
    let password = Secret::new("mypassword".to_string());

    let response = app
        .post_create_user(&app.test_user.user.email, &password, true)
        .await
        .expect_err("Expected request failure");

    assert_eq!(400, response.status().as_u16());
}

#[tokio::test]
async fn test_user_create_with_user_disabled_returns_returns_403() {
    let app = TestApp::spawn_app_without_user_creation().await;
    let password = Secret::new("mypassword".to_string());
    let email = Email::parse("foo@bat.com").expect("invalid email");

    let response = app
        .post_create_user(&email, &password, false)
        .await
        .expect_err("Request should fail");

    assert_eq!(403, response.status().as_u16())
}
