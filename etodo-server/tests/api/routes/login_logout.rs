use etodo_core::domain::Email;
use secrecy::Secret;
use uuid::Uuid;

use crate::helpers::TestApp;

#[tokio::test]
async fn test_login_with_correct_password_succeeds() {
    let app = TestApp::spawn_app().await;

    let user = app
        .post_login_with_test_user()
        .await
        .expect("Failed to login");

    assert_eq!(user, app.test_user.user);
}

#[tokio::test]
async fn test_login_with_incorrect_password_returns_404() {
    let app = TestApp::spawn_app().await;

    let response = app
        .post_login_with(
            &app.test_user.user.email,
            &Secret::new("foo bar".to_string()),
        )
        .await
        .expect_err("Log in request did not fail");

    assert_eq!(400, response.status().as_u16());
}

#[tokio::test]
async fn test_login_with_non_existing_user_returns_404() {
    let app = TestApp::spawn_app().await;

    let random_email = Email::parse(format!("{}@test.com", Uuid::new_v4().to_string()))
        .expect("Failed to generate email");
    let response = app
        .post_login_with(&random_email, &Secret::new("foo bar".to_string()))
        .await
        .expect_err("Log in request did not fail");

    assert_eq!(400, response.status().as_u16());
}

#[tokio::test]
async fn test_logout_after_login() {
    let app = TestApp::spawn_app().await;

    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    app.post_logout().await;
}

#[tokio::test]
async fn test_logout_without_login() {
    let app = TestApp::spawn_app().await;
    app.post_logout().await;
}
