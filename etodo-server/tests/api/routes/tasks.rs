use chrono::Utc;
use etodo_core::domain::{Task, TaskId};

use crate::helpers::TestApp;

#[tokio::test]
async fn get_all_tasks() {
    let app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    let note_text = "My first todo";

    let task1 = app
        .test_user
        .generate_test_item_and_add_to_db(note_text, &app.db)
        .await;
    let task2 = app
        .test_user
        .generate_test_item_and_add_to_db(note_text, &app.db)
        .await;

    let response = app
        .api_client
        .get(app.full_uri("/task"))
        .send()
        .await
        .expect("Failed to execute request");

    assert_eq!(200, response.status().as_u16());

    let returned_tasks = response
        .json::<Vec<Task>>()
        .await
        .expect("Failed to decode data");

    assert_eq!(2, returned_tasks.len());
    assert!(returned_tasks.contains(&task1));
    assert!(returned_tasks.contains(&task2));
}

#[tokio::test]
async fn get_task_without_login_returns_401() {
    let app = TestApp::spawn_app().await;

    let note_text = "My first todo";

    let task = app
        .test_user
        .generate_test_item_and_add_to_db(note_text, &app.db)
        .await;

    let response = app.get_task_from_remote(&task.id).await;

    assert_eq!(401, response.status().as_u16())
}

#[tokio::test]
async fn get_task_after_logout_returns_401() {
    let app = TestApp::spawn_app().await;

    let note_text = "My first todo";

    let task = app
        .test_user
        .generate_test_item_and_add_to_db(note_text, &app.db)
        .await;

    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    app.post_logout().await;

    let response = app.get_task_from_remote(&task.id).await;

    assert_eq!(401, response.status().as_u16())
}

#[tokio::test]
async fn get_task_with_uid() {
    let app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    let note_text = "My first todo";

    let task = app
        .test_user
        .generate_test_item_and_add_to_db(note_text, &app.db)
        .await;
    let _ = app
        .test_user
        .generate_test_item_and_add_to_db(note_text, &app.db)
        .await;

    let response = app.get_task_from_remote(&task.id).await;

    assert!(response.status().is_success());

    let returned_task = response
        .json::<Task>()
        .await
        .expect("Failed to decode data");

    assert_eq!(task, returned_task)
}

#[tokio::test]
async fn get_none_existing_task_returns_404() {
    let app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    let response = app.get_task_from_remote(&TaskId::random()).await;

    assert_eq!(404, response.status().as_u16());
}

#[tokio::test]
async fn get_task_which_was_submitted_to_remote() {
    let app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    let note_text = "My first posted todo";

    let mut task = app.test_user.generate_test_item(&note_text);

    let new_task_id = app.add_task_to_remote(&task).await;

    // update task id to match equality compare later.
    task.id = new_task_id;

    let response = app.get_task_from_remote(&new_task_id).await;

    assert!(response.status().is_success());

    let returned_task = response
        .json::<Task>()
        .await
        .expect("Failed to decode data");

    assert_eq!(task, returned_task)
}

#[tokio::test]
async fn delete_one_task() {
    let app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    let note_text = "My first todo";

    let task = app
        .test_user
        .generate_test_item_and_add_to_db(note_text, &app.db)
        .await;
    let task2 = app
        .test_user
        .generate_test_item_and_add_to_db(note_text, &app.db)
        .await;

    {
        let response = app.delete_task_from_remote(&[task.id]).await;
        assert!(response.status().is_success());
    }
    // deleted task should return 404
    {
        let response = app.get_task_from_remote(&task.id).await;

        assert_eq!(404, response.status().as_u16());
    }

    // check if other task still exists
    {
        let response = app.get_task_from_remote(&task2.id).await;

        assert!(response.status().is_success());

        let returned_task = response
            .json::<Task>()
            .await
            .expect("Failed to decode data");

        assert_eq!(task2, returned_task)
    }
}

#[tokio::test]
async fn delete_multiple_tasks() {
    let app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    let note_text = "My first todo";

    let task = app
        .test_user
        .generate_test_item_and_add_to_db(note_text, &app.db)
        .await;
    let task2 = app
        .test_user
        .generate_test_item_and_add_to_db(note_text, &app.db)
        .await;

    {
        let response = app.delete_task_from_remote(&[task.id, task2.id]).await;
        assert!(response.status().is_success());
    }

    // deleted task should return 404
    {
        let response = app.get_task_from_remote(&task.id).await;

        assert_eq!(404, response.status().as_u16());
    }

    // check if other task id
    {
        let response = app.get_task_from_remote(&task2.id).await;

        assert_eq!(404, response.status().as_u16());
    }
}

#[tokio::test]
async fn delete_none_existing_task_should_return_200() {
    let app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    let response = app.delete_task_from_remote(&[TaskId::random()]).await;
    assert!(response.status().is_success());
}

#[tokio::test]
async fn update_task_finished_status() {
    let app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    let note_text = "My first todo";

    let task = app
        .test_user
        .generate_test_item_and_add_to_db(note_text, &app.db)
        .await;

    app.update_task_on_remote_or_panic(&task.id, Some(true), None, None)
        .await;

    let response = app.get_task_from_remote(&task.id).await;

    assert!(response.status().is_success());

    let returned_task = response
        .json::<Task>()
        .await
        .expect("Failed to decode data");

    assert_eq!(returned_task.finished, true);
    assert_ne!(returned_task.dt_last_updated, task.dt_last_updated);
}

#[tokio::test]
async fn update_task_modified_time_only_returns_bad_request() {
    let app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    let note_text = "My first todo";

    let task = app
        .test_user
        .generate_test_item_and_add_to_db(note_text, &app.db)
        .await;

    let response = app
        .update_task_on_remote(&task.id, None, Some(Utc::now()), None)
        .await;

    assert_eq!(400, response.status().as_u16());
}

#[tokio::test]
async fn update_task_modified_data() {
    let app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    let note_text = "My first todo";

    let task = app
        .test_user
        .generate_test_item_and_add_to_db(note_text, &app.db)
        .await;

    let new_time = Utc::now();

    app.update_task_on_remote_or_panic(
        &task.id,
        None,
        Some(new_time.clone()),
        Some(vec![0u8, 10u8, 30u8]),
    )
    .await;

    let response = app.get_task_from_remote(&task.id).await;

    assert!(response.status().is_success());

    let returned_task = response
        .json::<Task>()
        .await
        .expect("Failed to decode data");

    assert_ne!(returned_task.data.as_ref(), task.data.as_ref());
    assert_ne!(returned_task.dt_last_updated, task.dt_last_updated);
    assert_eq!(returned_task.dt_last_updated, new_time)
}

#[tokio::test]
async fn update_on_unknown_task_returns_404() {
    let app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    let response = app
        .update_task_on_remote(&TaskId::random(), Some(true), None, None)
        .await;

    assert_eq!(404, response.status().as_u16());
}
