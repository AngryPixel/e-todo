use etodo_core::domain::TaskId;

use crate::helpers::TestApp;

#[tokio::test]
async fn test_max_payload_size_limiter() {
    let app = TestApp::spawn_app().await;
    app.post_login_with_test_user()
        .await
        .expect("Failed to login");

    let mut payload = Vec::with_capacity(1024);
    payload.resize_with(1024, || TaskId::random());

    let response = app.delete_task_from_remote(&payload).await;
    assert_eq!(400, response.status().as_u16());
}
