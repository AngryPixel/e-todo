use std::path::PathBuf;

use chrono::{DateTime, Utc};
use etodo_core::crypto::{
    hash_password_client, Encrypted, EncryptionKey, Nonce, PrivateKey, PublicKey, Salt,
};
use etodo_core::domain::{Email, TaskId, User, UserId};
use etodo_server::Application;
use once_cell::sync::Lazy;
use reqwest::Response;
use secrecy::{ExposeSecret, Secret};
use uuid::Uuid;

pub struct TestApp {
    pub address: String,
    pub port: u16,
    pub api_client: reqwest::Client,
    pub test_user: TestUser,
    pub db: etodo_server::db::DBClient,
    pub dir: PathBuf,
}

impl TestApp {
    pub async fn spawn_app() -> TestApp {
        Self::spawn_app_impl(true).await
    }

    pub async fn spawn_app_without_user_creation() -> TestApp {
        Self::spawn_app_impl(false).await
    }

    async fn spawn_app_impl(allow_user_creation: bool) -> TestApp {
        Lazy::force(&TRACING);

        let path_buf = std::env::temp_dir()
            .join(uuid::Uuid::new_v4().simple().to_string())
            .join("db.db");

        std::fs::create_dir_all(path_buf.parent().unwrap())
            .expect("Failed to create tmp directory");

        let mut configuration = etodo_server::get_configuration().expect("Failed to get config");
        configuration.database.path = path_buf.clone();
        configuration.application.port = 0;
        configuration.application.use_ssl = false;
        configuration.application.allow_user_creation = allow_user_creation;

        tracing::debug!(
            "Database path: {}",
            path_buf.to_str().unwrap_or_else(|| "Invalid path")
        );

        let app = Application::build(&configuration)
            .await
            .expect("Failed to create app");
        let port = app.port();
        let address = format!("http://127.0.0.1:{}", port);

        let _ = tokio::spawn(app.run_to_completion());

        let client = reqwest::Client::builder()
            .redirect(reqwest::redirect::Policy::none())
            .cookie_store(true)
            .build()
            .expect("Failed to create test client");

        let db = etodo_server::db::DBClient::new(&configuration.database)
            .await
            .expect("Failed to connect to database");

        let test_app = TestApp {
            address,
            port,
            api_client: client,
            test_user: TestUser::random(),
            db,
            dir: path_buf.parent().unwrap().into(),
        };

        test_app.test_user.write_to_database(&test_app.db).await;

        test_app
    }

    pub fn full_uri(&self, uri: impl AsRef<str>) -> String {
        format!("{}{}", self.address, uri.as_ref())
    }

    pub async fn get_task_from_remote(&self, task_id: &TaskId) -> Response {
        self.api_client
            .get(self.full_uri(&format!("/task/{}", task_id.to_string())))
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn add_task_to_remote(&self, task: &etodo_core::domain::Task) -> TaskId {
        #[derive(serde::Serialize)]
        struct FormData<'a> {
            created: DateTime<Utc>,
            data: &'a [u8],
        }

        let form_data = FormData {
            created: task.dt_created,
            data: task.data.as_ref(),
        };

        let response = self
            .api_client
            .post(self.full_uri("/task"))
            .json(&form_data)
            .send()
            .await
            .expect("Failed to submit task");

        assert_eq!(200, response.status().as_u16());

        response
            .json::<TaskId>()
            .await
            .expect("Failed to convert to task id")
    }

    pub async fn delete_task_from_remote(&self, task_ids: &[TaskId]) -> Response {
        self.api_client
            .delete(self.full_uri("/task"))
            .json(task_ids)
            .send()
            .await
            .expect("Failed to submit task")
    }

    pub async fn update_task_on_remote(
        &self,
        task_id: &TaskId,
        finished: Option<bool>,
        updated: Option<DateTime<Utc>>,
        data: Option<Vec<u8>>,
    ) -> Response {
        #[derive(serde::Serialize)]
        struct FormData {
            finished: Option<bool>,
            updated: Option<DateTime<Utc>>,
            data: Option<Vec<u8>>,
        }

        let form_data = FormData {
            finished,
            updated,
            data,
        };

        self.api_client
            .patch(self.full_uri(format!("/task/{}", task_id.to_string())))
            .json(&form_data)
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn update_task_on_remote_or_panic(
        &self,
        task_id: &TaskId,
        finished: Option<bool>,
        updated: Option<DateTime<Utc>>,
        data: Option<Vec<u8>>,
    ) {
        let response = self
            .update_task_on_remote(task_id, finished, updated, data)
            .await;
        assert_eq!(200, response.status().as_u16());
    }

    pub async fn post_login_with_test_user(&self) -> Result<User, Response> {
        self.post_login_with(&self.test_user.user.email, &self.test_user.password)
            .await
    }

    pub async fn post_login_with(
        &self,
        email: &Email,
        password: &Secret<String>,
    ) -> Result<User, Response> {
        #[derive(serde::Serialize)]
        struct EmailInput<'a> {
            email: &'a str,
        }
        let salt_response = self
            .api_client
            .get(&self.full_uri("/login/salt"))
            .json(&EmailInput {
                email: email.as_ref(),
            })
            .send()
            .await
            .expect("Failed to execute request");

        assert!(salt_response.status().is_success());

        let login_salt = salt_response
            .json::<Salt>()
            .await
            .expect("Failed to decode salt");

        let hashed_password = hash_password_client(password.expose_secret(), &login_salt)
            .expect("Failed to hash password");

        let login_data = [("email", email.as_ref()), ("password", &hashed_password)];

        let response = self
            .api_client
            .post(&self.full_uri("/login"))
            .form(&login_data)
            .send()
            .await
            .expect("Failed to execute request");

        if response.status().is_success() {
            Ok(response
                .json::<User>()
                .await
                .expect("Failed to convert to user"))
        } else {
            Err(response)
        }
    }

    pub async fn post_logout(&self) {
        let response = self
            .api_client
            .post(&self.full_uri("/logout"))
            .send()
            .await
            .expect("Failed to execute request");

        assert!(response.status().is_success());
    }

    pub async fn get_user(&self) -> Result<User, Response> {
        let response = self
            .api_client
            .get(&self.full_uri("/user"))
            .send()
            .await
            .expect("Failed to execute request");

        if response.status().is_success() {
            Ok(response
                .json::<User>()
                .await
                .expect("Failed to convert to user"))
        } else {
            Err(response)
        }
    }

    pub async fn delete_test_user(&self) {
        let response = self
            .api_client
            .delete(&self.full_uri("/user"))
            .send()
            .await
            .expect("Failed to execute request");

        assert!(response.status().is_success());
    }

    pub async fn update_test_user_pwd(&mut self) -> Result<Secret<String>, Response> {
        let update_salt_response = self
            .api_client
            .get(&self.full_uri("/user/salt"))
            .send()
            .await
            .expect("Failed to get salt");
        assert!(update_salt_response.status().is_success());

        let update_salt = update_salt_response
            .json::<Salt>()
            .await
            .expect("Failed to get salt from response");

        let new_password = Secret::new(Uuid::new_v4().to_string());
        let new_password_hash = hash_password_client(new_password.expose_secret(), &update_salt)
            .expect("Failed to calculate new password");

        let pwd_output = self
            .test_user
            .user
            .compute_password_change(&self.test_user.password, &new_password)
            .expect("Failed to compute password change");

        #[derive(serde::Serialize)]
        struct PasswordUpdate {
            old_password: String,
            new_password: String,
            encrypted_key: etodo_core::crypto::Encrypted<etodo_core::crypto::EncryptionKey>,
            encryption_nonce: etodo_core::crypto::Nonce,
            encryption_salt: etodo_core::crypto::Salt,
        }

        let response = self
            .api_client
            .patch(&self.full_uri("/user/password"))
            .json(&PasswordUpdate {
                old_password: self.test_user.password_hash.clone(),
                new_password: new_password_hash.clone(),
                encrypted_key: pwd_output.key.clone(),
                encryption_nonce: pwd_output.nonce.clone(),
                encryption_salt: pwd_output.salt.clone(),
            })
            .send()
            .await
            .expect("Failed to send request");

        if !response.status().is_success() {
            return Err(response);
        }

        self.test_user.user.apply_password_change(pwd_output);
        self.test_user.login_salt = update_salt;
        self.test_user.password_hash = new_password_hash;
        self.test_user.password = new_password.clone();
        Ok(new_password)
    }

    pub async fn update_test_user_email(&mut self) -> Result<(), Response> {
        let new_email = Email::parse(format!("{}@test.com", Uuid::new_v4().to_string()))
            .expect("Failed to parse email");

        #[derive(serde::Serialize)]
        struct EmailUpdate {
            email: String,
        }

        let response = self
            .api_client
            .patch(&self.full_uri("/user/email"))
            .json(&EmailUpdate {
                email: new_email.as_ref().to_string(),
            })
            .send()
            .await
            .expect("Failed to send request");

        if !response.status().is_success() {
            return Err(response);
        }

        self.test_user.user.email = new_email;

        Ok(())
    }

    pub async fn post_create_user(
        &self,
        email: &Email,
        password: &Secret<String>,
        skip_salt_request: bool,
    ) -> Result<User, Response> {
        let mut user = User::generate(email.clone(), &password).expect("Failed to generate user");

        #[derive(serde::Serialize)]
        struct EmailInput<'a> {
            email: &'a str,
        }

        let hashed_password = {
            if skip_salt_request {
                hash_password_client(password.expose_secret(), &Salt::random())
                    .expect("Failed to hash password")
            } else {
                let salt_response = self
                    .api_client
                    .get(&self.full_uri("/login/salt"))
                    .json(&EmailInput {
                        email: email.as_ref(),
                    })
                    .send()
                    .await
                    .expect("Failed to execute request");

                assert!(salt_response.status().is_success());

                let login_salt = salt_response
                    .json::<Salt>()
                    .await
                    .expect("Failed to get login salt");

                hash_password_client(password.expose_secret(), &login_salt)
                    .expect("Failed to hash password")
            }
        };

        #[derive(serde::Serialize)]
        struct AddUserData<'a> {
            email: &'a str,
            public_key: &'a PublicKey,
            encryption_key: &'a Encrypted<EncryptionKey>,
            encryption_key_nonce: &'a Nonce,
            private_key: &'a Encrypted<PrivateKey>,
            private_key_nonce: &'a Nonce,
            encryption_salt: &'a Salt,
            password: &'a str,
        }

        let submission_data = AddUserData {
            email: email.as_ref(),
            public_key: &user.public_key,
            encryption_key: &user.encryption_key,
            encryption_key_nonce: &user.encryption_key_nonce,
            private_key: &user.private_key,
            private_key_nonce: &user.private_key_nonce,
            encryption_salt: &user.encryption_salt,
            password: &hashed_password,
        };

        let response = self
            .api_client
            .post(&self.full_uri("/user"))
            .json(&submission_data)
            .send()
            .await
            .expect("Failed to submit request");
        if !response.status().is_success() {
            return Err(response);
        }

        let user_id = response
            .json::<UserId>()
            .await
            .expect("Failed to read user id");

        user.id = user_id;

        return Ok(user);
    }
}

impl Drop for TestApp {
    fn drop(&mut self) {
        std::fs::remove_dir_all(&self.dir).expect("Failed to delete tmp directory");
    }
}

pub struct TestUser {
    pub password: Secret<String>,
    pub password_hash: String,
    pub login_salt: Salt,
    pub user: User,
}

impl TestUser {
    pub fn random() -> Self {
        let email = Email::parse(format!("{}@test.com", Uuid::new_v4().to_string()))
            .expect("Failed to parse email");
        let password = Secret::new(Uuid::new_v4().to_string());
        let user =
            User::generate(email.clone(), &password).expect("Failed to generate test user data");
        let salt = Salt::random();
        let password_hash = hash_password_client(password.expose_secret(), &salt)
            .expect("Failed to generate password hash");

        Self {
            password,
            user,
            password_hash,
            login_salt: salt,
        }
    }

    async fn write_to_database(&self, db: &etodo_server::db::DBClient) {
        db.add_user(&self.user, &self.password_hash, &self.login_salt)
            .await
            .expect("Failed to create test user");
    }

    pub fn generate_test_item(&self, text: &str) -> etodo_core::domain::Task {
        let encrypted = self
            .user
            .public_key
            .encrypt(text.as_bytes())
            .expect("Failed to encrypt");
        etodo_core::domain::Task::with_data(encrypted)
    }

    pub async fn generate_test_item_and_add_to_db(
        &self,
        text: &str,
        db: &etodo_server::db::DBClient,
    ) -> etodo_core::domain::Task {
        let encrypted = self
            .user
            .public_key
            .encrypt(text.as_bytes())
            .expect("Failed to encrypt");

        let item = etodo_core::domain::Task::with_data(encrypted);

        db.add_task_test(&self.user.id, &item)
            .await
            .expect("failed to add task");

        item
    }
}

// Logging setup.
static TRACING: Lazy<()> = Lazy::new(|| {
    //std::env::set_var("RUST_LOG", "debug");
    //std::env::set_var("RUST_BACKTRACE", "1");

    // Increase file limit to ensure we can run the unit tests without issues.
    #[cfg(target_os = "linux")]
    rlimit::increase_nofile_limit(10000).expect("Failed to increase file limit");

    etodo_server::logging::init_fmt_logger()
});
