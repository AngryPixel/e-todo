use crate::helpers::TestApp;

#[tokio::test]
async fn test_health_check_endpoint() {
    let app = TestApp::spawn_app().await;

    let response = app
        .api_client
        .get(app.full_uri("/health_check"))
        .send()
        .await
        .expect("Failed to execute request");

    assert!(response.status().is_success());
    assert_eq!(Some(0), response.content_length());
}
