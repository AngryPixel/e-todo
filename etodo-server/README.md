# etodo-server

Server which powers the E-Todo Project.

## Overview

The server is using [actix-web](https://crates.io/crates/actix-web) as http server and stores all its data 
in a sqlite3 database using [sqlx](https://crates.io/crates/sqlx). 

The original idea behind this server was to make it easy to deploy to someone's VPS/Server without requiring many
additional dependencies or configuration steps. 

It's recommended to keep the total user count on the server low due to the nature of sqlite.

Finally, if running this for a single user, the server can be configured to not allow futher user creation.

## Security

The server uses a random generated keys for SSL and the session cookie storage. While this allows it to run without 
requiring much setup, every time it is rebooted existing session will become invalid.

To avoid dealing with database engine and redis dependencies, we chose to use 
[actix-session](https://crates.io/crates/actix-session)'s builtin cookie storage.